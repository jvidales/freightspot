class CreateWarehouses < ActiveRecord::Migration[5.2]
  def change
    create_table :warehouses do |t|
      t.string :name

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :warehouses, :deleted_at
  end
end
