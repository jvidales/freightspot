class CreateWayPays < ActiveRecord::Migration[5.2]
  def change
    create_table :way_pays do |t|
      t.string :code
      t.string :name
      t.text :description
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :way_pays, :deleted_at
  end
end
