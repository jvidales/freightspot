class CreateOrganizations < ActiveRecord::Migration[5.2]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :slug
      
      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :organizations, :deleted_at
    add_index :organizations, :slug, unique: true

    Organization.create(name: 'Freightspot S.A de C.V')
  end
end
