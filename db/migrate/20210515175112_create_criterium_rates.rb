class CreateCriteriumRates < ActiveRecord::Migration[5.2]
  def change
    create_table :criterium_rates do |t|
      t.references :criterium, foreign_key: true
      t.references :rate, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :criterium_rates, :deleted_at
  end
end
