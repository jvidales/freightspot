class CreateRateSurcharges < ActiveRecord::Migration[5.2]
  def change
    create_table :rate_surcharges do |t|
      t.references :rate, foreign_key: true
      t.references :surcharge, foreign_key: true
      t.references :currency, foreign_key: true
      t.decimal :value, precision: 10, scale: 4, default: 0
      t.string :type_surcharge

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :rate_surcharges, :deleted_at
  end
end
