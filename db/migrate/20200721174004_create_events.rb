class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.string :detail
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :events, :deleted_at
    add_index :events, :slug
  end
end
