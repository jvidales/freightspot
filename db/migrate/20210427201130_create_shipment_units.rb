class CreateShipmentUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :shipment_units do |t|
      t.references :shipment, foreign_key: true
      t.references :packaging_type, foreign_key: true
      t.references :product, foreign_key: true
      t.references :container, foreign_key:true
      t.bigint :number_units, default: 1
      t.decimal :length, precision: 10, scale: 4, default: 0
      t.decimal :height, precision: 10, scale: 4, default: 0
      t.decimal :width, precision: 10, scale: 4, default: 0
      t.decimal :weight, precision: 10, scale: 4, default: 0
      t.decimal :volume, precision: 20, scale: 4, default: 0

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :shipment_units, :deleted_at
  end
end
