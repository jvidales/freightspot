class CreateModeTransportations < ActiveRecord::Migration[5.2]
  def change
    create_table :mode_transportations do |t|
      t.string :code
      t.string :name
      t.references :service_mode, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :mode_transportations, :deleted_at
  end
end
