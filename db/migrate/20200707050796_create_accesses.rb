class CreateAccesses < ActiveRecord::Migration[5.2]
  def change
    create_table :accesses do |t|
      t.string :name
      t.string :slug
      t.timestamp :deleted_at

      t.timestamps
    end

    add_index :accesses, :deleted_at
  end
end
