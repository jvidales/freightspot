class CreateMiddlemen < ActiveRecord::Migration[5.2]
  def change
    create_table :middlemen do |t|
      t.string :name
      t.string :website
      t.string :rfc
      #add by AnnaCeci
      t.references :payment_term, foreign_key: true
      t.references :currency, foreign_key: true
      t.references :cfdi, foreign_key: true
      t.references :way_pay, foreign_key: true
      t.references :payment_method, foreign_key: true
      t.boolean :inactive, default: false
      t.bigint :status, default: 0         
      t.string :slug
      t.timestamp :deleted_at

      t.timestamps
    end

    add_index :middlemen, :deleted_at
  end
end
