class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.string :name
      t.references :department, foreign_key: true
      t.string :slug
      
      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :teams, :deleted_at
    add_index :teams, :slug, unique: true
  end
end
