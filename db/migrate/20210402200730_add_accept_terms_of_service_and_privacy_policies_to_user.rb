class AddAcceptTermsOfServiceAndPrivacyPoliciesToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :accept_terms_of_service_and_privacy_policies, :boolean, default: false
  end
end
