class CreateDeliveryRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :delivery_routes do |t|
      t.references :shipment, foreign_key: true
      t.belongs_to :destiny_port, foreign_key: { to_table: :ports }
      t.string :destiny
      t.decimal :latitude
      t.decimal :longitude

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :delivery_routes, :deleted_at
  end
end
