class CreatePorts < ActiveRecord::Migration[5.2]
  def change
    create_table :ports do |t|
      t.string :port_identifier
      t.string :iata_code
      t.string :name
      t.integer :transportation_method
      t.boolean :inactive, default: false
      t.references :country, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :ports, :slug
    add_index :ports, :deleted_at
  end
end
