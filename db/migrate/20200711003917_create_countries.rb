class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.string :code
      t.string :name
      t.string :phonecode
      t.boolean :prohibited, default: false

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :countries, :deleted_at
  end
end
