class CreateSurcharges < ActiveRecord::Migration[5.2]
  def change
    create_table :surcharges do |t|
      t.string :code
      t.string :name
      t.text :description
      t.boolean :active, default: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :surcharges, :deleted_at
    add_index :surcharges, :slug
  end
end
