class CreateAddressAddressTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :address_address_types do |t|
      t.references :address, foreign_key: true
      t.references :address_type, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :address_address_types, :deleted_at    
  end
end
