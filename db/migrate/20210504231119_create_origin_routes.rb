class CreateOriginRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :origin_routes do |t|
      t.references :quote, foreign_key: true
      t.belongs_to :origin_port, foreign_key: { to_table: :ports }
      t.string :origin
      t.decimal :latitude
      t.decimal :longitude

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :origin_routes, :deleted_at
  end
end
