class CreateCurrencies < ActiveRecord::Migration[5.2]
  def change
    create_table :currencies do |t|
      t.string :code
      t.string :name

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :currencies, :deleted_at
  end
end
