class CreateMultiServices < ActiveRecord::Migration[5.2]
  def change
    create_table :multi_services do |t|
      t.references :supplier, foreign_key: true
      t.references :service_mode, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :multi_services, :deleted_at
  end
end
