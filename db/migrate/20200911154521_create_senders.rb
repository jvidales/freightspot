class CreateSenders < ActiveRecord::Migration[5.2]
  def change
    create_table :senders do |t|
      t.references :senderable, polymorphic: true
      t.string :contact
      t.string :name
      t.string :rfc
      t.string :email
      t.string :phone
      t.string :address

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :senders, :deleted_at
  end
end
