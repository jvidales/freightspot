class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.references :user, foreign_key: true
      t.references :language, foreign_key: true
      t.string :time_zone, default: 'Mexico City'

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :settings, :deleted_at
  end
end
