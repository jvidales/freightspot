class CreateSenderTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :sender_types do |t|
      t.string :name
      t.boolean :inactive, default: false

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :sender_types, :deleted_at
  end
end
