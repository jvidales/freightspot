class CreateServiceModes < ActiveRecord::Migration[5.2]
  def change
    create_table :service_modes do |t|
      t.string :code
      t.string :name
      t.bigint :variable

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :service_modes, :deleted_at
  end
end
