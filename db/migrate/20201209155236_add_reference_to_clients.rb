class AddReferenceToClients < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def change
    add_reference :clients, :middleman, index: {algorithm: :concurrently}
  end

end
