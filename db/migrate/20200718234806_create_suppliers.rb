class CreateSuppliers < ActiveRecord::Migration[5.2]
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :website
      t.string :rfc
      t.string :iata_code
      t.string :caat_code
      t.string :scac_number
      t.boolean :inactive, default: false
      t.boolean :has_credit, default: false
      t.references :payment_term, foreign_key: true
      t.references :currency, foreign_key: true
      t.decimal :credit_limit, precision: 10, scale: 4, default: 0
      t.belongs_to :parent_entity, foreign_key: { to_table: :suppliers }
      t.text :notes
      t.boolean :authorized_credit, default: false
      t.references :cfdi, foreign_key: true
      t.references :way_pay, foreign_key: true
      t.references :payment_method, foreign_key: true
      t.boolean :carrier, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :suppliers, :deleted_at
    add_index :suppliers, :slug
  end
end
