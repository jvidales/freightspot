class CreateShipments < ActiveRecord::Migration[5.2]
  def change
    create_table :shipments do |t|
      t.string :qr_code
      t.string :bar_code
      t.string :number
      t.string :guide_number
      t.string :reservation_number
      t.references :quote, foreign_key: true
      t.timestamp :realized_at
      t.timestamp :departured_at
      t.timestamp :estimated_departured_at
      t.timestamp :arrivaled_at
      t.timestamp :estimated_arrivaled_at
      t.integer :type_service
      t.integer :operation_type
      t.text :description
      t.decimal :declared_value, precision: 10, scale: 4, default: 0
      t.bigint :service_mode
      t.references :incoterm, foreign_key: true
      t.belongs_to :freight_type, foreign_key: { to_table: :mode_transportations }
      t.references :client, foreign_key: true
      t.bigint :frequency
      t.boolean :danger_item, default: false
      t.integer :free_day_of_delay
      t.decimal :total_pieces, precision: 10, scale: 4, default: 0
      t.decimal :total_weight, precision: 10 , scale: 4, default: 0
      t.decimal :total_volume, precision: 20, scale: 4, default: 0
      t.decimal :chargeable_weight, precision: 20, scale: 4, default: 0
      t.belongs_to :salesman, foreign_key: { to_table: :employees }
      t.belongs_to :employee, foreign_key: { to_table: :employees }
      t.boolean :guarantee_letter, default: false
      t.bigint :status, default: 0
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :shipments, :deleted_at
    add_index :shipments, :slug
  end
end
