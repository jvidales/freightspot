class CreateBranches < ActiveRecord::Migration[5.2]
  def change
    create_table :branches do |t|
      t.string :name
      t.references :organization, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    
    add_index :branches, :deleted_at
    add_index :branches, :slug, unique: true

    Branch.create(name: 'Sucursal #1', organization_id: 1)
  end
end
