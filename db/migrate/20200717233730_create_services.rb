class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :code
      t.text :description
      t.integer :service_type
      t.decimal :price, precision: 10, scale: 4, default: 0
      t.boolean :has_cost, default: false
      t.references :currency, foreign_key: true
      t.references :tax, foreign_key: true
      t.references :service_mode, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :services, :code
    add_index :services, :deleted_at
    add_index :services, :slug, unique: true
  end
end
