class CreateEntities < ActiveRecord::Migration[5.2]
  def change
    create_table :entities do |t|
      t.string :name
      t.string :locale
      t.string :sender

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :entities, :deleted_at
  end
end
