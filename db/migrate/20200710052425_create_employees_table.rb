class CreateEmployeesTable < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.timestamp :birth_date
      t.string :phone
      t.string :phone_extension
      t.string :mobile
      t.string :origin
      t.string :city
      t.string :office_branch
      t.string :squad
      t.string :job_title
      t.timestamp :join_date
      t.string :language
      t.references :user, foreign_key: true
      
      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :employees, :deleted_at
  end
end
