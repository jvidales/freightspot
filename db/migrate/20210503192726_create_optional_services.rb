class CreateOptionalServices < ActiveRecord::Migration[5.2]
  def change
    create_table :optional_services do |t|
      t.string :name
      t.decimal :cost
      t.string :category
      t.boolean :active, default: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :optional_services, :deleted_at
  end
end
