class CreateCharges < ActiveRecord::Migration[5.2]
  def change
    create_table :charges do |t|
      t.references :chargeable, polymorphic: true, index: true
      t.references :service, foreign_key: true
      t.references :currency, foreign_key: true
      t.references :tax, foreign_key: true
      t.text :description
      t.string :apply_to_able_type
      t.string :apply_type
      t.bigint :apply_to_able_id
      t.integer :apply_by
      t.bigint :type_charge
      t.integer :number_pieces, default: 1
      t.decimal :total_weight, precision: 20, scale: 4, default: 0
      t.decimal :total_volume, precision: 20, scale: 4, default: 0
      t.decimal :weight_to_charge, precision: 20, scale: 4, default: 0
      t.decimal :quantity, precision: 20, scale: 4, default: 0
      t.string :unit
      t.decimal :price, precision: 20, scale: 4, default: 0
      t.decimal :total, precision: 20, scale: 4, default: 0

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :charges, :deleted_at
  end
end
