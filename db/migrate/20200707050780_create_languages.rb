class CreateLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :languages do |t|
      t.string :code
      t.string :name
      t.boolean :default, default: false

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :languages, :deleted_at
  end
end
