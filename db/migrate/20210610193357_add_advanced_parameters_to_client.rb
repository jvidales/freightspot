class AddAdvancedParametersToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :advanced_parameters, :boolean
  end
end
