class CreateDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :documents do |t|
      t.string :name
      t.boolean :obligatory
      t.string :document_type
      t.string :phase
      t.boolean :active, default: true
      t.string :slug
      t.string :variable
      t.boolean :client, default: true
      t.boolean :supplier, default: true

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :documents, :deleted_at
  end  
end
