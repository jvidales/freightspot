class CreateQuotes < ActiveRecord::Migration[5.2]
  def change
    create_table :quotes do |t|
      t.string :number
      t.references :rate, foreign_key: true
      t.timestamp :quoted_at
      t.timestamp :expired_at
      t.integer :operation_type
      t.text :description
      t.decimal :declared_value, precision: 10, scale: 4, default: 0
      t.references :client, foreign_key: true
      t.references :payment_term, foreign_key: true
      t.references :organization, foreign_key: true
      t.references :incoterm, foreign_key: true
      t.bigint :service_mode
      t.belongs_to :freight_type, foreign_key: { to_table: :mode_transportations }
      t.string :service_type
      t.bigint :frequency
      t.boolean :danger_item, default: false
      t.integer :free_day_of_delay
      t.belongs_to :salesman, foreign_key: { to_table: :employees }
      t.belongs_to :employee, foreign_key: { to_table: :employees }
      t.decimal :total_pieces, precision: 10, scale: 4, default: 0
      t.decimal :total_weight, precision: 10 , scale: 4, default: 0
      t.decimal :total_volume, precision: 20, scale: 4, default: 0
      t.decimal :total_weight_volume, precision: 20, scale: 4, default: 0
      t.boolean :guarantee_letter, default: false
      t.boolean :is_active_shipment, default: false
      t.bigint :status, default: 0
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :quotes, :deleted_at
    add_index :quotes, :slug
  end
end
