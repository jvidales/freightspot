class CreateContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :containers do |t|
      t.string :code
      t.string :name
      t.string :description
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :containers, :deleted_at
    add_index :containers, :slug, unique: true
  end
end
