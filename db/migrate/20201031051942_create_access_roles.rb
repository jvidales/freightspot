class CreateAccessRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :access_roles do |t|
      t.references :access, foreign_key: true
      t.references :role, foreign_key: true
      t.references :system_module, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :access_roles, :deleted_at
  end
end
