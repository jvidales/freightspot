class CreateClientDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :client_documents do |t|
      t.references :client, foreign_key: true
      t.references :document, foreign_key: true
      t.timestamp :deleted_at

      t.timestamps
    end
    add_index :client_documents, :deleted_at
  end
end
