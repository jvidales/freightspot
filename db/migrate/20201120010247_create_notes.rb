class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.references :noteable, polymorphic: true
      t.text :note

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :notes, :deleted_at
  end
end
