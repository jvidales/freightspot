class CreateRates < ActiveRecord::Migration[5.2]
  def change
    create_table :rates do |t|
      t.references :tariff, foreign_key: true
      t.belongs_to :origin_port, foreign_key: { to_table: :ports }
      t.belongs_to :discharge_port, foreign_key: { to_table: :ports }
      t.decimal :freight_rate, precision: 20, scale: 4, default: 0
      t.decimal :minimal_cost, precision: 20, scale: 4, default: 0
      t.references :container, foreign_key: true
      t.string :measurement_unit
      t.integer :transite_time
      t.bigint :frequency
      t.string :via_code
      t.string :via
      t.integer :free_days
      t.boolean :guarantee_letter, default: false

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :rates, :deleted_at
  end
end
