class CreateAgentDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :agent_documents do |t|
      t.references :agent, foreign_key: true
      t.references :document, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :agent_documents, :deleted_at
  end
end
