class CreateBankAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :bank_accounts do |t|
      t.string :bank
      t.string :account_number
      t.string :owner
      t.references :client, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :bank_accounts, :deleted_at
  end
end
