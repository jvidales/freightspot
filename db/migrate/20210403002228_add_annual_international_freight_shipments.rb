class AddAnnualInternationalFreightShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :annual_international_freight_shipments, :bigint
  end
end
