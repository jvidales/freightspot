class AddAuthorizedToQuotes < ActiveRecord::Migration[5.2]
  def change
    add_column :quotes, :authorized, :boolean, default: false
  end
end
