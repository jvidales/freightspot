class AddLatitudeAndLongitudeToPorts < ActiveRecord::Migration[5.2]
  def change
    add_column :ports, :latitude, :decimal
    add_column :ports, :longitude, :decimal
  end
end
