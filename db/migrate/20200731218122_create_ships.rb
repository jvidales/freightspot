class CreateShips < ActiveRecord::Migration[5.2]
  def change
    create_table :ships do |t|
      t.string :name

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :ships, :deleted_at
  end
end
