class CreateServiceTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :service_types do |t|
      t.references :incoterm, foreign_key: true
      t.string :operation_type
      t.string :name

    
      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :service_types, :deleted_at
  end
end
