class CreateTaxes < ActiveRecord::Migration[5.2]
  def change
    create_table :taxes do |t|
      t.string :code
      t.string :name
      t.decimal :value, precision: 10, scale: 4, default: 0
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :taxes, :code
    add_index :taxes, :deleted_at
    add_index :taxes, :slug, unique: true
  end
end
