class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :description
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :tasks, :deleted_at
    add_index :tasks, :slug
  end
end
