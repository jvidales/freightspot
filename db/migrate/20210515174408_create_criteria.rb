class CreateCriteria < ActiveRecord::Migration[5.2]
  def change
    create_table :criteria do |t|
      t.string :name
      t.decimal :amount
      t.references :currency, foreign_key: true
      t.timestamp :validity_start
      t.timestamp :validity_end
      t.text :remarks
      t.boolean :advanced_parameters, default: false
      t.belongs_to :origin, foreign_key: { to_table: :ports }
      t.belongs_to :destiny, foreign_key: { to_table: :ports }
      t.references :container, foreign_key: true
      t.bigint :status, default: 1

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :criteria, :deleted_at
  end
end
