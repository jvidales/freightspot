class CreateUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :units do |t|
      t.string :name
      t.bigint :unit_type
      t.string :code
      t.integer :value, default: 0
      t.bigint :unit_system

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :units, :deleted_at
  end
end
