class AddForeignKeyOnClients < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :clients, :middlemen, validate: false
  end
end
