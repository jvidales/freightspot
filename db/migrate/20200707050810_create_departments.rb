class CreateDepartments < ActiveRecord::Migration[5.2]
  def change
    create_table :departments do |t|
      t.string :name
      t.references :branch, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end
    
    add_index :departments, :deleted_at
  end
end
