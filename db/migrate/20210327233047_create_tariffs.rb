class CreateTariffs < ActiveRecord::Migration[5.2]
  def change
    create_table :tariffs do |t|
      t.belongs_to :carrier, foreign_key: { to_table: :suppliers }
      t.bigint :service_mode
      t.belongs_to :freight_type, foreign_key: { to_table: :mode_transportations }
      t.bigint :cargo_type
      t.timestamp :started_at
      t.timestamp :ended_at

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :tariffs, :deleted_at
  end
end
