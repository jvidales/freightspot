# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, name:{ name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', name:movie: movies.first)
#
Dir[File.join(Rails.root, 'db', 'seeds', 'catalogs', '*.rb')].sort.each do |seed|
  print "Loading #{seed} ...."
  load seed
  print "DONE\n"
end

rol_admin = Role.find_by_name('Administrador')
rol_user = Role.find_by_name('Usuario')
rol_client = Role.find_by_name('Cliente')

user1 = User.create(username: 'arodriguez', email: 'arodriguez@gcexpress.mx', admin: true, password: '12345678', role_id: rol_admin.id, accept_terms_of_service_and_privacy_policies: true)
Employee.create(name: 'Alí Rodriguez', phone: '55555555', phone_extension: '100', origin: 'Mexico', city: 'CDMX', office_branch: 'Terraz', squad: 'Squad 1', job_title: 'Developer', language: 'Español', user_id: user1.id)
User.find_each(&:save)

user2 =User.create(username: 'kluna', email: 'kluna@gcexpress.mx', admin: true, password: '12345678', role_id: rol_admin.id, accept_terms_of_service_and_privacy_policies: true)
Employee.create(name: 'Karenina Luna', phone: '55555555', phone_extension: '100', origin: 'Mexico', city: 'CDMX', office_branch: 'Terraz', squad: 'Squad 1', job_title: 'Developer', language: 'Español', user_id: user2.id)
User.find_each(&:save)

user3 = User.create(username: 'test-user1', email: 'test-user1@gcexpress.mx', admin: false, password: '12345678', role_id: rol_user.id, accept_terms_of_service_and_privacy_policies: true)
Employee.create(name: 'Test User1', phone: '55555555', phone_extension: '100', origin: 'Mexico', city: 'CDMX', office_branch: 'Terraz', squad: 'Squad 1', job_title: 'Developer', language: 'Español', user_id: user3.id)
User.find_each(&:save)

user4 = User.create(username: 'test-user2', email: 'test-user2@gcexpress.mx', admin: false, password: '12345678', role_id: rol_user.id, accept_terms_of_service_and_privacy_policies: true)
Employee.create(name: 'Test User2', phone: '55555555', phone_extension: '100', origin: 'Mexico', city: 'CDMX', office_branch: 'Terraz', squad: 'Squad 1', job_title: 'Developer', language: 'Español', user_id: user4.id)
User.find_each(&:save)

user5 = User.create(username: 'test-user3', email: 'test-user3@gcexpress.mx', admin: false, password: '12345678', role_id: rol_user.id, accept_terms_of_service_and_privacy_policies: true)
Employee.create(name: 'Test User3 ', phone: '55555555', phone_extension: '100', origin: 'Mexico', city: 'CDMX', office_branch: 'Terraz', squad: 'Squad 1', job_title: 'Developer', language: 'Español', user_id: user5.id)
User.find_each(&:save)

user6 = User.create(username: 'test-user4', email: 'test-user4@gcexpress.mx', admin: false, password: '12345678', role_id: rol_user.id, accept_terms_of_service_and_privacy_policies: true)
Employee.create(name: 'Test User4', phone: '55555555', phone_extension: '100', origin: 'Mexico', city: 'CDMX', office_branch: 'Terraz', squad: 'Squad 1', job_title: 'Developer', language: 'Español', user_id: user6.id)
User.find_each(&:save)

user7 = User.create(username: 'test-user5', email: 'test-user5@gcexpress.mx', admin: false, password: '12345678', role_id: rol_user.id, accept_terms_of_service_and_privacy_policies: true)
Employee.create(name: 'Test User5', phone: '55555555', phone_extension: '100', origin: 'Mexico', city: 'CDMX', office_branch: 'Terraz', squad: 'Squad 1', job_title: 'Developer', language: 'Español', user_id: user7.id)
User.find_each(&:save)

middleman = Middleman.new(rfc: 'ROVI9209LI3', name: 'Ivan Ali Rodriguez Vasquez', website: 'www.example.com',currency_id: 1, payment_term_id: 1)
middleman.save
Middleman.find_each(&:save)
Address.create(addressable_type: 'Middleman', addressable_id: middleman.id, street: 'SALINA CRUZ', interior_number: '6', country: 'MEXICO', zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create(phoneable_type: 'Middleman', phoneable_id: middleman.id, code: '52', number: '9711009012', principal: true, type_phone: 0)
contact_middleman = Contact.new(contactable_type: 'Middleman', contactable_id: middleman.id, name: 'William Nolasco de la Cruz', email: 'wnolasco@gmail.com', principal: true, birth_date: '1992-08-11 19:08:27.108077')
contact_middleman.save
Address.create(addressable_type: 'Contact', addressable_id: contact_middleman.id, street: 'Gomez Farias', interior_number: '12', country: 'MEXICO', zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create(phoneable_type: 'Contact', phoneable_id: contact_middleman.id, code: '+52', number: '9531202393', principal: true, type_phone: 0)

client = Client.new(name: 'IBAÑES S.A DE C.V', website: 'www.ibañes.com', currency_id: 1, payment_term_id: 1, middleman_id: middleman.id)
client.save
Client.find_each(&:save)
Address.create(addressable_type: 'Client', addressable_id: client.id, street: 'SALINA CRUZ', interior_number: '6', country: 'MEXICO', zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create(phoneable_type: 'Client', phoneable_id: client.id, code: '52', number: '9711009012', principal: true, type_phone: 0)
user_contact = User.create(username: 'jalvaro', email: 'jalvaro@gcexpress.mx', admin: false, password: '12345678', role_id: rol_client.id, accept_terms_of_service_and_privacy_policies: true)
contact = Contact.new(contactable_type: 'Client', contactable_id: client.id, name: 'JOSE ALVARO RAMIREZ SANTIAGO', email: 'jalvaro@gmail.com', principal: true, birth_date: '1990-08-13 19:08:27.108077', user_id: user_contact.id)
contact.save
Address.create(addressable_type: 'Contact', addressable_id: contact.id, street: 'SALINA CRUZ', interior_number: '6', country: 'MEXICO', zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create(phoneable_type: 'Contact', phoneable_id: contact.id, code: '+52', number: '9711009012', principal: true, type_phone: 0)

# Create Supplier
supplier = Supplier.new(name: 'DITROMEXICO S.A DE C.V', website: 'www.ditromexico.com', currency_id: 3, payment_term_id: 1)
supplier.save
Supplier.find_each(&:save)
Address.create(addressable_type: 'Supplier', addressable_id: supplier.id, street: 'RICARDO FLORES MAGON', interior_number: '6', country: 'MEXICO', zip_code: '70110', principal: true, colony: '1° SECCION', state_id: 208, name: 'Addres #1')
Phone.create(phoneable_type: 'Supplier', phoneable_id: supplier.id, code: '52', number: '9711192021', principal: true, type_phone: 0)
contact2 = Contact.new(contactable_type: 'Supplier', contactable_id: supplier.id, name: 'ANA MARIA CRUZ ALVAREZ', email: 'amaria@gmail.com', principal: true, birth_date: '1989-02-03 19:08:27.108077')
contact2.save
Address.create(addressable_type: 'Contact', addressable_id: contact2.id, street: 'SALINA CRUZ', interior_number: '6', country: 'MEXICO', zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create(phoneable_type: 'Contact', phoneable_id: contact2.id, code: '+52', number: '9711009012', principal: true, type_phone: 0)

maritimo = ServiceMode.create(code: 'FM', name: 'Servicios Marítimos', variable: 2)
aereo = ServiceMode.create(code: 'FA', name: 'Servicios Aéreos', variable: 0)
aduanal = ServiceMode.create(code: 'SS', name: 'Servicios Aduanales', variable: 3)
terrestre = ServiceMode.create(code: 'FT', name: 'Servicios Terrestres', variable: 1)

# Maritimo
fcl = ModeTransportation.create(code: 'FCL', name: 'Full Container Load', service_mode_id: maritimo.id)
lcl = ModeTransportation.create(code: 'LCL', name: 'Less than Container Load', service_mode_id: maritimo.id)

# Terrestre
# ModeTransportation.create(code: '', name: 'CARRETERA', service_mode_id: terrestre.id)
ltl = ModeTransportation.create(code: 'LTL', name: 'Less Than Truckload', service_mode_id: terrestre.id)
ftl = ModeTransportation.create(code: 'FTL', name: 'Full Truck Load', service_mode_id: terrestre.id)
# ModeTransportation.create(code: '', name: 'FERROCARRIL', service_mode_id: terrestre.id)

# Aduanal
ModeTransportation.create(code: '', name: 'INTERNAS', service_mode_id: aduanal.id)
ModeTransportation.create(code: '', name: 'FRONTERAS', service_mode_id: aduanal.id)
ModeTransportation.create(code: '', name: 'MARITIMAS', service_mode_id: aduanal.id)

Service.create(code: '4jfd8', description: 'INGRESO 1', service_type: 0, price: 120.0, has_cost: false, currency_id: 2, service_mode_id: 1)
Service.create(code: '9d3md', description: 'COSTO 1', service_type: 1, price: 120.0, has_cost: false, currency_id: 2, service_mode_id: 1)
Service.create(code: '3md93', description: 'INGRESO 2', service_type: 0, price: 150.0, has_cost: false, currency_id: 2, service_mode_id: 2)
Service.create(code: '0smsl', description: 'COSTO 2', service_type: 1, price: 150.0, has_cost: false, currency_id: 2, service_mode_id: 2)
Service.create(code: 'dkk39', description: 'INGRESO 3', service_type: 0, price: 180.0, has_cost: false, currency_id: 2, service_mode_id: 4)
Service.create(code: '93md8', description: 'COSTO 3', service_type: 1, price: 180.0, has_cost: false, currency_id: 2, service_mode_id: 4)

access = Access.find_by_name('manage')

agents_module = SystemModule.find_by_name('Agent')
clients_module = SystemModule.find_by_name('Client')
criteria_module = SystemModule.find_by_name('Criterium')
containers_module = SystemModule.find_by_name('Container')
documents_module = SystemModule.find_by_name('Document')
middlemen_module = SystemModule.find_by_name('Middleman')
packaging_types_module = SystemModule.find_by_name('PackagingType')
ports_module = SystemModule.find_by_name('Port')
quotes_module = SystemModule.find_by_name('Quote')
roles_module = SystemModule.find_by_name('Role')
services_module = SystemModule.find_by_name('Service')
shipments_module = SystemModule.find_by_name('Shipment')
suppliers_module = SystemModule.find_by_name('Supplier')
tariffs_module = SystemModule.find_by_name('Tariff')
taxes_module = SystemModule.find_by_name('Tax')
users_module = SystemModule.find_by_name('User')

AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: agents_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: clients_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: criteria_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: containers_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: documents_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: tariffs_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: middlemen_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: packaging_types_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: ports_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: quotes_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: roles_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: services_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: shipments_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: suppliers_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: taxes_module.id)
AccessRole.create(role_id: rol_admin.id, access_id: access.id, system_module_id: users_module.id)
#
#
fast_forward = Supplier.find_by_name('Fast Forward')
Tariff.create(carrier_id: fast_forward.id, service_mode: 2, freight_type_id: lcl.id, started_at: '02-04-2021', ended_at: '08-04-2021')
Tariff.create(carrier_id: fast_forward.id, service_mode: 2, freight_type_id: lcl.id, started_at: '08-04-2021', ended_at: '16-04-2021')
Tariff.create(carrier_id: fast_forward.id, service_mode: 2, freight_type_id: fcl.id, started_at: '02-04-2021', ended_at: '08-04-2021')
Tariff.create(carrier_id: fast_forward.id, service_mode: 2, freight_type_id: fcl.id, started_at: '10-04-2021', ended_at: '15-04-2021')

fast_forward_tariffs = Tariff.where(carrier_id: fast_forward.id, service_mode: 2)

alexandria = Port.find_by_port_identifier('EGEDK')
veracruz = Port.find_by_port_identifier('MXVER')
manzanillo = Port.find_by_port_identifier('MXZLO')
altamira = Port.find_by_port_identifier('MXATM')
ahmedabad = Port.find_by_port_identifier('INAMD')

fast_forward_tariffs.each do |fast_forward_tariff|
  # Ahmedabad Port
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: ahmedabad.id, discharge_port_id: altamira.id, freight_rate: 124, minimal_cost: 124, transite_time: 50, frequency: 0, via_code: 'DEHAM', via: 'HAMBURGO')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: ahmedabad.id, discharge_port_id: veracruz.id, freight_rate: 134, minimal_cost: 134, transite_time: 52, frequency: 1, via_code: 'DEHAM', via: 'HAMBURGO')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: ahmedabad.id, discharge_port_id: manzanillo.id, freight_rate: 103, minimal_cost: 103, transite_time: 51, frequency: 1, via_code: 'INNSA', via: 'NHAVA SHEVA')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: ahmedabad.id, discharge_port_id: veracruz.id, freight_rate: 115, minimal_cost: 115, transite_time: 60, frequency: 1, via_code: 'NLRTM', via: 'ROTTERDAM')
  # # Alexadria Port
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: alexandria.id, discharge_port_id: altamira.id, freight_rate: 120, minimal_cost: 120, transite_time: 43, frequency: 1, via_code: 'ESBCN', via: 'BARCELONA')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: alexandria.id, discharge_port_id: veracruz.id, freight_rate: 60, minimal_cost: 60, transite_time: 45, frequency: 1, via_code: 'ESBCN', via: 'BARCELONA')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: alexandria.id, discharge_port_id: altamira.id, freight_rate: 100, minimal_cost: 100, transite_time: 41, frequency: 0, via_code: 'DEHAM', via: 'HAMBURGO')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: alexandria.id, discharge_port_id: veracruz.id, freight_rate: 100, minimal_cost: 100, transite_time: 43, frequency: 1, via_code: 'DEHAM', via: 'HAMBURGO')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: alexandria.id, discharge_port_id: veracruz.id, freight_rate: 89, minimal_cost: 89, transite_time: 36, frequency: 1, via_code: 'NLRTM', via: 'ROTTERDAM')
  Rate.create(tariff_id: fast_forward_tariff.id, origin_port_id: alexandria.id, discharge_port_id: manzanillo.id, freight_rate: 109, minimal_cost: 109, transite_time: 67, frequency: 1, via_code: 'SGSIN', via: 'SINGAPORE')
  #
end
aeromexico = Supplier.find_by_name('Aeromexico')
Tariff.create(carrier_id: aeromexico.id, service_mode: 0, started_at: '02-04-2021', ended_at: '08-04-2021')
Tariff.create(carrier_id: aeromexico.id, service_mode: 0, started_at: '02-04-2021', ended_at: '06-04-2021')
Tariff.create(carrier_id: aeromexico.id, service_mode: 0, started_at: '02-04-2021', ended_at: '06-04-2021')
Tariff.create(carrier_id: aeromexico.id, service_mode: 0, started_at: '02-04-2021', ended_at: '06-04-2021')
aeromexico_tariffs = Tariff.where(carrier_id: aeromexico.id)
atlanta = Port.find_by_port_identifier('ATL')
angeles = Port.find_by_port_identifier('LAX')
monterrey = Port.find_by_port_identifier('MTY')
mexico = Port.find_by_port_identifier('MEX')
aeromexico_tariffs.each do |aeromexico_tariff|
  Rate.create(tariff_id: aeromexico_tariff.id, origin_port_id: mexico.id, discharge_port_id: atlanta.id, freight_rate: 86, minimal_cost: 86, transite_time: 12, frequency: 0)
  Rate.create(tariff_id: aeromexico_tariff.id, origin_port_id: atlanta.id, discharge_port_id: monterrey.id, freight_rate: 65, minimal_cost: 65, transite_time: 7, frequency: 1)
  Rate.create(tariff_id: aeromexico_tariff.id, origin_port_id: mexico.id, discharge_port_id: angeles.id, freight_rate: 76, minimal_cost: 76, transite_time: 10, frequency: 1)
end


MultiService.create(supplier_id: aeromexico.id, service_mode_id: aereo.id)
MultiService.create(supplier_id: fast_forward.id, service_mode_id: maritimo.id)
MultiService.create(supplier_id: fast_forward.id, service_mode_id: aereo.id)
=begin
p = Product.new
p.name = 'Oso de Peluche Cafe 200cmx200cm'
p.save

quote = Quote.new
quote.number = 'FS-001'
quote.rate_id = 9
quote.quoted_at = '2021-04-28'
quote.expired_at = '2021-05-17'
quote.type_service = 0
quote.operation_type = 0
quote.description = 'Osos de Peluche Cafe 200cmx200cm'
quote.declared_value = 12000
quote.client_id = 1
quote.payment_term_id = 1
quote.organization_id = 1
quote.incoterm_id = 1
quote.service_mode = 2
quote.freight_type_id = 1
quote.free_day_of_delay = 0
quote.salesman_id = 1
quote.employee_id = 1
quote.total_pieces = 10000
quote.total_weight = 105000
quote.total_volume = 18480
quote.total_weight_volume = 0
quote.status = 0
quote.slug = 'fs-0001'

qu = QuoteUnit.new
qu.quote_id = 1
qu.container_id = 2
qu.packaging_type_id = 1
qu.product_id = 1
qu.number_units = 10000
qu.length = 1.1
qu.height = 1.2
qu.width = 1.4
qu.volume = 1.848
qu.weight = 10.5
=end

language = Language.find_by_code("es")
User.all.each do |user|
  Setting.create(user_id: user.id, language_id: language.id)
end