# Unidades de Longitud
# Imperial
Unit.create(name: 'Pie', unit_type: 0, code: 'ft', value: 30.48, unit_system: 0)
Unit.create(name: 'Pulgada', unit_type: 0, code: 'in', value: 2.54, unit_system: 0)
Unit.create(name: 'Yarda', unit_type: 0, code: 'yd', value: 91.44, unit_system: 0)
# Metrico
Unit.create(name: 'Metro', unit_type: 0, code: 'm', value: 1, unit_system: 1)
Unit.create(name: 'Centímetro', unit_type: 0, code: 'cm', value: 0.01, unit_system: 1)

#Unidades de Peso
# Imperial
Unit.create(name: 'Libras', unit_type: 1, code: 'lbs', value: 0, unit_system: 0)
Unit.create(name: 'Onza', unit_type: 1, code: 'oz', value: 0, unit_system: 0)
# Metrico
Unit.create(name: 'Kilogramo', unit_type: 1, code: 'kg', value: 1000, unit_system: 1)
Unit.create(name: 'Gramo', unit_type: 1, code: 'g', value: 1, unit_system: 1)
Unit.create(name: 'Decigramo', unit_type: 1, code: 'dg', value: 0.1, unit_system: 1)
Unit.create(name: 'Centigramo', unit_type: 1, code: 'cg', value: 0.01, unit_system: 1)

#Unidades de Volumen
# Imperial
Unit.create(name: 'Pie Cúbico', unit_type: 2, code: 'ft³', value: 0, unit_system: 0)
Unit.create(name: 'Pulgada Cúbica', unit_type: 2, code: 'in³', value: 0, unit_system: 0)
Unit.create(name: 'Yarda Cúbica', unit_type: 2, code: 'yd³', value: 0, unit_system: 0)
#Metrico
Unit.create(name: 'Metro Cúbico', unit_type: 2, code: 'm³', value: 1, unit_system: 1)
Unit.create(name: 'Centimetro Cúbico', unit_type: 2, code: 'cm³', value: 0.000001, unit_system: 1)
