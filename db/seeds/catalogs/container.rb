Container.create(
    code: "20' DC",
    name: "20' DRY CARGO",
    description: 'Standard 20’ containers are manufactured with one set of double doors on one end of the container and are generally ideal to transport weighty cargo such as sugar, machinery and paper, rather than voluminous cargo.',
)

Container.create(
    code: "40' DC",
    name: "40' DRY CARGO",
    description: 'Standard 40’ Containers are manufactured with one set of double doors on one end of the container and are generally ideal to transport voluminous cargo such as furniture, cotton, tobacco, rather than heavy cargo.',
)

Container.create(
    code: "40' HC",
    name: "40' HIGH CUBE DRY CARGO",
    description: '40’ Containers are manufactured with one set of double doors on one end of the container and are generally ideal to transport voluminous cargo such as furniture, cotton, tobacco, rather than heavy cargo. High Cube containers are manufactured 1 foot taller than a standard height container to accommodate over-height cargo.',
)
Container.find_each(&:save)