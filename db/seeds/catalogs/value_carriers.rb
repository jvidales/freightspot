rol_client = Role.find_by_name('Cliente')
address_type = AddressType.find_by_name('fiscal')

carrier = Supplier.new
carrier.name = 'Fast Forward'
carrier.website = 'www.fastforward.mx'
carrier.currency_id = 2
carrier.carrier = true
carrier.save

phone_carrier = Phone.new
phone_carrier.phoneable_type = 'Supplier'
phone_carrier.phoneable_id = carrier.id
phone_carrier.code = '+52'
phone_carrier.number = '5591269870'
phone_carrier.type_phone = 0
phone_carrier.principal = true
phone_carrier.save

address_carrier = Address.new
address_carrier.addressable_type = 'Supplier'
address_carrier.addressable_id = carrier.id
address_carrier.name = 'CDMX'
address_carrier.street = 'Alvaro Obregon'
address_carrier.interior_number = 's/n'
address_carrier.zip_code = '01710'
address_carrier.city = 'Ciudad de México'
address_carrier.country = 'Mexico'
address_carrier.state_id = 195
address_carrier.principal = true
address_carrier.save

address_carrier_type_ids = AddressAddressType.new
address_carrier_type_ids.address_type_id = address_type.id
address_carrier_type_ids.address_id = address_carrier.id
address_carrier_type_ids.save

contact_carrier = Contact.new
contact_carrier.contactable_type = 'Supplier'
contact_carrier.contactable_id = carrier.id
contact_carrier.name = 'Federico Tamez'
contact_carrier.email = 'ftamez@ffastforward.com'
contact_carrier.birth_date = '14-09-1987'
contact_carrier.job = 'Commercial Director'
contact_carrier.principal = true

u = User.new
u.username = contact_carrier.name.mentionable
u.email = contact_carrier.email
u.password = 'password'
u.role_id = rol_client.id
u.accept_terms_of_service_and_privacy_policies = true
u.admin = false
u.save

contact_carrier.user_id = u.id
contact_carrier.save

address_contact = Address.new
address_contact.addressable_type = 'Contact'
address_contact.addressable_id = contact_carrier.id
address_contact.name = 'CDMX'
address_contact.street = 'Alvaro Obregon'
address_contact.interior_number = 's/n'
address_contact.zip_code = '01710'
address_contact.city = 'Ciudad de México'
address_contact.country = 'Mexico'
address_contact.state_id = 195
address_contact.principal = true
address_contact.address_type_ids << address_type
address_contact.save

address_contact_type_ids = AddressAddressType.new
address_contact_type_ids.address_type_id = address_type.id
address_contact_type_ids.address_id = address_contact.id
address_contact_type_ids.save

phone_contact = Phone.new
phone_contact.phoneable_type = 'Contact'
phone_contact.phoneable_id = contact_carrier.id
phone_contact.code = '+52'
phone_contact.number = '5591269870'
phone_contact.type_phone = 0
phone_contact.principal = true
phone_contact.save

carrier = Supplier.new
carrier.name = 'Aeromexico'
carrier.website = 'www.aeromexico.com'
carrier.currency_id = 2
carrier.carrier = true
carrier.save

phone_carrier = Phone.new
phone_carrier.phoneable_type = 'Supplier'
phone_carrier.phoneable_id = carrier.id
phone_carrier.code = '+52'
phone_carrier.number = '5591269870'
phone_carrier.type_phone = 0
phone_carrier.principal = true
phone_carrier.save

address_carrier = Address.new
address_carrier.addressable_type = 'Supplier'
address_carrier.addressable_id = carrier.id
address_carrier.name = 'CDMX'
address_carrier.street = 'Alvaro Obregon'
address_carrier.interior_number = 's/n'
address_carrier.zip_code = '01710'
address_carrier.city = 'Ciudad de México'
address_carrier.country = 'Mexico'
address_carrier.state_id = 195
address_carrier.principal = true
address_carrier.save

address_carrier_type_ids = AddressAddressType.new
address_carrier_type_ids.address_type_id = address_type.id
address_carrier_type_ids.address_id = address_carrier.id
address_carrier_type_ids.save

contact_carrier = Contact.new
contact_carrier.contactable_type = 'Supplier'
contact_carrier.contactable_id = carrier.id
contact_carrier.name = 'Julian Osorio'
contact_carrier.email = 'josorio@aeromexico.com'
contact_carrier.birth_date = '22-03-1987'
contact_carrier.job = 'Commercial Director'
contact_carrier.principal = true

u = User.new
u.username = contact_carrier.name.mentionable
u.email = contact_carrier.email
u.password = 'password'
u.role_id = rol_client.id
u.accept_terms_of_service_and_privacy_policies = true
u.admin = false
u.save

contact_carrier.user_id = u.id
contact_carrier.save

address_contact = Address.new
address_contact.addressable_type = 'Contact'
address_contact.addressable_id = contact_carrier.id
address_contact.name = 'CDMX'
address_contact.street = 'Alvaro Obregon'
address_contact.interior_number = 's/n'
address_contact.zip_code = '01710'
address_contact.city = 'Ciudad de México'
address_contact.country = 'Mexico'
address_contact.state_id = 195
address_contact.principal = true
address_contact.address_type_ids << address_type
address_contact.save

address_contact_type_ids = AddressAddressType.new
address_contact_type_ids.address_type_id = address_type.id
address_contact_type_ids.address_id = address_contact.id
address_contact_type_ids.save

phone_contact = Phone.new
phone_contact.phoneable_type = 'Contact'
phone_contact.phoneable_id = contact_carrier.id
phone_contact.code = '+52'
phone_contact.number = '5591269870'
phone_contact.type_phone = 0
phone_contact.principal = true
phone_contact.save