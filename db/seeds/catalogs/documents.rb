Document.create(name: 'Acta Constitutiva', obligatory: true, document_type: '', phase: 'register',
                  active: true, variable: 'acta_constitutiva')
Document.create(name: 'Alta del RFC', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'alta_rfc')
Document.create(name: 'Carátula Bancaria', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'caratula_bancaria')
Document.create(name: 'Carta Dueño Beneficiario', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'carta_dueño_beneficiario')
Document.create(name: 'Carta de Encomienda', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'carta_encomienda')
Document.create(name: 'Comprobante de Docimicilio', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'comprobante_domicilio')
Document.create(name: 'Constancia de Actividades Vulnerables', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'constancia_actividades_vulnerables')
Document.create(name: 'Constancia de Situación Fiscal', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'constancia_situacion_fiscal')
Document.create(name: 'Identificación del Representante Legal', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'identificacion_representante_legal')
Document.create(name: 'Opinión Positiva del SAT', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'opinion_positiva_sat')
Document.create(name: 'Poder Notarial', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'poder_notarial')
Document.create(name: 'Solicitud de Crédito', obligatory: true, document_type: '', phase: 'register',
                active: true, variable: 'solicitud_credito')
