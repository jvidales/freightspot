# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_28_192132) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_roles", force: :cascade do |t|
    t.bigint "access_id"
    t.bigint "role_id"
    t.bigint "system_module_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["access_id"], name: "index_access_roles_on_access_id"
    t.index ["deleted_at"], name: "index_access_roles_on_deleted_at"
    t.index ["role_id"], name: "index_access_roles_on_role_id"
    t.index ["system_module_id"], name: "index_access_roles_on_system_module_id"
  end

  create_table "accesses", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_accesses_on_deleted_at"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "address_address_types", force: :cascade do |t|
    t.bigint "address_id"
    t.bigint "address_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["address_id"], name: "index_address_address_types_on_address_id"
    t.index ["address_type_id"], name: "index_address_address_types_on_address_type_id"
    t.index ["deleted_at"], name: "index_address_address_types_on_deleted_at"
  end

  create_table "address_types", force: :cascade do |t|
    t.string "name"
    t.boolean "inactive", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_address_types_on_deleted_at"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.string "name"
    t.string "street"
    t.string "interior_number"
    t.string "outdoor_number"
    t.string "colony"
    t.string "city"
    t.string "country"
    t.bigint "state_id"
    t.string "zip_code"
    t.boolean "principal", default: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
    t.index ["deleted_at"], name: "index_addresses_on_deleted_at"
    t.index ["slug"], name: "index_addresses_on_slug"
    t.index ["state_id"], name: "index_addresses_on_state_id"
  end

  create_table "agent_documents", force: :cascade do |t|
    t.bigint "agent_id"
    t.bigint "document_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["agent_id"], name: "index_agent_documents_on_agent_id"
    t.index ["deleted_at"], name: "index_agent_documents_on_deleted_at"
    t.index ["document_id"], name: "index_agent_documents_on_document_id"
  end

  create_table "agents", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "rfc"
    t.bigint "payment_term_id"
    t.bigint "currency_id"
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.boolean "inactive", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["cfdi_id"], name: "index_agents_on_cfdi_id"
    t.index ["currency_id"], name: "index_agents_on_currency_id"
    t.index ["deleted_at"], name: "index_agents_on_deleted_at"
    t.index ["payment_method_id"], name: "index_agents_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_agents_on_payment_term_id"
    t.index ["way_pay_id"], name: "index_agents_on_way_pay_id"
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string "bank"
    t.string "account_number"
    t.string "owner"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["client_id"], name: "index_bank_accounts_on_client_id"
    t.index ["deleted_at"], name: "index_bank_accounts_on_deleted_at"
  end

  create_table "branches", force: :cascade do |t|
    t.string "name"
    t.bigint "organization_id"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_branches_on_deleted_at"
    t.index ["organization_id"], name: "index_branches_on_organization_id"
    t.index ["slug"], name: "index_branches_on_slug", unique: true
  end

  create_table "cfdis", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.boolean "physical", default: false
    t.boolean "moral", default: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_cfdis_on_deleted_at"
  end

  create_table "charges", force: :cascade do |t|
    t.string "chargeable_type"
    t.bigint "chargeable_id"
    t.bigint "service_id"
    t.bigint "currency_id"
    t.bigint "tax_id"
    t.text "description"
    t.string "apply_to_able_type"
    t.string "apply_type"
    t.bigint "apply_to_able_id"
    t.integer "apply_by"
    t.bigint "type_charge"
    t.integer "number_pieces", default: 1
    t.decimal "total_weight", precision: 20, scale: 4, default: "0.0"
    t.decimal "total_volume", precision: 20, scale: 4, default: "0.0"
    t.decimal "weight_to_charge", precision: 20, scale: 4, default: "0.0"
    t.decimal "quantity", precision: 20, scale: 4, default: "0.0"
    t.string "unit"
    t.decimal "price", precision: 20, scale: 4, default: "0.0"
    t.decimal "total", precision: 20, scale: 4, default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["chargeable_type", "chargeable_id"], name: "index_charges_on_chargeable_type_and_chargeable_id"
    t.index ["currency_id"], name: "index_charges_on_currency_id"
    t.index ["deleted_at"], name: "index_charges_on_deleted_at"
    t.index ["service_id"], name: "index_charges_on_service_id"
    t.index ["tax_id"], name: "index_charges_on_tax_id"
  end

  create_table "client_documents", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "document_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_client_documents_on_client_id"
    t.index ["deleted_at"], name: "index_client_documents_on_deleted_at"
    t.index ["document_id"], name: "index_client_documents_on_document_id"
  end

  create_table "client_products", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "product_id"
    t.decimal "tariff_fraction"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["client_id"], name: "index_client_products_on_client_id"
    t.index ["deleted_at"], name: "index_client_products_on_deleted_at"
    t.index ["product_id"], name: "index_client_products_on_product_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "rfc"
    t.bigint "payment_term_id"
    t.boolean "has_credit", default: false
    t.bigint "currency_id"
    t.decimal "credit_limit", precision: 10, scale: 4, default: "0.0"
    t.bigint "parent_entity_id"
    t.boolean "authorized_credit", default: false
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.boolean "inactive", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.bigint "middleman_id"
    t.bigint "annual_international_freight_shipments"
    t.boolean "advanced_parameters"
    t.index ["cfdi_id"], name: "index_clients_on_cfdi_id"
    t.index ["currency_id"], name: "index_clients_on_currency_id"
    t.index ["deleted_at"], name: "index_clients_on_deleted_at"
    t.index ["middleman_id"], name: "index_clients_on_middleman_id"
    t.index ["parent_entity_id"], name: "index_clients_on_parent_entity_id"
    t.index ["payment_method_id"], name: "index_clients_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_clients_on_payment_term_id"
    t.index ["slug"], name: "index_clients_on_slug"
    t.index ["way_pay_id"], name: "index_clients_on_way_pay_id"
  end

  create_table "collection_routes", force: :cascade do |t|
    t.bigint "shipment_id"
    t.bigint "origin_port_id"
    t.string "origin"
    t.decimal "latitude"
    t.decimal "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_collection_routes_on_deleted_at"
    t.index ["origin_port_id"], name: "index_collection_routes_on_origin_port_id"
    t.index ["shipment_id"], name: "index_collection_routes_on_shipment_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "contactable_type"
    t.bigint "contactable_id"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "area"
    t.string "job"
    t.datetime "birth_date"
    t.boolean "principal", default: false
    t.string "slug"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["contactable_type", "contactable_id"], name: "index_contacts_on_contactable_type_and_contactable_id"
    t.index ["deleted_at"], name: "index_contacts_on_deleted_at"
    t.index ["slug"], name: "index_contacts_on_slug"
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "container_dimensions", force: :cascade do |t|
    t.string "name"
    t.decimal "metric_value"
    t.bigint "metric_measurement_id"
    t.decimal "imperial_value"
    t.bigint "imperial_measurement_id"
    t.bigint "container_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_container_dimensions_on_container_id"
    t.index ["deleted_at"], name: "index_container_dimensions_on_deleted_at"
    t.index ["imperial_measurement_id"], name: "index_container_dimensions_on_imperial_measurement_id"
    t.index ["metric_measurement_id"], name: "index_container_dimensions_on_metric_measurement_id"
  end

  create_table "containers", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "description"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_containers_on_deleted_at"
    t.index ["slug"], name: "index_containers_on_slug", unique: true
  end

  create_table "countries", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "phonecode"
    t.boolean "prohibited", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_countries_on_deleted_at"
  end

  create_table "criteria", force: :cascade do |t|
    t.string "name"
    t.decimal "amount"
    t.bigint "currency_id"
    t.datetime "validity_start"
    t.datetime "validity_end"
    t.text "remarks"
    t.boolean "advanced_parameters", default: false
    t.bigint "origin_id"
    t.bigint "destiny_id"
    t.bigint "container_id"
    t.bigint "status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_criteria_on_container_id"
    t.index ["currency_id"], name: "index_criteria_on_currency_id"
    t.index ["deleted_at"], name: "index_criteria_on_deleted_at"
    t.index ["destiny_id"], name: "index_criteria_on_destiny_id"
    t.index ["origin_id"], name: "index_criteria_on_origin_id"
  end

  create_table "criterium_rates", force: :cascade do |t|
    t.bigint "criterium_id"
    t.bigint "rate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["criterium_id"], name: "index_criterium_rates_on_criterium_id"
    t.index ["deleted_at"], name: "index_criterium_rates_on_deleted_at"
    t.index ["rate_id"], name: "index_criterium_rates_on_rate_id"
  end

  create_table "currencies", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_currencies_on_deleted_at"
  end

  create_table "delivery_routes", force: :cascade do |t|
    t.bigint "shipment_id"
    t.bigint "destiny_port_id"
    t.string "destiny"
    t.decimal "latitude"
    t.decimal "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_delivery_routes_on_deleted_at"
    t.index ["destiny_port_id"], name: "index_delivery_routes_on_destiny_port_id"
    t.index ["shipment_id"], name: "index_delivery_routes_on_shipment_id"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.bigint "branch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["branch_id"], name: "index_departments_on_branch_id"
    t.index ["deleted_at"], name: "index_departments_on_deleted_at"
  end

  create_table "destiny_routes", force: :cascade do |t|
    t.bigint "quote_id"
    t.bigint "destiny_port_id"
    t.string "destiny"
    t.decimal "latitude"
    t.decimal "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_destiny_routes_on_deleted_at"
    t.index ["destiny_port_id"], name: "index_destiny_routes_on_destiny_port_id"
    t.index ["quote_id"], name: "index_destiny_routes_on_quote_id"
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.boolean "obligatory"
    t.string "document_type"
    t.string "phase"
    t.boolean "active", default: true
    t.string "slug"
    t.string "variable"
    t.boolean "client", default: true
    t.boolean "supplier", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_documents_on_deleted_at"
  end

  create_table "employees", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "birth_date"
    t.string "phone"
    t.string "phone_extension"
    t.string "mobile"
    t.string "origin"
    t.string "city"
    t.string "office_branch"
    t.string "squad"
    t.string "job_title"
    t.datetime "join_date"
    t.string "language"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_employees_on_deleted_at"
    t.index ["user_id"], name: "index_employees_on_user_id"
  end

  create_table "entities", force: :cascade do |t|
    t.string "name"
    t.string "locale"
    t.string "sender"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_entities_on_deleted_at"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.string "detail"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_events_on_deleted_at"
    t.index ["slug"], name: "index_events_on_slug"
  end

  create_table "incoterms", force: :cascade do |t|
    t.string "code"
    t.string "description"
    t.boolean "inactive", default: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_incoterms_on_deleted_at"
    t.index ["slug"], name: "index_incoterms_on_slug"
  end

  create_table "languages", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.boolean "default", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_languages_on_deleted_at"
  end

  create_table "middlemen", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "rfc"
    t.bigint "payment_term_id"
    t.bigint "currency_id"
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.boolean "inactive", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cfdi_id"], name: "index_middlemen_on_cfdi_id"
    t.index ["currency_id"], name: "index_middlemen_on_currency_id"
    t.index ["deleted_at"], name: "index_middlemen_on_deleted_at"
    t.index ["payment_method_id"], name: "index_middlemen_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_middlemen_on_payment_term_id"
    t.index ["way_pay_id"], name: "index_middlemen_on_way_pay_id"
  end

  create_table "mode_transportations", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "service_mode_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_mode_transportations_on_deleted_at"
    t.index ["service_mode_id"], name: "index_mode_transportations_on_service_mode_id"
  end

  create_table "multi_services", force: :cascade do |t|
    t.bigint "supplier_id"
    t.bigint "service_mode_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_multi_services_on_deleted_at"
    t.index ["service_mode_id"], name: "index_multi_services_on_service_mode_id"
    t.index ["supplier_id"], name: "index_multi_services_on_supplier_id"
  end

  create_table "notes", force: :cascade do |t|
    t.string "noteable_type"
    t.bigint "noteable_id"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_notes_on_deleted_at"
    t.index ["noteable_type", "noteable_id"], name: "index_notes_on_noteable_type_and_noteable_id"
  end

  create_table "optional_services", force: :cascade do |t|
    t.string "name"
    t.decimal "cost"
    t.string "category"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_optional_services_on_deleted_at"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_organizations_on_deleted_at"
    t.index ["slug"], name: "index_organizations_on_slug", unique: true
  end

  create_table "origin_routes", force: :cascade do |t|
    t.bigint "quote_id"
    t.bigint "origin_port_id"
    t.string "origin"
    t.decimal "latitude"
    t.decimal "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_origin_routes_on_deleted_at"
    t.index ["origin_port_id"], name: "index_origin_routes_on_origin_port_id"
    t.index ["quote_id"], name: "index_origin_routes_on_quote_id"
  end

  create_table "packaging_types", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "definition"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["code"], name: "index_packaging_types_on_code"
    t.index ["deleted_at"], name: "index_packaging_types_on_deleted_at"
    t.index ["slug"], name: "index_packaging_types_on_slug", unique: true
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_payment_methods_on_deleted_at"
  end

  create_table "payment_terms", force: :cascade do |t|
    t.string "description"
    t.integer "amount_days_pay"
    t.boolean "inactive", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_payment_terms_on_deleted_at"
  end

  create_table "phones", force: :cascade do |t|
    t.string "phoneable_type"
    t.bigint "phoneable_id"
    t.string "code"
    t.string "number"
    t.string "extension"
    t.integer "type_phone"
    t.boolean "principal", default: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_phones_on_deleted_at"
    t.index ["phoneable_type", "phoneable_id"], name: "index_phones_on_phoneable_type_and_phoneable_id"
    t.index ["slug"], name: "index_phones_on_slug"
  end

  create_table "ports", force: :cascade do |t|
    t.string "port_identifier"
    t.string "iata_code"
    t.string "name"
    t.integer "transportation_method"
    t.boolean "inactive", default: false
    t.bigint "country_id"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.decimal "latitude"
    t.decimal "longitude"
    t.index ["country_id"], name: "index_ports_on_country_id"
    t.index ["deleted_at"], name: "index_ports_on_deleted_at"
    t.index ["slug"], name: "index_ports_on_slug"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_products_on_deleted_at"
  end

  create_table "quote_optional_services", force: :cascade do |t|
    t.bigint "quote_id"
    t.bigint "optional_service_id"
    t.boolean "applied_charge", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_quote_optional_services_on_deleted_at"
    t.index ["optional_service_id"], name: "index_quote_optional_services_on_optional_service_id"
    t.index ["quote_id"], name: "index_quote_optional_services_on_quote_id"
  end

  create_table "quote_units", force: :cascade do |t|
    t.bigint "quote_id"
    t.bigint "packaging_type_id"
    t.bigint "product_id"
    t.bigint "service_id"
    t.bigint "container_id"
    t.bigint "number_units", default: 1
    t.decimal "length", precision: 10, scale: 4, default: "0.0"
    t.decimal "height", precision: 10, scale: 4, default: "0.0"
    t.decimal "width", precision: 10, scale: 4, default: "0.0"
    t.decimal "weight", precision: 10, scale: 4, default: "0.0"
    t.decimal "volume", precision: 20, scale: 4, default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_quote_units_on_container_id"
    t.index ["deleted_at"], name: "index_quote_units_on_deleted_at"
    t.index ["packaging_type_id"], name: "index_quote_units_on_packaging_type_id"
    t.index ["product_id"], name: "index_quote_units_on_product_id"
    t.index ["quote_id"], name: "index_quote_units_on_quote_id"
    t.index ["service_id"], name: "index_quote_units_on_service_id"
  end

  create_table "quotes", force: :cascade do |t|
    t.string "number"
    t.bigint "rate_id"
    t.datetime "quoted_at"
    t.datetime "expired_at"
    t.integer "operation_type"
    t.text "description"
    t.decimal "declared_value", precision: 10, scale: 4, default: "0.0"
    t.bigint "client_id"
    t.bigint "payment_term_id"
    t.bigint "organization_id"
    t.bigint "incoterm_id"
    t.bigint "service_mode"
    t.bigint "freight_type_id"
    t.string "service_type"
    t.bigint "frequency"
    t.boolean "danger_item", default: false
    t.integer "free_day_of_delay"
    t.bigint "salesman_id"
    t.bigint "employee_id"
    t.decimal "total_pieces", precision: 10, scale: 4, default: "0.0"
    t.decimal "total_weight", precision: 10, scale: 4, default: "0.0"
    t.decimal "total_volume", precision: 20, scale: 4, default: "0.0"
    t.decimal "total_weight_volume", precision: 20, scale: 4, default: "0.0"
    t.boolean "guarantee_letter", default: false
    t.boolean "is_active_shipment", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.boolean "authorized", default: false
    t.index ["client_id"], name: "index_quotes_on_client_id"
    t.index ["deleted_at"], name: "index_quotes_on_deleted_at"
    t.index ["employee_id"], name: "index_quotes_on_employee_id"
    t.index ["freight_type_id"], name: "index_quotes_on_freight_type_id"
    t.index ["incoterm_id"], name: "index_quotes_on_incoterm_id"
    t.index ["organization_id"], name: "index_quotes_on_organization_id"
    t.index ["payment_term_id"], name: "index_quotes_on_payment_term_id"
    t.index ["rate_id"], name: "index_quotes_on_rate_id"
    t.index ["salesman_id"], name: "index_quotes_on_salesman_id"
    t.index ["slug"], name: "index_quotes_on_slug"
  end

  create_table "rate_surcharges", force: :cascade do |t|
    t.bigint "rate_id"
    t.bigint "surcharge_id"
    t.bigint "currency_id"
    t.decimal "value", precision: 10, scale: 4, default: "0.0"
    t.string "type_surcharge"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["currency_id"], name: "index_rate_surcharges_on_currency_id"
    t.index ["deleted_at"], name: "index_rate_surcharges_on_deleted_at"
    t.index ["rate_id"], name: "index_rate_surcharges_on_rate_id"
    t.index ["surcharge_id"], name: "index_rate_surcharges_on_surcharge_id"
  end

  create_table "rates", force: :cascade do |t|
    t.bigint "tariff_id"
    t.bigint "origin_port_id"
    t.bigint "discharge_port_id"
    t.decimal "freight_rate", precision: 20, scale: 4, default: "0.0"
    t.decimal "minimal_cost", precision: 20, scale: 4, default: "0.0"
    t.bigint "container_id"
    t.string "measurement_unit"
    t.integer "transite_time"
    t.bigint "frequency"
    t.string "via_code"
    t.string "via"
    t.integer "free_days"
    t.boolean "guarantee_letter", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_rates_on_container_id"
    t.index ["deleted_at"], name: "index_rates_on_deleted_at"
    t.index ["discharge_port_id"], name: "index_rates_on_discharge_port_id"
    t.index ["origin_port_id"], name: "index_rates_on_origin_port_id"
    t.index ["tariff_id"], name: "index_rates_on_tariff_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_roles_on_deleted_at"
    t.index ["slug"], name: "index_roles_on_slug"
  end

  create_table "sender_sender_types", force: :cascade do |t|
    t.bigint "sender_id"
    t.bigint "sender_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_sender_sender_types_on_deleted_at"
    t.index ["sender_id"], name: "index_sender_sender_types_on_sender_id"
    t.index ["sender_type_id"], name: "index_sender_sender_types_on_sender_type_id"
  end

  create_table "sender_types", force: :cascade do |t|
    t.string "name"
    t.boolean "inactive", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_sender_types_on_deleted_at"
  end

  create_table "senders", force: :cascade do |t|
    t.string "senderable_type"
    t.bigint "senderable_id"
    t.string "contact"
    t.string "name"
    t.string "rfc"
    t.string "email"
    t.string "phone"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_senders_on_deleted_at"
    t.index ["senderable_type", "senderable_id"], name: "index_senders_on_senderable_type_and_senderable_id"
  end

  create_table "service_modes", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "variable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_service_modes_on_deleted_at"
  end

  create_table "service_types", force: :cascade do |t|
    t.bigint "incoterm_id"
    t.string "operation_type"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_service_types_on_deleted_at"
    t.index ["incoterm_id"], name: "index_service_types_on_incoterm_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "code"
    t.text "description"
    t.integer "service_type"
    t.decimal "price", precision: 10, scale: 4, default: "0.0"
    t.boolean "has_cost", default: false
    t.bigint "currency_id"
    t.bigint "tax_id"
    t.bigint "service_mode_id"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["code"], name: "index_services_on_code"
    t.index ["currency_id"], name: "index_services_on_currency_id"
    t.index ["deleted_at"], name: "index_services_on_deleted_at"
    t.index ["service_mode_id"], name: "index_services_on_service_mode_id"
    t.index ["slug"], name: "index_services_on_slug", unique: true
    t.index ["tax_id"], name: "index_services_on_tax_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "settings", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "language_id"
    t.string "time_zone", default: "Mexico City"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_settings_on_deleted_at"
    t.index ["language_id"], name: "index_settings_on_language_id"
    t.index ["user_id"], name: "index_settings_on_user_id"
  end

  create_table "shipment_units", force: :cascade do |t|
    t.bigint "shipment_id"
    t.bigint "packaging_type_id"
    t.bigint "product_id"
    t.bigint "container_id"
    t.bigint "number_units", default: 1
    t.decimal "length", precision: 10, scale: 4, default: "0.0"
    t.decimal "height", precision: 10, scale: 4, default: "0.0"
    t.decimal "width", precision: 10, scale: 4, default: "0.0"
    t.decimal "weight", precision: 10, scale: 4, default: "0.0"
    t.decimal "volume", precision: 20, scale: 4, default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_shipment_units_on_container_id"
    t.index ["deleted_at"], name: "index_shipment_units_on_deleted_at"
    t.index ["packaging_type_id"], name: "index_shipment_units_on_packaging_type_id"
    t.index ["product_id"], name: "index_shipment_units_on_product_id"
    t.index ["shipment_id"], name: "index_shipment_units_on_shipment_id"
  end

  create_table "shipments", force: :cascade do |t|
    t.string "qr_code"
    t.string "bar_code"
    t.string "number"
    t.string "guide_number"
    t.string "reservation_number"
    t.bigint "quote_id"
    t.datetime "realized_at"
    t.datetime "departured_at"
    t.datetime "estimated_departured_at"
    t.datetime "arrivaled_at"
    t.datetime "estimated_arrivaled_at"
    t.integer "type_service"
    t.integer "operation_type"
    t.text "description"
    t.decimal "declared_value", precision: 10, scale: 4, default: "0.0"
    t.bigint "service_mode"
    t.bigint "incoterm_id"
    t.bigint "freight_type_id"
    t.bigint "client_id"
    t.bigint "frequency"
    t.boolean "danger_item", default: false
    t.integer "free_day_of_delay"
    t.decimal "total_pieces", precision: 10, scale: 4, default: "0.0"
    t.decimal "total_weight", precision: 10, scale: 4, default: "0.0"
    t.decimal "total_volume", precision: 20, scale: 4, default: "0.0"
    t.decimal "chargeable_weight", precision: 20, scale: 4, default: "0.0"
    t.bigint "salesman_id"
    t.bigint "employee_id"
    t.boolean "guarantee_letter", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["client_id"], name: "index_shipments_on_client_id"
    t.index ["deleted_at"], name: "index_shipments_on_deleted_at"
    t.index ["employee_id"], name: "index_shipments_on_employee_id"
    t.index ["freight_type_id"], name: "index_shipments_on_freight_type_id"
    t.index ["incoterm_id"], name: "index_shipments_on_incoterm_id"
    t.index ["quote_id"], name: "index_shipments_on_quote_id"
    t.index ["salesman_id"], name: "index_shipments_on_salesman_id"
    t.index ["slug"], name: "index_shipments_on_slug"
  end

  create_table "ships", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_ships_on_deleted_at"
  end

  create_table "states", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["country_id"], name: "index_states_on_country_id"
    t.index ["deleted_at"], name: "index_states_on_deleted_at"
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "rfc"
    t.string "iata_code"
    t.string "caat_code"
    t.string "scac_number"
    t.boolean "inactive", default: false
    t.boolean "has_credit", default: false
    t.bigint "payment_term_id"
    t.bigint "currency_id"
    t.decimal "credit_limit", precision: 10, scale: 4, default: "0.0"
    t.bigint "parent_entity_id"
    t.text "notes"
    t.boolean "authorized_credit", default: false
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.boolean "carrier", default: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["cfdi_id"], name: "index_suppliers_on_cfdi_id"
    t.index ["currency_id"], name: "index_suppliers_on_currency_id"
    t.index ["deleted_at"], name: "index_suppliers_on_deleted_at"
    t.index ["parent_entity_id"], name: "index_suppliers_on_parent_entity_id"
    t.index ["payment_method_id"], name: "index_suppliers_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_suppliers_on_payment_term_id"
    t.index ["slug"], name: "index_suppliers_on_slug"
    t.index ["way_pay_id"], name: "index_suppliers_on_way_pay_id"
  end

  create_table "surcharges", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.boolean "active", default: true
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_surcharges_on_deleted_at"
    t.index ["slug"], name: "index_surcharges_on_slug"
  end

  create_table "system_modules", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_system_modules_on_deleted_at"
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "tariffs", force: :cascade do |t|
    t.bigint "carrier_id"
    t.bigint "service_mode"
    t.bigint "freight_type_id"
    t.bigint "cargo_type"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["carrier_id"], name: "index_tariffs_on_carrier_id"
    t.index ["deleted_at"], name: "index_tariffs_on_deleted_at"
    t.index ["freight_type_id"], name: "index_tariffs_on_freight_type_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "description"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_tasks_on_deleted_at"
    t.index ["slug"], name: "index_tasks_on_slug"
  end

  create_table "taxes", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.decimal "value", precision: 10, scale: 4, default: "0.0"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["code"], name: "index_taxes_on_code"
    t.index ["deleted_at"], name: "index_taxes_on_deleted_at"
    t.index ["slug"], name: "index_taxes_on_slug", unique: true
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.bigint "department_id"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_teams_on_deleted_at"
    t.index ["department_id"], name: "index_teams_on_department_id"
    t.index ["slug"], name: "index_teams_on_slug", unique: true
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.bigint "unit_type"
    t.string "code"
    t.integer "value", default: 0
    t.bigint "unit_system"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_units_on_deleted_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.bigint "role_id"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.string "time_zone", default: "Mexico City"
    t.datetime "accepted_terms_at"
    t.datetime "accepted_privacy_at"
    t.datetime "announcements_read_at"
    t.datetime "last_seen_at"
    t.boolean "admin", default: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.boolean "approved", default: false
    t.boolean "accept_terms_of_service_and_privacy_policies", default: false
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email"
    t.index ["invitation_token"], name: "index_users_on_invitation_token"
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token"
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["slug"], name: "index_users_on_slug"
    t.index ["username"], name: "index_users_on_username"
  end

  create_table "warehouses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_warehouses_on_deleted_at"
  end

  create_table "way_pays", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_way_pays_on_deleted_at"
  end

  add_foreign_key "access_roles", "accesses"
  add_foreign_key "access_roles", "roles"
  add_foreign_key "access_roles", "system_modules"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "address_address_types", "address_types"
  add_foreign_key "address_address_types", "addresses"
  add_foreign_key "addresses", "states"
  add_foreign_key "agent_documents", "agents"
  add_foreign_key "agent_documents", "documents"
  add_foreign_key "agents", "cfdis"
  add_foreign_key "agents", "currencies"
  add_foreign_key "agents", "payment_methods"
  add_foreign_key "agents", "payment_terms"
  add_foreign_key "agents", "way_pays"
  add_foreign_key "bank_accounts", "clients"
  add_foreign_key "branches", "organizations"
  add_foreign_key "charges", "currencies"
  add_foreign_key "charges", "services"
  add_foreign_key "charges", "taxes"
  add_foreign_key "client_documents", "clients"
  add_foreign_key "client_documents", "documents"
  add_foreign_key "client_products", "clients"
  add_foreign_key "client_products", "products"
  add_foreign_key "clients", "cfdis"
  add_foreign_key "clients", "clients", column: "parent_entity_id"
  add_foreign_key "clients", "currencies"
  add_foreign_key "clients", "middlemen"
  add_foreign_key "clients", "payment_methods"
  add_foreign_key "clients", "payment_terms"
  add_foreign_key "clients", "way_pays"
  add_foreign_key "collection_routes", "ports", column: "origin_port_id"
  add_foreign_key "collection_routes", "shipments"
  add_foreign_key "contacts", "users"
  add_foreign_key "container_dimensions", "containers"
  add_foreign_key "container_dimensions", "units", column: "imperial_measurement_id"
  add_foreign_key "container_dimensions", "units", column: "metric_measurement_id"
  add_foreign_key "criteria", "containers"
  add_foreign_key "criteria", "currencies"
  add_foreign_key "criteria", "ports", column: "destiny_id"
  add_foreign_key "criteria", "ports", column: "origin_id"
  add_foreign_key "criterium_rates", "criteria"
  add_foreign_key "criterium_rates", "rates"
  add_foreign_key "delivery_routes", "ports", column: "destiny_port_id"
  add_foreign_key "delivery_routes", "shipments"
  add_foreign_key "departments", "branches"
  add_foreign_key "destiny_routes", "ports", column: "destiny_port_id"
  add_foreign_key "destiny_routes", "quotes"
  add_foreign_key "employees", "users"
  add_foreign_key "middlemen", "cfdis"
  add_foreign_key "middlemen", "currencies"
  add_foreign_key "middlemen", "payment_methods"
  add_foreign_key "middlemen", "payment_terms"
  add_foreign_key "middlemen", "way_pays"
  add_foreign_key "mode_transportations", "service_modes"
  add_foreign_key "multi_services", "service_modes"
  add_foreign_key "multi_services", "suppliers"
  add_foreign_key "origin_routes", "ports", column: "origin_port_id"
  add_foreign_key "origin_routes", "quotes"
  add_foreign_key "ports", "countries"
  add_foreign_key "quote_optional_services", "optional_services"
  add_foreign_key "quote_optional_services", "quotes"
  add_foreign_key "quote_units", "containers"
  add_foreign_key "quote_units", "packaging_types"
  add_foreign_key "quote_units", "products"
  add_foreign_key "quote_units", "quotes"
  add_foreign_key "quote_units", "services"
  add_foreign_key "quotes", "clients"
  add_foreign_key "quotes", "employees"
  add_foreign_key "quotes", "employees", column: "salesman_id"
  add_foreign_key "quotes", "incoterms"
  add_foreign_key "quotes", "mode_transportations", column: "freight_type_id"
  add_foreign_key "quotes", "organizations"
  add_foreign_key "quotes", "payment_terms"
  add_foreign_key "quotes", "rates"
  add_foreign_key "rate_surcharges", "currencies"
  add_foreign_key "rate_surcharges", "rates"
  add_foreign_key "rate_surcharges", "surcharges"
  add_foreign_key "rates", "containers"
  add_foreign_key "rates", "ports", column: "discharge_port_id"
  add_foreign_key "rates", "ports", column: "origin_port_id"
  add_foreign_key "rates", "tariffs"
  add_foreign_key "sender_sender_types", "sender_types"
  add_foreign_key "sender_sender_types", "senders"
  add_foreign_key "service_types", "incoterms"
  add_foreign_key "services", "currencies"
  add_foreign_key "services", "service_modes"
  add_foreign_key "services", "taxes"
  add_foreign_key "settings", "languages"
  add_foreign_key "settings", "users"
  add_foreign_key "shipment_units", "containers"
  add_foreign_key "shipment_units", "packaging_types"
  add_foreign_key "shipment_units", "products"
  add_foreign_key "shipment_units", "shipments"
  add_foreign_key "shipments", "clients"
  add_foreign_key "shipments", "employees"
  add_foreign_key "shipments", "employees", column: "salesman_id"
  add_foreign_key "shipments", "incoterms"
  add_foreign_key "shipments", "mode_transportations", column: "freight_type_id"
  add_foreign_key "shipments", "quotes"
  add_foreign_key "states", "countries"
  add_foreign_key "suppliers", "cfdis"
  add_foreign_key "suppliers", "currencies"
  add_foreign_key "suppliers", "payment_methods"
  add_foreign_key "suppliers", "payment_terms"
  add_foreign_key "suppliers", "suppliers", column: "parent_entity_id"
  add_foreign_key "suppliers", "way_pays"
  add_foreign_key "taggings", "tags"
  add_foreign_key "tariffs", "mode_transportations", column: "freight_type_id"
  add_foreign_key "tariffs", "suppliers", column: "carrier_id"
  add_foreign_key "teams", "departments"
  add_foreign_key "users", "roles"
end
