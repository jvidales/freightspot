# == Schema Information
#
# Table name: events
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  detail     :string
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_events_on_deleted_at  (deleted_at)
#  index_events_on_slug        (slug)
#
require 'rails_helper'

RSpec.describe Event, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
