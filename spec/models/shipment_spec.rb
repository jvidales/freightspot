# == Schema Information
#
# Table name: shipments
#
#  id                      :bigint           not null, primary key
#  arrivaled_at            :datetime
#  bar_code                :string
#  chargeable_weight       :decimal(20, 4)   default(0.0)
#  danger_item             :boolean          default(FALSE)
#  declared_value          :decimal(10, 4)   default(0.0)
#  deleted_at              :datetime
#  departured_at           :datetime
#  description             :text
#  estimated_arrivaled_at  :datetime
#  estimated_departured_at :datetime
#  free_day_of_delay       :integer
#  frequency               :bigint
#  guarantee_letter        :boolean          default(FALSE)
#  guide_number            :string
#  number                  :string
#  operation_type          :integer
#  qr_code                 :string
#  realized_at             :datetime
#  reservation_number      :string
#  service_mode            :bigint
#  slug                    :string
#  status                  :bigint           default("waiting_collection")
#  total_pieces            :decimal(10, 4)   default(0.0)
#  total_volume            :decimal(20, 4)   default(0.0)
#  total_weight            :decimal(10, 4)   default(0.0)
#  type_service            :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  client_id               :bigint
#  employee_id             :bigint
#  freight_type_id         :bigint
#  incoterm_id             :bigint
#  quote_id                :bigint
#  salesman_id             :bigint
#
# Indexes
#
#  index_shipments_on_client_id        (client_id)
#  index_shipments_on_deleted_at       (deleted_at)
#  index_shipments_on_employee_id      (employee_id)
#  index_shipments_on_freight_type_id  (freight_type_id)
#  index_shipments_on_incoterm_id      (incoterm_id)
#  index_shipments_on_quote_id         (quote_id)
#  index_shipments_on_salesman_id      (salesman_id)
#  index_shipments_on_slug             (slug)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (freight_type_id => mode_transportations.id)
#  fk_rails_...  (incoterm_id => incoterms.id)
#  fk_rails_...  (quote_id => quotes.id)
#  fk_rails_...  (salesman_id => employees.id)
#
require 'rails_helper'

RSpec.describe Shipment, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
