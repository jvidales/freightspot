# == Schema Information
#
# Table name: users
#
#  id                                           :bigint           not null, primary key
#  accept_terms_of_service_and_privacy_policies :boolean          default(FALSE)
#  accepted_privacy_at                          :datetime
#  accepted_terms_at                            :datetime
#  admin                                        :boolean          default(FALSE)
#  announcements_read_at                        :datetime
#  approved                                     :boolean          default(FALSE)
#  current_sign_in_at                           :datetime
#  current_sign_in_ip                           :inet
#  deleted_at                                   :datetime
#  email                                        :string           default(""), not null
#  encrypted_password                           :string           default(""), not null
#  invitation_accepted_at                       :datetime
#  invitation_created_at                        :datetime
#  invitation_limit                             :integer
#  invitation_sent_at                           :datetime
#  invitation_token                             :string
#  invitations_count                            :integer          default(0)
#  invited_by_type                              :string
#  last_seen_at                                 :datetime
#  last_sign_in_at                              :datetime
#  last_sign_in_ip                              :inet
#  remember_created_at                          :datetime
#  reset_password_sent_at                       :datetime
#  reset_password_token                         :string
#  sign_in_count                                :integer          default(0), not null
#  slug                                         :string
#  time_zone                                    :string           default("Mexico City")
#  username                                     :string
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  invited_by_id                                :bigint
#  role_id                                      :bigint
#
# Indexes
#
#  index_users_on_deleted_at                         (deleted_at)
#  index_users_on_email                              (email)
#  index_users_on_invitation_token                   (invitation_token)
#  index_users_on_invitations_count                  (invitations_count)
#  index_users_on_invited_by_id                      (invited_by_id)
#  index_users_on_invited_by_type_and_invited_by_id  (invited_by_type,invited_by_id)
#  index_users_on_reset_password_token               (reset_password_token)
#  index_users_on_role_id                            (role_id)
#  index_users_on_slug                               (slug)
#  index_users_on_username                           (username)
#
# Foreign Keys
#
#  fk_rails_...  (role_id => roles.id)
#
require 'rails_helper'

RSpec.describe User, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
