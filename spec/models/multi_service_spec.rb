# == Schema Information
#
# Table name: multi_services
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint
#  supplier_id     :bigint
#
# Indexes
#
#  index_multi_services_on_deleted_at       (deleted_at)
#  index_multi_services_on_service_mode_id  (service_mode_id)
#  index_multi_services_on_supplier_id      (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#
require 'rails_helper'

RSpec.describe MultiService, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
