# == Schema Information
#
# Table name: units
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  name        :string
#  unit_system :bigint
#  unit_type   :bigint
#  value       :integer          default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_units_on_deleted_at  (deleted_at)
#
require 'rails_helper'

RSpec.describe Unit, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
