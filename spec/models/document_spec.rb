# == Schema Information
#
# Table name: documents
#
#  id            :bigint           not null, primary key
#  active        :boolean          default(TRUE)
#  client        :boolean          default(TRUE)
#  deleted_at    :datetime
#  document_type :string
#  name          :string
#  obligatory    :boolean
#  phase         :string
#  slug          :string
#  supplier      :boolean          default(TRUE)
#  variable      :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_documents_on_deleted_at  (deleted_at)
#
require 'rails_helper'

RSpec.describe Document, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
