# == Schema Information
#
# Table name: addresses
#
#  id               :bigint           not null, primary key
#  addressable_type :string
#  city             :string
#  colony           :string
#  country          :string
#  deleted_at       :datetime
#  interior_number  :string
#  name             :string
#  outdoor_number   :string
#  principal        :boolean          default(FALSE)
#  slug             :string
#  street           :string
#  zip_code         :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  addressable_id   :bigint
#  state_id         :bigint
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#  index_addresses_on_deleted_at                           (deleted_at)
#  index_addresses_on_slug                                 (slug)
#  index_addresses_on_state_id                             (state_id)
#
# Foreign Keys
#
#  fk_rails_...  (state_id => states.id)
#
require 'rails_helper'

RSpec.describe Address, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
