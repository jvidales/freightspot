# == Schema Information
#
# Table name: branches
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  name            :string
#  slug            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :bigint
#
# Indexes
#
#  index_branches_on_deleted_at       (deleted_at)
#  index_branches_on_organization_id  (organization_id)
#  index_branches_on_slug             (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (organization_id => organizations.id)
#
require 'rails_helper'

RSpec.describe Branch, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
