# == Schema Information
#
# Table name: quote_units
#
#  id                :bigint           not null, primary key
#  deleted_at        :datetime
#  height            :decimal(10, 4)   default(0.0)
#  length            :decimal(10, 4)   default(0.0)
#  number_units      :bigint           default(1)
#  volume            :decimal(20, 4)   default(0.0)
#  weight            :decimal(10, 4)   default(0.0)
#  width             :decimal(10, 4)   default(0.0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  container_id      :bigint
#  packaging_type_id :bigint
#  product_id        :bigint
#  quote_id          :bigint
#  service_id        :bigint
#
# Indexes
#
#  index_quote_units_on_container_id       (container_id)
#  index_quote_units_on_deleted_at         (deleted_at)
#  index_quote_units_on_packaging_type_id  (packaging_type_id)
#  index_quote_units_on_product_id         (product_id)
#  index_quote_units_on_quote_id           (quote_id)
#  index_quote_units_on_service_id         (service_id)
#
# Foreign Keys
#
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (packaging_type_id => packaging_types.id)
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (quote_id => quotes.id)
#  fk_rails_...  (service_id => services.id)
#
require 'rails_helper'

RSpec.describe QuoteUnit, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
