# == Schema Information
#
# Table name: teams
#
#  id            :bigint           not null, primary key
#  deleted_at    :datetime
#  name          :string
#  slug          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  department_id :bigint
#
# Indexes
#
#  index_teams_on_deleted_at     (deleted_at)
#  index_teams_on_department_id  (department_id)
#  index_teams_on_slug           (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (department_id => departments.id)
#
require 'rails_helper'

RSpec.describe Team, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
