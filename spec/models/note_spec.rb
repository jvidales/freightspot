# == Schema Information
#
# Table name: notes
#
#  id            :bigint           not null, primary key
#  deleted_at    :datetime
#  note          :text
#  noteable_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  noteable_id   :bigint
#
# Indexes
#
#  index_notes_on_deleted_at                     (deleted_at)
#  index_notes_on_noteable_type_and_noteable_id  (noteable_type,noteable_id)
#
require 'rails_helper'

RSpec.describe Note, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
