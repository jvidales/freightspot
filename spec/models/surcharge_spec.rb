# == Schema Information
#
# Table name: surcharges
#
#  id          :bigint           not null, primary key
#  active      :boolean          default(TRUE)
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_surcharges_on_deleted_at  (deleted_at)
#  index_surcharges_on_slug        (slug)
#
require 'rails_helper'

RSpec.describe Surcharge, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
