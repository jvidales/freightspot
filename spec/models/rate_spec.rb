# == Schema Information
#
# Table name: rates
#
#  id                :bigint           not null, primary key
#  deleted_at        :datetime
#  free_days         :integer
#  freight_rate      :decimal(20, 4)   default(0.0)
#  frequency         :bigint
#  guarantee_letter  :boolean          default(FALSE)
#  measurement_unit  :string
#  minimal_cost      :decimal(20, 4)   default(0.0)
#  transite_time     :integer
#  via               :string
#  via_code          :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  container_id      :bigint
#  discharge_port_id :bigint
#  origin_port_id    :bigint
#  tariff_id         :bigint
#
# Indexes
#
#  index_rates_on_container_id       (container_id)
#  index_rates_on_deleted_at         (deleted_at)
#  index_rates_on_discharge_port_id  (discharge_port_id)
#  index_rates_on_origin_port_id     (origin_port_id)
#  index_rates_on_tariff_id          (tariff_id)
#
# Foreign Keys
#
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (discharge_port_id => ports.id)
#  fk_rails_...  (origin_port_id => ports.id)
#  fk_rails_...  (tariff_id => tariffs.id)
#
require 'rails_helper'

RSpec.describe Rate, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
