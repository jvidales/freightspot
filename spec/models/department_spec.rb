# == Schema Information
#
# Table name: departments
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  branch_id  :bigint
#
# Indexes
#
#  index_departments_on_branch_id   (branch_id)
#  index_departments_on_deleted_at  (deleted_at)
#
# Foreign Keys
#
#  fk_rails_...  (branch_id => branches.id)
#
require 'rails_helper'

RSpec.describe Department, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
