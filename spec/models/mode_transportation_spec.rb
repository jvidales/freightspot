# == Schema Information
#
# Table name: mode_transportations
#
#  id              :bigint           not null, primary key
#  code            :string
#  deleted_at      :datetime
#  name            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint
#
# Indexes
#
#  index_mode_transportations_on_deleted_at       (deleted_at)
#  index_mode_transportations_on_service_mode_id  (service_mode_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#
require 'rails_helper'

RSpec.describe ModeTransportation, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
