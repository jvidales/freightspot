# == Schema Information
#
# Table name: criteria
#
#  id                  :bigint           not null, primary key
#  advanced_parameters :boolean          default(FALSE)
#  amount              :decimal(, )
#  deleted_at          :datetime
#  name                :string
#  remarks             :text
#  status              :bigint           default("active")
#  validity_end        :datetime
#  validity_start      :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  container_id        :bigint
#  currency_id         :bigint
#  destiny_id          :bigint
#  origin_id           :bigint
#
# Indexes
#
#  index_criteria_on_container_id  (container_id)
#  index_criteria_on_currency_id   (currency_id)
#  index_criteria_on_deleted_at    (deleted_at)
#  index_criteria_on_destiny_id    (destiny_id)
#  index_criteria_on_origin_id     (origin_id)
#
# Foreign Keys
#
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (destiny_id => ports.id)
#  fk_rails_...  (origin_id => ports.id)
#
require 'rails_helper'

RSpec.describe Criterium, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
