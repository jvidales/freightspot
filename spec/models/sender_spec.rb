# == Schema Information
#
# Table name: senders
#
#  id              :bigint           not null, primary key
#  address         :string
#  contact         :string
#  deleted_at      :datetime
#  email           :string
#  name            :string
#  phone           :string
#  rfc             :string
#  senderable_type :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  senderable_id   :bigint
#
# Indexes
#
#  index_senders_on_deleted_at                         (deleted_at)
#  index_senders_on_senderable_type_and_senderable_id  (senderable_type,senderable_id)
#
require 'rails_helper'

RSpec.describe Sender, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
