# == Schema Information
#
# Table name: packaging_types
#
#  id         :bigint           not null, primary key
#  code       :string
#  definition :text
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_packaging_types_on_code        (code)
#  index_packaging_types_on_deleted_at  (deleted_at)
#  index_packaging_types_on_slug        (slug) UNIQUE
#
require 'rails_helper'

RSpec.describe PackagingType, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
