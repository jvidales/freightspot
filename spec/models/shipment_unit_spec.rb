# == Schema Information
#
# Table name: shipment_units
#
#  id                :bigint           not null, primary key
#  deleted_at        :datetime
#  height            :decimal(10, 4)   default(0.0)
#  length            :decimal(10, 4)   default(0.0)
#  number_units      :bigint           default(1)
#  volume            :decimal(20, 4)   default(0.0)
#  weight            :decimal(10, 4)   default(0.0)
#  width             :decimal(10, 4)   default(0.0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  container_id      :bigint
#  packaging_type_id :bigint
#  product_id        :bigint
#  shipment_id       :bigint
#
# Indexes
#
#  index_shipment_units_on_container_id       (container_id)
#  index_shipment_units_on_deleted_at         (deleted_at)
#  index_shipment_units_on_packaging_type_id  (packaging_type_id)
#  index_shipment_units_on_product_id         (product_id)
#  index_shipment_units_on_shipment_id        (shipment_id)
#
# Foreign Keys
#
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (packaging_type_id => packaging_types.id)
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (shipment_id => shipments.id)
#
require 'rails_helper'

RSpec.describe ShipmentUnit, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
