# == Schema Information
#
# Table name: client_products
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  tariff_fraction :decimal(, )
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  client_id       :bigint
#  product_id      :bigint
#
# Indexes
#
#  index_client_products_on_client_id   (client_id)
#  index_client_products_on_deleted_at  (deleted_at)
#  index_client_products_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (product_id => products.id)
#
require 'rails_helper'

RSpec.describe ClientProduct, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
