# == Schema Information
#
# Table name: service_types
#
#  id             :bigint           not null, primary key
#  deleted_at     :datetime
#  name           :string
#  operation_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  incoterm_id    :bigint
#
# Indexes
#
#  index_service_types_on_deleted_at   (deleted_at)
#  index_service_types_on_incoterm_id  (incoterm_id)
#
# Foreign Keys
#
#  fk_rails_...  (incoterm_id => incoterms.id)
#
require 'rails_helper'

RSpec.describe ServiceType, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
