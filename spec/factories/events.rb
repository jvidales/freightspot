# == Schema Information
#
# Table name: events
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  detail     :string
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_events_on_deleted_at  (deleted_at)
#  index_events_on_slug        (slug)
#
FactoryBot.define do
  factory :event do
    name { "MyString" }
    detail { "MyString" }
    deleted_at { "2020-07-21 12:40:04" }
    slug { "MyString" }
  end
end
