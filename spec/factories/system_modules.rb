# == Schema Information
#
# Table name: system_modules
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_system_modules_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :system_module do
    name { "MyString" }
    slug { "MyString" }
    deleted_at { "2020-10-30 22:32:14" }
  end
end
