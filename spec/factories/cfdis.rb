# == Schema Information
#
# Table name: cfdis
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  moral       :boolean          default(FALSE)
#  name        :string
#  physical    :boolean          default(FALSE)
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_cfdis_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :cfdi do
    code { "MyString" }
    name { "MyString" }
    phisical { false }
    moral { false }
    deleted_at { "2020-11-19 15:32:31" }
  end
end
