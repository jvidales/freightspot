# == Schema Information
#
# Table name: delivery_routes
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  destiny         :string
#  latitude        :decimal(, )
#  longitude       :decimal(, )
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  destiny_port_id :bigint
#  shipment_id     :bigint
#
# Indexes
#
#  index_delivery_routes_on_deleted_at       (deleted_at)
#  index_delivery_routes_on_destiny_port_id  (destiny_port_id)
#  index_delivery_routes_on_shipment_id      (shipment_id)
#
# Foreign Keys
#
#  fk_rails_...  (destiny_port_id => ports.id)
#  fk_rails_...  (shipment_id => shipments.id)
#
FactoryBot.define do
  factory :delivery_route do
    shipment { nil }
    deleted_at { "2021-04-29 05:35:11" }
  end
end
