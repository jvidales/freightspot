# == Schema Information
#
# Table name: products
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_products_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :product do
    name { "MyString" }
    deleted_at { "2020-08-03 08:24:38" }
  end
end
