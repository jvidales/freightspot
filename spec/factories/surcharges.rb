# == Schema Information
#
# Table name: surcharges
#
#  id          :bigint           not null, primary key
#  active      :boolean          default(TRUE)
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_surcharges_on_deleted_at  (deleted_at)
#  index_surcharges_on_slug        (slug)
#
FactoryBot.define do
  factory :surcharge do
    code { "MyString" }
    name { "MyString" }
    slug { "MyString" }
    deleted_at { "2021-04-06 10:10:28" }
  end
end
