# == Schema Information
#
# Table name: sender_types
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  inactive   :boolean          default(FALSE)
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_sender_types_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :sender_type do
    name { "MyString" }
    inactive { "MyString" }
  end
end
