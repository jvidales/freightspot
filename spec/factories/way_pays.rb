# == Schema Information
#
# Table name: way_pays
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_way_pays_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :way_pay do
    code { "MyString" }
    name { "MyString" }
    description { "MyText" }
    slug { "MyString" }
    deleted_at { "2020-11-19 17:13:38" }
  end
end
