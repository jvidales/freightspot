# == Schema Information
#
# Table name: sender_sender_types
#
#  id             :bigint           not null, primary key
#  deleted_at     :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  sender_id      :bigint
#  sender_type_id :bigint
#
# Indexes
#
#  index_sender_sender_types_on_deleted_at      (deleted_at)
#  index_sender_sender_types_on_sender_id       (sender_id)
#  index_sender_sender_types_on_sender_type_id  (sender_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (sender_id => senders.id)
#  fk_rails_...  (sender_type_id => sender_types.id)
#
FactoryBot.define do
  factory :sender_sender_type do
    sender { nil }
    sender_type { nil }
    deleted_at { "2020-11-28 10:49:45" }
  end
end
