# == Schema Information
#
# Table name: incoterms
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :string
#  inactive    :boolean          default(FALSE)
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_incoterms_on_deleted_at  (deleted_at)
#  index_incoterms_on_slug        (slug)
#
FactoryBot.define do
  factory :incoterm do
    code { "MyString" }
    description { "MyString" }
    deleted_at { "2020-07-20 17:56:03" }
  end
end
