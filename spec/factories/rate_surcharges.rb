# == Schema Information
#
# Table name: rate_surcharges
#
#  id             :bigint           not null, primary key
#  deleted_at     :datetime
#  type_surcharge :string
#  value          :decimal(10, 4)   default(0.0)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  currency_id    :bigint
#  rate_id        :bigint
#  surcharge_id   :bigint
#
# Indexes
#
#  index_rate_surcharges_on_currency_id   (currency_id)
#  index_rate_surcharges_on_deleted_at    (deleted_at)
#  index_rate_surcharges_on_rate_id       (rate_id)
#  index_rate_surcharges_on_surcharge_id  (surcharge_id)
#
# Foreign Keys
#
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (rate_id => rates.id)
#  fk_rails_...  (surcharge_id => surcharges.id)
#
FactoryBot.define do
  factory :rate_surcharge do
    rate { nil }
    surcharge { nil }
    currency { nil }
    value { "9.99" }
    type { "" }
  end
end
