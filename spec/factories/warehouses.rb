# == Schema Information
#
# Table name: warehouses
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_warehouses_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :warehouse do
    name { "MyString" }
    deleted_at { "2021-03-31 17:46:01" }
  end
end
