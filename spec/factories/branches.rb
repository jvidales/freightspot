# == Schema Information
#
# Table name: branches
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  name            :string
#  slug            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :bigint
#
# Indexes
#
#  index_branches_on_deleted_at       (deleted_at)
#  index_branches_on_organization_id  (organization_id)
#  index_branches_on_slug             (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (organization_id => organizations.id)
#
FactoryBot.define do
  factory :branch do
    name { "MyString" }
    organization { nil }
  end
end
