FactoryBot.define do
  factory :shipment_entity do
    name { "MyString" }
    address { "MyString" }
    shipment { nil }
    entity { nil }
    deleted_at { "2020-07-30 14:03:10" }
  end
end
