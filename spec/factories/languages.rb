# == Schema Information
#
# Table name: languages
#
#  id         :bigint           not null, primary key
#  code       :string
#  default    :boolean          default(FALSE)
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_languages_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :language do
    code { "MyString" }
    name { "MyString" }
    default { false }
  end
end
