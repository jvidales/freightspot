# == Schema Information
#
# Table name: packaging_types
#
#  id         :bigint           not null, primary key
#  code       :string
#  definition :text
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_packaging_types_on_code        (code)
#  index_packaging_types_on_deleted_at  (deleted_at)
#  index_packaging_types_on_slug        (slug) UNIQUE
#
FactoryBot.define do
  factory :packaging_type do
    code { "MyString" }
    name { "MyString" }
    definition { "MyText" }
    deleted_at { "2020-07-12 14:09:22" }
  end
end
