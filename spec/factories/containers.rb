# == Schema Information
#
# Table name: containers
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :string
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_containers_on_deleted_at  (deleted_at)
#  index_containers_on_slug        (slug) UNIQUE
#
FactoryBot.define do
  factory :container do
    code { "MyString" }
    name { "MyString" }
    description { "MyString" }
  end
end
