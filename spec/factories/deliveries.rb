FactoryBot.define do
  factory :delivery do
    deliverable { nil }
    country { nil }
    city { "MyString" }
    zip_code { "MyString" }
    address { "MyString" }
    string { "MyString" }
    contact { "MyString" }
    email { "MyString" }
    phone_number { "MyString" }
  end
end
