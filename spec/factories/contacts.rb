# == Schema Information
#
# Table name: contacts
#
#  id               :bigint           not null, primary key
#  area             :string
#  birth_date       :datetime
#  contactable_type :string
#  deleted_at       :datetime
#  email            :string
#  first_name       :string
#  job              :string
#  last_name        :string
#  principal        :boolean          default(FALSE)
#  slug             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  contactable_id   :bigint
#  user_id          :bigint
#
# Indexes
#
#  index_contacts_on_contactable_type_and_contactable_id  (contactable_type,contactable_id)
#  index_contacts_on_deleted_at                           (deleted_at)
#  index_contacts_on_slug                                 (slug)
#  index_contacts_on_user_id                              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :contact do
    contactable { nil }
    deleted_at { "2020-07-13 01:46:08" }
  end
end
