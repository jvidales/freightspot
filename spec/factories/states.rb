# == Schema Information
#
# Table name: states
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  country_id :bigint
#
# Indexes
#
#  index_states_on_country_id  (country_id)
#  index_states_on_deleted_at  (deleted_at)
#
# Foreign Keys
#
#  fk_rails_...  (country_id => countries.id)
#
FactoryBot.define do
  factory :state do
    code { "MyString" }
    name { "MyString" }
    country { nil }
    deleted_at { "2020-07-13 01:35:28" }
  end
end
