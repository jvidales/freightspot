# == Schema Information
#
# Table name: origin_routes
#
#  id             :bigint           not null, primary key
#  deleted_at     :datetime
#  latitude       :decimal(, )
#  longitude      :decimal(, )
#  origin         :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  origin_port_id :bigint
#  quote_id       :bigint
#
# Indexes
#
#  index_origin_routes_on_deleted_at      (deleted_at)
#  index_origin_routes_on_origin_port_id  (origin_port_id)
#  index_origin_routes_on_quote_id        (quote_id)
#
# Foreign Keys
#
#  fk_rails_...  (origin_port_id => ports.id)
#  fk_rails_...  (quote_id => quotes.id)
#
FactoryBot.define do
  factory :origin_route do
    quote { nil }
    origin { "MyString" }
    deleted_at { "2021-05-04 18:11:19" }
  end
end
