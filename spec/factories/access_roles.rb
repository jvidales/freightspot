# == Schema Information
#
# Table name: access_roles
#
#  id               :bigint           not null, primary key
#  deleted_at       :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  access_id        :bigint
#  role_id          :bigint
#  system_module_id :bigint
#
# Indexes
#
#  index_access_roles_on_access_id         (access_id)
#  index_access_roles_on_deleted_at        (deleted_at)
#  index_access_roles_on_role_id           (role_id)
#  index_access_roles_on_system_module_id  (system_module_id)
#
# Foreign Keys
#
#  fk_rails_...  (access_id => accesses.id)
#  fk_rails_...  (role_id => roles.id)
#  fk_rails_...  (system_module_id => system_modules.id)
#
FactoryBot.define do
  factory :access_role do
    access { nil }
    role { nil }
    system_module { nil }
  end
end
