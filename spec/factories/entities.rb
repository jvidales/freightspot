# == Schema Information
#
# Table name: entities
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  locale     :string
#  name       :string
#  sender     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_entities_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :entity do
    name { "MyString" }
  end
end
