# == Schema Information
#
# Table name: units
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  name        :string
#  unit_system :bigint
#  unit_type   :bigint
#  value       :integer          default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_units_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :unit do
    name { "MyString" }
    type_unit { 1 }
  end
end
