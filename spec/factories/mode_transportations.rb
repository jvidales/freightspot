# == Schema Information
#
# Table name: mode_transportations
#
#  id              :bigint           not null, primary key
#  code            :string
#  deleted_at      :datetime
#  name            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint
#
# Indexes
#
#  index_mode_transportations_on_deleted_at       (deleted_at)
#  index_mode_transportations_on_service_mode_id  (service_mode_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#
FactoryBot.define do
  factory :mode_transportation do
    name { "MyString" }
    deleted_at { "2020-08-03 07:25:31" }
  end
end
