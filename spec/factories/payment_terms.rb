# == Schema Information
#
# Table name: payment_terms
#
#  id              :bigint           not null, primary key
#  amount_days_pay :integer
#  deleted_at      :datetime
#  description     :string
#  inactive        :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_payment_terms_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :payment_term do
    description { "MyString" }
    amount_days_pay { "MyString" }
    inactive { false }
    deleted_at { "2020-07-13 02:04:05" }
  end
end
