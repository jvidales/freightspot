# == Schema Information
#
# Table name: collection_routes
#
#  id             :bigint           not null, primary key
#  deleted_at     :datetime
#  latitude       :decimal(, )
#  longitude      :decimal(, )
#  origin         :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  origin_port_id :bigint
#  shipment_id    :bigint
#
# Indexes
#
#  index_collection_routes_on_deleted_at      (deleted_at)
#  index_collection_routes_on_origin_port_id  (origin_port_id)
#  index_collection_routes_on_shipment_id     (shipment_id)
#
# Foreign Keys
#
#  fk_rails_...  (origin_port_id => ports.id)
#  fk_rails_...  (shipment_id => shipments.id)
#
FactoryBot.define do
  factory :collection_route do
    shipment { nil }
    deleted_at { "2021-04-29 05:34:27" }
  end
end
