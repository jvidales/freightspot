# == Schema Information
#
# Table name: tasks
#
#  id          :bigint           not null, primary key
#  deleted_at  :datetime
#  description :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_tasks_on_deleted_at  (deleted_at)
#  index_tasks_on_slug        (slug)
#
FactoryBot.define do
  factory :task do
    description { "" }
  end
end
