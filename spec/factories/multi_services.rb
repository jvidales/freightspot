# == Schema Information
#
# Table name: multi_services
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint
#  supplier_id     :bigint
#
# Indexes
#
#  index_multi_services_on_deleted_at       (deleted_at)
#  index_multi_services_on_service_mode_id  (service_mode_id)
#  index_multi_services_on_supplier_id      (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#
FactoryBot.define do
  factory :multi_service do
    supplier { nil }
    service_mode { nil }
    deleted_at { "2020-11-14 11:35:10" }
  end
end
