# == Schema Information
#
# Table name: ports
#
#  id                    :bigint           not null, primary key
#  deleted_at            :datetime
#  iata_code             :string
#  inactive              :boolean          default(FALSE)
#  latitude              :decimal(, )
#  longitude             :decimal(, )
#  name                  :string
#  port_identifier       :string
#  slug                  :string
#  transportation_method :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  country_id            :bigint
#
# Indexes
#
#  index_ports_on_country_id  (country_id)
#  index_ports_on_deleted_at  (deleted_at)
#  index_ports_on_slug        (slug)
#
# Foreign Keys
#
#  fk_rails_...  (country_id => countries.id)
#
FactoryBot.define do
  factory :port do
    port_identifier { "MyString" }
    iata_code { "MyString" }
    transportation_method { "MyString" }
    name { "MyString" }
    inactive { false }
    deleted_at { "2020-07-18 14:26:53" }
  end
end
