# == Schema Information
#
# Table name: accesses
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_accesses_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :access do
    name { "MyString" }
    slug { "MyString" }
    deleted_at { "2020-10-30 22:39:46" }
  end
end
