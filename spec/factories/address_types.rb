# == Schema Information
#
# Table name: address_types
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  inactive   :boolean          default(FALSE)
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_address_types_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :address_type do
    name { "MyString" }
    inactive { false }
  end
end
