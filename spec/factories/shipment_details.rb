FactoryBot.define do
  factory :shipment_detail do
    description { "MyString" }
    product { "MyString" }
    shipment { nil }
    slug { "MyString" }
    deleted_at { "2020-07-30 01:54:02" }
  end
end
