# == Schema Information
#
# Table name: suppliers
#
#  id                :bigint           not null, primary key
#  authorized_credit :boolean          default(FALSE)
#  caat_code         :string
#  carrier           :boolean          default(FALSE)
#  credit_limit      :decimal(10, 4)   default(0.0)
#  deleted_at        :datetime
#  has_credit        :boolean          default(FALSE)
#  iata_code         :string
#  inactive          :boolean          default(FALSE)
#  name              :string
#  notes             :text
#  rfc               :string
#  scac_number       :string
#  slug              :string
#  website           :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  cfdi_id           :bigint
#  currency_id       :bigint
#  parent_entity_id  :bigint
#  payment_method_id :bigint
#  payment_term_id   :bigint
#  way_pay_id        :bigint
#
# Indexes
#
#  index_suppliers_on_cfdi_id            (cfdi_id)
#  index_suppliers_on_currency_id        (currency_id)
#  index_suppliers_on_deleted_at         (deleted_at)
#  index_suppliers_on_parent_entity_id   (parent_entity_id)
#  index_suppliers_on_payment_method_id  (payment_method_id)
#  index_suppliers_on_payment_term_id    (payment_term_id)
#  index_suppliers_on_slug               (slug)
#  index_suppliers_on_way_pay_id         (way_pay_id)
#
# Foreign Keys
#
#  fk_rails_...  (cfdi_id => cfdis.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (parent_entity_id => suppliers.id)
#  fk_rails_...  (payment_method_id => payment_methods.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (way_pay_id => way_pays.id)
#
FactoryBot.define do
  factory :supplier do
    
  end
end
