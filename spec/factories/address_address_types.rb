# == Schema Information
#
# Table name: address_address_types
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  address_id      :bigint
#  address_type_id :bigint
#
# Indexes
#
#  index_address_address_types_on_address_id       (address_id)
#  index_address_address_types_on_address_type_id  (address_type_id)
#  index_address_address_types_on_deleted_at       (deleted_at)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#  fk_rails_...  (address_type_id => address_types.id)
#
FactoryBot.define do
  factory :address_address_type do
    
  end
end
