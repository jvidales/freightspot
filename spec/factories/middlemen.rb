# == Schema Information
#
# Table name: middlemen
#
#  id                :bigint           not null, primary key
#  deleted_at        :datetime
#  inactive          :boolean          default(FALSE)
#  name              :string
#  rfc               :string
#  slug              :string
#  status            :bigint           default("pending")
#  website           :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  cfdi_id           :bigint
#  currency_id       :bigint
#  payment_method_id :bigint
#  payment_term_id   :bigint
#  way_pay_id        :bigint
#
# Indexes
#
#  index_middlemen_on_cfdi_id            (cfdi_id)
#  index_middlemen_on_currency_id        (currency_id)
#  index_middlemen_on_deleted_at         (deleted_at)
#  index_middlemen_on_payment_method_id  (payment_method_id)
#  index_middlemen_on_payment_term_id    (payment_term_id)
#  index_middlemen_on_way_pay_id         (way_pay_id)
#
# Foreign Keys
#
#  fk_rails_...  (cfdi_id => cfdis.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (payment_method_id => payment_methods.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (way_pay_id => way_pays.id)
#
FactoryBot.define do
  factory :middleman do
    rfc { "MyString" }
    name { "MyString" }
    website { "MyString" }
    slug { "MyString" }
    deleted_at { "2020-12-09 07:17:45" }
  end
end
