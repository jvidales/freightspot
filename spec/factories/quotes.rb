# == Schema Information
#
# Table name: quotes
#
#  id                  :bigint           not null, primary key
#  authorized          :boolean          default(FALSE)
#  danger_item         :boolean          default(FALSE)
#  declared_value      :decimal(10, 4)   default(0.0)
#  deleted_at          :datetime
#  description         :text
#  expired_at          :datetime
#  free_day_of_delay   :integer
#  frequency           :bigint
#  guarantee_letter    :boolean          default(FALSE)
#  is_active_shipment  :boolean          default(FALSE)
#  number              :string
#  operation_type      :integer
#  quoted_at           :datetime
#  service_mode        :bigint
#  service_type        :string
#  slug                :string
#  status              :bigint           default("open")
#  total_pieces        :decimal(10, 4)   default(0.0)
#  total_volume        :decimal(20, 4)   default(0.0)
#  total_weight        :decimal(10, 4)   default(0.0)
#  total_weight_volume :decimal(20, 4)   default(0.0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  client_id           :bigint
#  employee_id         :bigint
#  freight_type_id     :bigint
#  incoterm_id         :bigint
#  organization_id     :bigint
#  payment_term_id     :bigint
#  rate_id             :bigint
#  salesman_id         :bigint
#
# Indexes
#
#  index_quotes_on_client_id        (client_id)
#  index_quotes_on_deleted_at       (deleted_at)
#  index_quotes_on_employee_id      (employee_id)
#  index_quotes_on_freight_type_id  (freight_type_id)
#  index_quotes_on_incoterm_id      (incoterm_id)
#  index_quotes_on_organization_id  (organization_id)
#  index_quotes_on_payment_term_id  (payment_term_id)
#  index_quotes_on_rate_id          (rate_id)
#  index_quotes_on_salesman_id      (salesman_id)
#  index_quotes_on_slug             (slug)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (freight_type_id => mode_transportations.id)
#  fk_rails_...  (incoterm_id => incoterms.id)
#  fk_rails_...  (organization_id => organizations.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (rate_id => rates.id)
#  fk_rails_...  (salesman_id => employees.id)
#
FactoryBot.define do
  factory :quote do
    tariff { nil }
    deleted_at { "2021-04-08 03:30:58" }
  end
end
