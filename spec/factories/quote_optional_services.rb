# == Schema Information
#
# Table name: quote_optional_services
#
#  id                  :bigint           not null, primary key
#  applied_charge      :boolean          default(TRUE)
#  deleted_at          :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  optional_service_id :bigint
#  quote_id            :bigint
#
# Indexes
#
#  index_quote_optional_services_on_deleted_at           (deleted_at)
#  index_quote_optional_services_on_optional_service_id  (optional_service_id)
#  index_quote_optional_services_on_quote_id             (quote_id)
#
# Foreign Keys
#
#  fk_rails_...  (optional_service_id => optional_services.id)
#  fk_rails_...  (quote_id => quotes.id)
#
FactoryBot.define do
  factory :quote_optional_service do
    
  end
end
