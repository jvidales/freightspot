# == Schema Information
#
# Table name: currencies
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_currencies_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :currency do
    code { "MyString" }
    name { "MyString" }
    deleted_at { "2020-07-15 12:34:28" }
  end
end
