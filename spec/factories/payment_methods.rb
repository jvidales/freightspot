# == Schema Information
#
# Table name: payment_methods
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_payment_methods_on_deleted_at  (deleted_at)
#
FactoryBot.define do
  factory :payment_method do
    code { "MyString" }
    name { "MyString" }
    description { "MyText" }
    slug { "MyString" }
    deleted_at { "2020-11-19 18:28:34" }
  end
end
