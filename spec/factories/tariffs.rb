# == Schema Information
#
# Table name: tariffs
#
#  id              :bigint           not null, primary key
#  cargo_type      :bigint
#  deleted_at      :datetime
#  ended_at        :datetime
#  service_mode    :bigint
#  started_at      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  carrier_id      :bigint
#  freight_type_id :bigint
#
# Indexes
#
#  index_tariffs_on_carrier_id       (carrier_id)
#  index_tariffs_on_deleted_at       (deleted_at)
#  index_tariffs_on_freight_type_id  (freight_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (carrier_id => suppliers.id)
#  fk_rails_...  (freight_type_id => mode_transportations.id)
#
FactoryBot.define do
  factory :tariff do
    carrier { "" }
    service_mode { "" }
    freight_type { "" }
    started_at { "2021-03-27 17:30:49" }
    ended_at { "2021-03-27 17:30:49" }
    deleted_at { "2021-03-27 17:30:49" }
  end
end
