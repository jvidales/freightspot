# == Schema Information
#
# Table name: addresses
#
#  id               :bigint           not null, primary key
#  addressable_type :string
#  city             :string
#  colony           :string
#  country          :string
#  deleted_at       :datetime
#  interior_number  :string
#  name             :string
#  outdoor_number   :string
#  principal        :boolean          default(FALSE)
#  slug             :string
#  street           :string
#  zip_code         :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  addressable_id   :bigint
#  state_id         :bigint
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#  index_addresses_on_deleted_at                           (deleted_at)
#  index_addresses_on_slug                                 (slug)
#  index_addresses_on_state_id                             (state_id)
#
# Foreign Keys
#
#  fk_rails_...  (state_id => states.id)
#
FactoryBot.define do
  factory :address do
    addresable { nil }
    street { "MyString" }
    deleted_at { "2020-07-13 01:41:49" }
  end
end
