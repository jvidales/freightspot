require 'rails_helper'

RSpec.describe "MaritimeCarriers", type: :request do

  describe "GET /index" do
    it "returns http success" do
      get "/maritime_carriers/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/maritime_carriers/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/maritime_carriers/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/maritime_carriers/edit"
      expect(response).to have_http_status(:success)
    end
  end

end
