require 'rails_helper'

RSpec.describe "UserSteps", type: :request do
  describe "GET /company" do
    it "returns http success" do
      get "/user_steps/company"
      expect(response).to have_http_status(:success)
    end
  end

end
