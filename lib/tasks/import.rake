namespace :import do
  desc 'Import ports from csv'
  task ports: :environment do
    filename = "#{Rails.root}/db/csv/ports.csv"
    counter = 0
    CSV.foreach(filename) do |row|
      p row
      port_identifier, name, transportation_method, country, latitude, longitude = row
      country_data = Country.find_by_name(country)
      port = Port.create(port_identifier: port_identifier, name: name, country_id: country_data.id, latitude: latitude,
                         longitude: longitude, transportation_method: transportation_method)
      counter += 1 if port.persisted?
      puts port.errors.full_messages.join(',') if port.errors.any?
    end

    puts "Imported #{counter} ports"
  end
end
