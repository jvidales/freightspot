module Reports
  # Reports for systems
  module DashboardReport
    def dashboard
      @serie = [
        { name: 'Embarque FS-001', data: 20 }, { name: 'Embarque FS-002', data: 15 },
        { name: 'Embarque FS-003', data: 35 }, { name: 'Embarque FS-004', data: 10 },
        { name: 'Embarque FS-005', data: 20 }
      ]
      User.default_timezone = :utc
      @user_date = User.group_by_day(:created_at).count
      options
    end

    def options
      @options = {
        title: 'Properties Growth', subtitle: 'Grouped Per Day',
        xtitle: 'Day', ytitle: 'Properties', stacked: true, height: 200
      }
    end
  end
end
