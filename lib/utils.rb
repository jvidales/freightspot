# Create shared methods for controllers
module Utils
  # Methods for searching
  module Search
    def phone_list(address, contacts, senders)
      puts 'stuffs'
      address_phones = address.phones
      contact_phones = contact_phones(contacts)
      sender_phones = sender_phones(senders)
      phones = address_phones + contact_phones + sender_phones
    end

    # Method that gets phone numbers from contacts
    def contact_phones(contacts)
      phones = []
      contacts.each do |contact|
        phones << contact.phones.first
      end
      puts "SIZE 2: #{phones.size}"
      phones
    end

    # Method that gets phone numbers from senders
    def sender_phones(senders)
      phones = []
      unless senders.blank?
        senders.each do |sender|
          phones << sender.phones.first
        end
      end
      phones
    end
  end
end
