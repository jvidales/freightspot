module ApplicationHelper
  def action?(*action)
    action.include?(params[:action])
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def flash_class(type)
    case type
    when 'notice' then 'alert-outline-primary'
    when 'success' then 'alert-outline-success'
    when 'error' then 'alert-outline-danger'
    when 'alert' then 'alert-outline-warning'
    end
  end

  def icon_service_mode(code)
    case code
    when 'maritime'
      'fa-ship'
    when 'aerial'
       'fa-plane'
    when 'land'
      'fa-truck'
    end
  end

  def icon_transportation_method(type)
    case type
    when 'land'
      'fa-truck'
    when 'maritime'
      'fa-ship'
    when 'aerial'
      'fa-plane'
    end
  end

  def prepare_hash
    @permissions = Hash.new
    @access.each do |access|
      if @permissions[access.system_module_id].present?
        @permissions[access.system_module_id] << access.access_id
      else
        @permissions[access.system_module_id] = [access.access_id]
      end
    end
  end

  def type_service(type)
    case (type)
    when 'aerial'
      return 2
    when 'land'
      return 4
    when 'maritime'
      return 1
    end
  end

  def squish(string)
    string.delete(' ').underscore

  end

  def get_service_mode(type)
    service_mode = nil
    case (type)
    when 'land'
      service_mode = ServiceMode.find_by_code('FT')
    when 'maritime'
      service_mode = ServiceMode.find_by_code('FM')
    end
    service_mode
  end

  def phoneable_type(phone)
    name = ''
    case (phone.phoneable_type)
    when 'Agent'
      agent = Agent.find(phone.phoneable_id)
      name = agent.name
    when 'Client'
      client = Client.find(phone.phoneable_id)
      name = client.name
    when 'Contact'
      contact = Contact.find(phone.phoneable_id)
      name = contact.name
    when 'Middleman'
      middleman = Middleman.find(phone.phoneable_id)
      name = middleman.name
    when 'Sender'
      sender = Sender.find(phone.phoneable_id)
      name = sender.name
    when 'Supplier'
      supplier = Supplier.find(phone.phoneable_id)
      name = supplier.name
    else
      name = 'Not registration'
    end
    name
  end

  def phone_type(type)
    case (type)
    when 'Agent'
      t('activerecord.models.agent.one')
    when 'Client'
      t('activerecord.models.client.one')
    when 'Contact'
      t('activerecord.models.contact.one')
    when 'Middleman'
      t('activerecord.models.middleman.one')
    when 'Sender'
      t('activerecord.models.sender.one')
    when 'Supplier'
      t('activerecord.models.supplier.one')
    else
      'Not type found'
    end
  end

  def icon_phone(type)
    case (type)
    when 'mobile'
      'fa-mobile-alt'
    when 'permanent'
      'fa-phone'
    else
      'fa-phone'
    end
  end

  def label_status_shipment(status)
    case status
    when 'waiting_collection'
      'label-warning'
    when 'in_transit'
      'label-info'
    when 'delivery_route'
      'label-primary'
    when 'delivered'
      'label-success'
    else
      'label-dark'
    end
  end
end
