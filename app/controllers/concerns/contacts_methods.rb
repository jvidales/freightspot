# Methods for creating, updating, deleting and listing contacts.
module ContactsMethods
  extend ActiveSupport::Concern

  included do
    before_action :set_contact, only: %i[show_contact edit_contact update_contact destroy_contact]
  end

  def new_contact
    @contact = Contact.new
    @contact.addresses.build
    @contact.phones.build
  end

  def edit_contact; end

  def add_contact
    @contact = Contact.new(contact_params)
    assign_address(@contact)
    respond_to do |format|
      if @contact.save
        @response = t('controllers.contacts.create')
        @contacts = @agent.contacts.order('principal DESC').page params[:page]
        format.js { render layout: false, content_type: 'text/javascript' }
      else
        format.js { render :new_contact }
      end
    end
  end

  def update_contact
    change_address(@contact)
    respond_to do |format|
      if @contact.update(contact_params)
        @response = t('controllers.contacts.update')
        @contacts = @agent.contacts.order('principal DESC').page params[:page]
        format.js { render layout: false, content_type: 'text/javascript' }
      else
        format.js { render :edit_contact }
      end
    end
  end

  def destroy_contact
    @contact.destroy
    respond_to do |format|
      @contacts = @agent.contacts.order('principal DESC').page params[:page]
      @response = t('controllers.contacts.destroy')
      format.js { render layout: false, content_type: 'text/javascript' }
    end
  end

  private

  def set_contact
    @contact = Contact.find(params[:contact_id])
  end

  def contact_params
    params.require(:contact).permit(:id, :contactable_type, :contactable_id, :name, :email, :area, :job, :birth_date,
                                    :principal, :photo, phones_attributes: phones_attributes)
  end

  def phones_contact_attributes
    %i[id phoneable_type phoneable_id code number extension type_phone principal mobile_number]
  end

  def contacts(model)
    @contacts = model.contacts.order('principal DESC').page params[:page]
  end

  def model_with_contactable(model)
    model.find(params[:contact][:contactable_id])
  end

  def assign_address(contact)
    address = Address.find(params[:address][:id]) unless params[:address][:id].blank?
    contact.addresses << clone_data(address) unless address.blank?
  end

  def change_address(contact)
    address = Address.find(params[:address][:id]) unless params[:address][:id].blank?
    address_contact = contact.addresses.first
    contact.addresses << change_data(address, address_contact) unless address.blank?
  end

  def change_data(address, address_contact)
    address_contact.name = address.name
    address_contact.street = address.street
    address_contact.interior_number = address.interior_number
    address_contact.outdoor_number = address.outdoor_number
    address_contact.colony = address.colony
    address_contact.city = address.city
    address_contact.country = address.country
    address_contact.zip_code = address.zip_code
    address_contact.state_id = address.state_id
    address_contact.address_type_ids = address.address_type_ids
    address_contact.principal = address.principal
    address_contact
  end

  def clone_data(address)
    clone_address = Address.new
    clone_address.name = address.name
    clone_address.street = address.street
    clone_address.interior_number = address.interior_number
    clone_address.outdoor_number = address.outdoor_number
    clone_address.colony = address.colony
    clone_address.city = address.city
    clone_address.country = address.country
    clone_address.zip_code = address.zip_code
    clone_address.state_id = address.state_id
    clone_address.address_type_ids = address.address_type_ids
    clone_address.principal = address.principal
    clone_address
  end
end
