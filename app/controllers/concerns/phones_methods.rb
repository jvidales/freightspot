# Methods for creating, updating, deleting and listing phones.
module PhonesMethods
  extend ActiveSupport::Concern

  included do
    before_action :set_phone, only: %i[show_phone edit_phone update_phone destroy_phone]
  end

  def new_phone
    @phone = Phone.new
  end

  def edit_phone; end

  def add_phone
    @phone = Phone.new(phone_params)
    respond_to do |format|
      if @phone.save
        @contacts = @agent.contacts.order('principal DESC').page params[:page]
        @phones = phone_list(@agent, @contacts, [])
        @response = t('controllers.phones.create')
        format.js { render layout: false, content_type: 'text/javascript' }
      else
        format.js { render :new_phone }
      end
    end
  end

  def update_phone
    respond_to do |format|
      if @phone.update(phone_params)
        @contacts = @agent.contacts.order('principal DESC').page params[:page]
        @phones = phone_list(@agent, @contacts, [])
        @response = t('controllers.phones.update')
        format.js { render layout: false, content_type: 'text/javascript' }
      else
        format.js { render :edit_phone }
      end
    end
  end

  def destroy_phone
    @phone.destroy
    respond_to do |format|
      @contacts = @agent.contacts.order('principal DESC').page params[:page]
      @phones = phone_list(@agent, @contacts, [])
      @response = t('controllers.phones.destroy')
      format.js { render layout: false, content_type: 'text/javascript' }
    end
  end

  private

  def set_phone
    @phone = Phone.find(params[:phone_id])
  end

  def phone_params
    params.require(:phone).permit(:id, :phoneable_type, :phoneable_id, :code, :number, :extension, :type_phone,
                                  :principal, :mobile_number)
  end

  def model_with_phoneable(model)
    model.find(params[:phone][:phoneable_id])
  end
end
