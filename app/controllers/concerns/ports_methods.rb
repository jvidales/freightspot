# Methods for ports
module PortsMethods
  extend ActiveSupport::Concern

  # Method that returns the code of the port searched by the id of the port
  def port_code
    port = Port.find(params[:port_id])
    @port_code = port.port_identifier
    respond_to do |format|
      format.json { render json: @port_code }
    end
  end
end
