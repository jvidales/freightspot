# Shared methods between the different controllers
module ShipmentConverter
  extend ActiveSupport::Concern

  def convert_to_shipment(quote); end
end
