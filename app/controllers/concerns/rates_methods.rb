# Methods for creating, updating, deleting and listing rates.
module RatesMethods
  extend ActiveSupport::Concern

  private

  def rate_params
    params.require(:rate).permit(:tariff_id, :origin_port_id, :discharge_port_id, :freight_rate, :minimal_cost,
                                 :container_id, :measurement_unit, :transite_time, :frequency, :via_code, :via,
                                 :free_days, :guarantee_letter)
  end

  def rates_attributes
    %i[id _destroy tariff_id origin_port_id discharge_port_id freight_rate minimal_cost container_id measurement_unit
       transite_time frequency via_code via free_days guarantee_letter]
  end

  def cart
    session["cart_items#{current_user.id}"]
  end

  def cart_items
    items_hash = cart
    items = []
    items_hash.map { |item| items << (item.instance_of?(Hash) ? Rate.new(item) : item) }
    items
  end
end
