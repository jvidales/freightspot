# Methods for creating, updating, deleting and listing addresses.
module AddressesMethods
  extend ActiveSupport::Concern

  included do
    before_action :set_agent_for_address, only: %i[add_address update_address]
    before_action :set_address, only: %i[show_address edit_address update_address destroy_address]
  end

  def show_address; end

  def new_address
    @is_contact = params[:is_contact]
    @address = Address.new
  end

  def edit_address; end

  def add_address
    @is_contact = false
    @address = Address.new(address_params)
    respond_to do |format|
      if @address.save
        @is_contact = params[:is_contact]
        @addresses = @agent.addresses.order('name ASC').page params[:page]
        @response = t('controllers.addresses.create')
        format.js { render layout: false, content_type: 'text/javascript' }
      else
        format.js { render :new_address }
      end
    end
  end

  def update_address
    respond_to do |format|
      if @address.update(address_params)
        @addresses = @agent.addresses.order('name ASC').page params[:page]
        @response = t('controllers.addresses.update')
        format.js { render layout: false, content_type: 'text/javascript' }
      else
        format.js { render :edit_address }
      end
    end
  end

  def destroy_address
    @address.destroy
    respond_to do |format|
      @addresses = @agent.addresses.order('name ASC').page params[:page]
      @response  = t('controllers.addresses.destroy')
      format.js { render layout: false, content_type: 'text/javascript' }
    end
  end

  private

  def set_address
    @address = Address.find(params[:address_id])
  end

  def address_params
    params.require(:address).permit(:id, :addressable_type, :addressable_id, :name, :street, :interior_number,
                                    :outdoor_number, :colony, :city, :country, :state_id, :zip_code,
                                    address_type_ids: [])
  end

  def set_addresses
    @addresses = @agent.addresses.order('name ASC').page params[:page]
  end

  def set_agent_for_address
    @agent = Agent.find(params[:address][:addressable_id])
  end
end
