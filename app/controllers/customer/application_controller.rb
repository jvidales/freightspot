module Customer
  # Customer Application Controller
  class ApplicationController < Administrate::ApplicationController
    protect_from_forgery with: :exception
    
    before_action :authenticate_user!
    before_action :set_locale
    before_action :set_last_seen_at, if: :user_signed_in?
    before_action :set_breadcrumb_metronic

    protected

    def authenticate_user!
      redirect_to "/", alert: "Not unauthorized." unless user_signed_in? && !current_user.admin?
    end

    def set_locale
      locale = params[:locale].to_s.strip.to_sym
      I18n.locale = I18n.available_locales.include?(locale) ? locale : I18n.default_locale
    end

    def default_url_options
      { locale: I18n.locale }
    end

    def set_last_seen_at
      current_user.touch(:last_seen_at)
    end

    def add_breadcrumbs_metronic(label, path = nil)
      @breadcrumbs << {
        label: label,
        path: path
      }
    end

    def set_breadcrumb_metronic
      @breadcrumbs = []
    end
  end
end
