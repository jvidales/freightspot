module Customer
  # Quotes Controller for Customer
  class QuotesController < Customer::ApplicationController
    include Wicked::Wizard
    steps :quote_units, :optional_services, :quote_detail

    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]
    before_action :initialize_quote, only: %i[new]
    before_action :set_quote, only: %i[show update tariffs]
    before_action :set_client, only: %i[index new create]

    def index
      @quotes = Quote.filter(params.slice(:service_mode)).where(client_id: @client.id).page(params[:page])
      add_breadcrumbs_metronic I18n.translate('activerecord.models.quote.other')
    end

    def new
      @client = current_user.contact.contactable
      @quote.build_origin_route
      @quote.build_destiny_route
      @freight_types = []
      add_breadcrumbs_metronic I18n.translate('views.admin.quotes.new.subtitle').html_safe
    end

    def create
      @quote = Quote.new(quote_params)
      tariffs = Tariff.joins(:rates).where(rates: { origin_port_id: @quote.origin_route.origin_port_id,
                                                    discharge_port_id: @quote.destiny_route.destiny_port_id },
                                           tariffs: { service_mode: @quote.service_mode }).exists?
      unless tariffs.blank?
        if @quote.save
          session[:quote_id] = @quote.id
          redirect_to tariffs_customer_quotes_path
        else
          render :new
        end
      else
        flash[:error] = ' No existen tarifas'
        render :new
      end
    end

    def show
      add_breadcrumbs_metronic I18n.translate('views.admin.quotes.new.subtitle').html_safe
      case step
      when :quote_units
        puts "in quotes unit step  " + params.to_s
        @quote = Quote.find(params[:quote_id])
        @rate = Rate.find(params[:rate_id])
        @quote.rate = @rate
        puts @rate.frequency.to_s
        @quote.frequency = @rate.frequency
        @quote.save
        @quote.quote_units.build
        wizard_path(:quote_units)
      when :optional_services
        puts 'in optional services step' + params.to_s
        @optional_services = OptionalService.all.group_by(&:category)

        @total_cost = @quote.rate.minimal_cost
        wizard_path(:optional_services)
      when :quote_details
        puts "in quotes detail step  " + params.to_s
      end
      if params[:id] == "wicked_finish"
        redirect_to customer_quotes_path
      else
        render_wizard
      end

    end

    def update
      if @quote.update_attributes(quote_params)
        if step == :quote_units
          @quote.description = @quote.description ? @quote.description : ""
          @quote.quote_units.each do |quote_unit|
            @quote.description += quote_unit.product.name + "\n"
          end
        end
        render_wizard @quote
      else
        # print the errors to the development log
        Rails.logger.info(@quote.errors.messages.inspect)
      end
    end

    def tariffs
      puts "in tariffs step"
      puts @quote.service_mode.to_s
      tariffs = Tariff.where(service_mode: @quote.service_mode)
      rates ||= []
      tariffs.each do |tariff|
        rates << tariff.rates.where(origin_port_id: @quote.origin_route.origin_port_id, discharge_port_id: @quote.destiny_route.destiny_port_id) unless tariff.rates.nil?
      end
      @rates = rates.flatten.sort_by { |rate| [  rate.minimal_cost, rate.transite_time]}
      @minimal_cost = @rates.first
      @cost_benefit = @rates.second
    end

    def search_freight_types
      service_mode_variable = params[:service_mode]
      service_mode = ServiceMode.find_by_variable(service_mode_variable)
      @freight_types = ModeTransportation.where('service_mode_id = ?', service_mode.id)
      respond_to do |format|
        format.json { render json: @freight_types }
      end
    end

    def search_service_type_by_incoterm
      if params[:operation_type] != 'national'
        service_type = {}
        @service_type = ServiceType.where(incoterm_id: params[:incoterm_id], operation_type: params[:operation_type]).first unless params[:incoterm_id].nil?
        service_type[:name] = t("enums.quote.type_service.#{@service_type.name}")
      end
      respond_to do |format|
        format.json { render json: service_type }
      end
    end

    def search_service_type_by_operation_type
      if params[:operation_type] != 'national'
        service_type = {}
        @service_type = ServiceType.where(incoterm_id: params[:incoterm_id], operation_type: params[:operation_type]).first unless params[:incoterm_id].nil?
        service_type[:name] = t("enums.quote.type_service.#{@service_type.name}")
      end
      respond_to do |format|
        format.json { render json: service_type }
      end
    end

    def get_port_coordinates
      port_coordinates = {}
      port = Port.find(params[:port_id])
      port_coordinates[:latitude] = port.latitude
      port_coordinates[:longitude] = port.longitude
      respond_to do |format|
        format.json { render json: port_coordinates }
      end
    end

    def get_optional_service
      optional_service = Hash.new
      service = OptionalService.find(params[:optional_service_id])
      optional_service[:id] =  service.id
      optional_service[:name] =  service.name
      optional_service[:name_label] =  service.name_label
      optional_service[:cost] =  service.cost
      respond_to do |format|
        format.json { render :json => optional_service }
      end
    end

    def set_freight_type
      session['freight_type'] = params[:transportation_modality]
    end

    private

    def finish_wizard_path
      redirect_to customer_quotes_path
    end

    def set_client
      @client = current_user.contact.contactable
    end

    def set_quote
      @quote = Quote.find_by_id(session[:quote_id])
    end

    def quote_params
      params.require(:quote).permit( :id, :operation_type, :service_mode,:organization_id,
                                     :quoted_at, :transportation_method, :frequency , :incoterm_id,
                                     :client_id, :service_type, :transportation_modality,
                                     :operation_type, :service_mode_id, :danger_item, :guarantee_letter,
                                     :freight_type_id, :description, :declared_value,
                                     origin_route_attributes: origin_route_attributes,
                                     destiny_route_attributes: destiny_route_attributes,
                                     quote_units_attributes: quote_units_attributes,
                                     optional_service_ids: [])
    end
    def quote_units_attributes
      %i[id length height width weight volume number_units packaging_type_id product_id]
    end
    def quote_route_attributes
      %i[id discharge_port_id origin_port_id]
    end

    def origin_route_attributes
      %i[id quote_id origin_port_id origin latitude longitude]
    end

    def destiny_route_attributes
      %i[id quote_id destiny_port_id destiny latitude longitude]
    end

    def quote_optional_services
      %i[id optional_service_id applied_charge]
    end

    def initialize_quote
      @quote = Quote.new
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.quote.other').html_safe, customer_quotes_path
    end

  end
end
