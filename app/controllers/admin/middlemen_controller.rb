module Admin
  # Middlemen Controller
  class MiddlemenController < Admin::ApplicationController
    include Utils::Search
    skip_before_action :verify_authenticity_token
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]
    before_action :set_middleman, only: %i[edit show destroy update new_address show_address
                                           edit_address update_address destroy_address
                                           new_phone show_phone destroy_phone edit_phone
                                           update_phone update_readonly]
    before_action :set_readonly, only: %i[show]
    before_action :set_middleman_for_contact, only: %i[new_contact show_contact edit_contact destroy_contact]
    before_action :set_address, only: %i[show_address edit_address update_address destroy_address]
    before_action :set_phone, only: %i[show_phone edit_phone update_phone destroy_phone]
    before_action :set_contact, only: %i[show_contact edit_contact update_contact destroy_contact]

    def index
      @middlemen = Middleman.all
      add_breadcrumbs_metronic I18n.translate('activerecord.models.middleman.other').html_safe
    end

    def new
      @middleman = Middleman.new
      @middleman.addresses.build
      @middleman.phones.build
      @middleman.contacts.build.phones.build
      add_breadcrumbs_metronic I18n.translate('views.admin.middlemen.new.subtitle').html_safe
    end

    def show
      @addresses = @middleman.addresses.page params[:page]
      @contacts = @middleman.contacts.order('principal DESC').page params[:page]
      @phones = phone_list(@middleman, @contacts, [])
      dashboard
      add_breadcrumbs_metronic @middleman.name.to_s.html_safe
    end

    def edit; end

    def create
      @middleman = Middleman.new(middleman_params)
      @middleman.contacts.first.addresses << clone_address(@middleman.addresses.first)
      if @middleman.save
        flash[:success] = I18n.translate('controllers.middlemen.create')
        redirect_to admin_middlemen_path
      else
        render :new
      end
    end

    def update
      if @middleman.update(middleman_params)
        @response = t('controllers.middlemen.update')
        respond_to do |format|
          format.js
        end
      else
        render :show
      end
    end

    def destroy
      @middleman.destroy
      @response = t('controllers.middlemen.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def new_address
      @is_contact = params[:is_contact]
      @address = Address.new
    end

    def show_address; end

    def add_address
      @is_contact = false
      @create = false
      @address = Address.new(address_params)
      @middleman = Middleman.find(@address.addressable_id)
      respond_to do |format|
        if @address.save
          @create = true
          @is_contact = params[:is_contact]
          @addresses = @middleman.addresses.page params[:page]
          @response = t('controllers.addresses.create')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        else
          puts @address.errors.messages
        end
      end
    end

    def edit_address; end

    def update_address
      @update = false
      respond_to do |format|
        if @address.update(address_params)
          @addresses = @middleman.addresses.page params[:page]
          @update = true
          @response = t('controllers.addresses.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_address
      @address.destroy
      @addresses = @middleman.addresses.page params[:page]
      @response  = t('controllers.addresses.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def new_phone
      @phone = Phone.new
    end

    def add_phone
      @phone = Phone.new(phone_params)
      @middleman = Middleman.find(@phone.phoneable_id)
      respond_to do |format|
        if @phone.save
          @contacts = @middleman.contacts
          @phones = phone_list(@middleman, @contacts, [])
          @response = t('controllers.phones.create')
          format.js { render layout: false, content_type: 'text/javascript' }
        else
          format.js { render :new_phone, content_type: 'text/javascript' }
        end
      end
    end

    def show_phone; end

    def edit_phone; end

    def update_phone
      @middleman = Middleman.find(@phone.phoneable_id)
      respond_to do |format|
        if @phone.update(phone_params)
          @contacts = @middleman.contacts.order('principal DESC').page params[:page]
          @phones = phone_list(@middleman, @contacts, [])
          @response = t('controllers.phones.update')
          format.js { render layout: false, content_type: 'text/javascript' }
        else
          format.js { render :edit_phone, content_type: 'text/javascript' }
        end
      end
    end

    def destroy_phone
      @middleman = Middleman.find(@phone.phoneable_id)
      @phone.destroy
      @contacts = @middleman.contacts.order('principal DESC').page params[:page]
      @phones = phone_list(@middleman, @contacts, [])
      @response  = t('controllers.phones.destroy')
      respond_to do |format|
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def new_contact
      @contact = Contact.new
      @contact.addresses.build
      @contact.phones.build
      @contacts = @middleman.contacts.order('principal DESC').page params[:page]
      @button = t('views.shared.buttons.save')
    end

    def add_contact
      @create = false
      @contact = Contact.new(contact_params)
      @middleman = Middleman.find(@contact.contactable_id)
      if !params[:address][:id].blank?
        address = Address.find(params[:address][:id])
        @contact.addresses << clone_address(address)
      end
      respond_to do |format|
        if @contact.save
          @create = true
          @response = t('controllers.contacts.create')
          @contacts = @middleman.contacts.order('principal DESC').page params[:page]
          format.js { render layout: false, content_type: 'text/javascript' }
        end
      end
    end

    def show_contact; end

    def edit_contact
      @contact.phones.build if @contact.phones.blank?
      @contacts = @middleman.contacts.order('principal DESC').page params[:page]
      @button = t('views.shared.buttons.update')
    end

    def update_contact
      @update = false
      @middleman = Middleman.find(@contact.contactable_id)
      respond_to do |format|
        if @contact.update(contact_params)
          @contacts = @middleman.contacts.order('principal DESC').page params[:page]
          @update = true
          @response = t('controllers.contacts.update')
          format.js { render layout: false, content_type: 'text/javascript' }
        end
      end
    end

    def destroy_contact
      @middleman = Middleman.find(@contact.contactable_id)
      @contact.destroy
      @contacts = @middleman.contacts.order('principal DESC').page params[:page]
      @response = t('controllers.contacts.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def filter_states
      country_name = params[:country_id]
      country = Country.find_by_name(country_name)
      @states = State.where('country_id = ?', country.id)
      respond_to do |format|
        format.json { render json: @states}
      end
    end

    def clone_address(address)
      cloneAddress = Address.new
      cloneAddress.name = address.name
      cloneAddress.street = address.street
      cloneAddress.interior_number = address.interior_number
      cloneAddress.outdoor_number = address.outdoor_number
      cloneAddress.colony = address.colony
      cloneAddress.city = address.city
      cloneAddress.country = address.country
      cloneAddress.zip_code = address.zip_code
      cloneAddress.state_id = address.state_id
      cloneAddress.address_type_ids = address.address_type_ids
      cloneAddress.principal = address.principal
      cloneAddress
    end


    def dashboard
      @serie = [
        { name: 'Embarque FS-001', data: 20 },
        { name: 'Embarque FS-002', data: 15 },
        { name: 'Embarque FS-003', data: 35 },
        { name: 'Embarque FS-004', data: 10 },
        { name: 'Embarque FS-005', data: 20 },
      ]
      User.default_timezone = :utc
      @user_date = User.group_by_day(:created_at).count
      @options = {
        title: 'Properties Growth',
        subtitle: 'Grouped Per Day',
        xtitle: 'Day',
        ytitle: 'Properties',
        stacked: true,
        height: 200
      }
    end

    def update_readonly
      if params[:readonly] == 'true'
        @readonly = true
      else
        @readonly = false
      end
      respond_to do |format|
        format.js
      end
    end
        ##

    private

    def middleman_params
      params.require(:middleman).permit(:name, :rfc, :website, :currency_id, :payment_term_id,
                                        :pay_like, :cfdi_id, :way_pay_id,
                                        :payment_method_id, :status,
                                        addresses_attributes: address_attributes,
                                        phones_attributes: phones_attributes,
                                        contacts_attributes: contacts_attributes)
    end

    def address_params
      params.require(:address).permit(:id, :addressable_type, :name, :addressable_id, :street, :interior_number,
                                      :outdoor_number, :city, :zip_code, :country, :state_id, :colony, :principal,
                                      address_type_ids: [])
    end

    def phone_params
      params.require(:phone).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code, :type_phone,
                                    :mobile_number)
    end

    def phones_params
      params.require(:phones).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code,
                                     :type_phone, :principal, :mobile_number)
    end

    def addresses_params
      params.require(:addresses).permit(:id, :addressable_type, :addressable_id, :street, :interior_number,
                                        :outdoor_number, :city, :zip_code, :country, address_type_ids: [])
    end

    def contact_params
      params.require(:contact).permit(:id, :contactable_id, :contactable_type, :name, :email, :area,
                                      :job, :birth_date, :principal, :photo,
                                      phones_attributes: phones_attributes)
    end

    def address_attributes
      %i[id _destroy addressable_type addressable_id name street interior_number outdoor_number country city zip_code
         principal address_type_ids state_id colony]
    end

    def phones_attributes
      %i[id _destroy phoneable_type phoneable_id code number extension principal type_phone mobile_number]
    end

    def contacts_attributes
      [:id, :_destroy, :contactable_type, :contactable_id, :name, :email, :birth_date, :principal,
       :area, :job, phones_attributes: phones_attributes]
    end

    def set_middleman
      @middleman = Middleman.friendly.find(params[:id])
    end

    def set_middleman_for_contact
      @middleman = Middleman.friendly.find(params[:id])
    end

    def set_address
      @address = Address.find(params[:address_id])
    end

    def set_phone
      @phone = Phone.find(params[:phone_id])
    end

    def set_contact
      @contact = Contact.find(params[:contact_id])
    end

    def set_readonly
      if params[:readonly] == 'true'
        @readonly = true
      else
        @readonly = false
      end
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.middleman.other').html_safe, admin_middlemen_path
    end
  end
end
