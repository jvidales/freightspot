module Admin
  # Class controller Services
  class SettingsController < Admin::ApplicationController
    before_action :set_setting, only: %i[set_language]

    def set_language
      @language = Language.find_by_code(params[:language])
      if @setting.update(language_id: @language.id)
        redirect_to admin_root_path(locale: @language.code.to_s.strip.to_sym)
        flash[:success] = t('controllers.setting.update')
      else
        redirect_to admin_root_path(locale: I18n.locale)
      end
    end

    private

    def set_setting
      @setting = Setting.find(params[:id])
    end
  end
end
