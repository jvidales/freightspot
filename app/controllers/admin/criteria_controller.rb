module Admin
  # Creates the CRUD for the criteria.
  class CriteriaController < Admin::ApplicationController
    before_action :set_criterium, only: %i[edit show update destroy]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    # load_and_authorize_resource

    def index
      add_breadcrumbs_metronic I18n.translate('activerecord.models.criterium.other')
      @criteria = Criterium.all
    end

    def show
      add_breadcrumbs_metronic @criterium.name.to_s.html_safe
    end

    def new
      @criterium = Criterium.new
      add_breadcrumbs_metronic I18n.translate('views.admin.criteria.new.subtitle').html_safe
    end

    def edit
      add_breadcrumbs_metronic "#{t('views.admin.criteria.edit.subtitle')} #{@criterium.name}".html_safe
    end

    def create
      @criterium = Criterium.new(criterium_params)
      if @criterium.save
        redirect_to admin_criteria_path
        flash[:success] = t('controllers.criteria.create')
      else
        render :new
      end
    end

    def update
      if @criterium.update(criterium_params)
        redirect_to admin_criteria_path
        flash[:success] = t('controllers.criteria.update')
      else
        render :edit
      end
    end

    def destroy
      @criterium.destroy
      flash[:success] = t('controllers.criteria.destroy')
      redirect_to admin_criteria_path
    end

    private

    def criterium_params
      params.require(:criterium).permit(:id, :name, :amount, :currency_id, :validity_start, :validity_end, :remarks,
                                        :advanced_parameters, :origin_id, :destiny_id, :container_id, :status)
    end

    def set_criterium
      @criterium = Criterium.find(params[:id])
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.criterium.other').html_safe, admin_tariffs_path
    end
  end
end
