module Admin
  # Creates the CRUD for the clients.
  class ClientsController < Admin::ApplicationController
    include Utils::Search
    skip_before_action :verify_authenticity_token
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]
    before_action :set_client, only: %i[edit show destroy update new_address show_address
                                        edit_address update_address destroy_address
                                        new_phone show_phone destroy_phone edit_phone
                                        update_phone]
    before_action :set_readonly, only: %i[show]
    before_action :set_client_for_product, only: %i[new_client_product edit_client_product update_client_product]
    before_action :set_client_for_document, only: %i[new_client_document edit_client_document update_client_document]
    before_action :set_client_for_contact, only: %i[new_contact show_contact edit_contact destroy_contact]
    before_action :set_address, only: %i[show_address edit_address update_address destroy_address]
    before_action :set_phone, only: %i[show_phone edit_phone update_phone destroy_phone]
    before_action :set_contact, only: %i[show_contact edit_contact update_contact destroy_contact]
    before_action :set_sender, only: %i[show_sender edit_sender update_sender destroy_sender]
    before_action :set_client_product, only: %i[edit_client_product update_client_product destroy_client_product]
    before_action :set_client_document, only: %i[edit_client_document update_client_document destroy_client_document download_client_document]

    load_and_authorize_resource

    def index
      add_breadcrumbs_metronic I18n.translate('activerecord.models.client.other').html_safe
      set_default_columns
      @column_names = get_columns
      @all_columns = @clients.get_all_columns_names
      respond_to do |format|
        format.html
        # format.json { render json: ClientsDatatable.new(view_context)}
      end
    end

    def show
      @addresses = @client.addresses.page params[:page]
      @contacts = @client.contacts.order('principal DESC').page params[:page]
      @senders = @client.senders.order('name DESC').page params[:page]
      @phones = phone_list(@client, @contacts, @senders)
      @products = @client.client_products.page params[:page]
      @documents = @client.client_documents.page params[:page]
      dashboard
      add_breadcrumbs_metronic @client.name.to_s.html_safe
      respond_to do |format|
        format.js { render 'show.js.erb' }
        format.html
        format.pdf {
          render pdf: "Client No. #{@client.id}",
                 disposition: 'inline',
                 page_size: 'A4',
                 template: 'admin/clients/report.html.slim',
                 layout: 'template',
                 orientation: 'Landscape',
                 lowquality: true,
                 zoom: 1,
                 dpi: 75
        }
      end
    end

    def dashboard
      @serie = [
        { name: 'Embarque FS-001', data: 20 },
        { name: 'Embarque FS-002', data: 15 },
        { name: 'Embarque FS-003', data: 35 },
        { name: 'Embarque FS-004', data: 10 },
        { name: 'Embarque FS-005', data: 20 },
      ]
      User.default_timezone = :utc
      @user_date = User.group_by_day(:created_at).count
      @options = {
        title: 'Properties Growth',
        subtitle: 'Grouped Per Day',
        xtitle: 'Day',
        ytitle: 'Properties',
        stacked: true,
        height: 200
      }
    end

    def new
      @client = Client.new
      @client.addresses.build
      2.times do
        @client.phones.build
      end
      @client.contacts.build.phones.build
      add_breadcrumbs_metronic I18n.translate('views.admin.clients.new.subtitle').html_safe
    end

    def edit; end

    def create
      @client = Client.new(client_params)
      if @client.save
        flash[:success] = I18n.translate('controllers.clients.create')
        redirect_to admin_clients_path
      else
        render :new
      end
    end

    def update
      if @client.update(client_params)
        @response = t('controllers.clients.update')
        respond_to do |format|
          format.js
        end
      else
        render :show
      end
    end

    def destroy
      @client.destroy
      flash[:error] = I18n.translate('controllers.clients.destroy')
      redirect_to admin_clients_path
    end

    def new_address
      @is_contact = params[:is_contact]
      @address = Address.new
    end

    def show_address; end

    def add_address
      @is_contact = false
      @create = false
      @address = Address.new(address_params)
      @client = Client.find(@address.addressable_id)
      respond_to do |format|
        if @address.save
          @create = true
          @is_contact = params[:is_contact]
          @addresses = @client.addresses.page params[:page]
          @response = t('controllers.addresses.create')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        else
          puts @address.errors.messages
        end
      end
    end

    def edit_address; end

    def update_address
      @update = false
      respond_to do |format|
        if @address.update(address_params)
          @addresses = @client.addresses.page params[:page]
          @update = true
          @response = t('controllers.addresses.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_address
      @address.destroy
      @addresses = @client.addresses.page params[:page]
      @response  = t('controllers.addresses.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def clone_address(address)
      cloneAddress = Address.new
      cloneAddress.name = address.name
      cloneAddress.street = address.street
      cloneAddress.interior_number = address.interior_number
      cloneAddress.outdoor_number = address.outdoor_number
      cloneAddress.colony = address.colony
      cloneAddress.city = address.city
      cloneAddress.country = address.country
      cloneAddress.zip_code = address.zip_code
      cloneAddress.state_id = address.state_id
      cloneAddress.address_type_ids = address.address_type_ids
      cloneAddress.principal = address.principal
      cloneAddress
    end

    def new_phone
      @phone = Phone.new
    end

    def add_phone
      @phone = Phone.new(phone_params)
      @client = Client.find(@phone.phoneable_id)
      respond_to do |format|
        if @phone.save
          @contacts = @client.contacts.order('principal DESC').page params[:page]
          @senders = @client.senders
          @phones = phone_list(@client, @contacts, @senders)
          @response = t('controllers.phones.create')
          format.js { render layout: false, content_type: 'text/javascript' }
        else
          format.js { render :new_phone, content_type: 'text/javascript' }
        end
      end
    end

    def show_phone; end

    def edit_phone; end

    def update_phone
      @client = Client.find(@phone.phoneable_id)
      respond_to do |format|
        if @phone.update(phone_params)
          @contacts = @client.contacts.order('principal DESC').page params[:page]
          @senders = @client.senders
          @phones = phone_list(@client, @contacts, @senders)
          @response = t('controllers.phones.update')
          format.js { render layout: false, content_type: 'text/javascript' }
        else
          format.js { render :edit_phone, content_type: 'text/javascript' }
        end
      end
    end

    def destroy_phone
      @client = Client.find(@phone.phoneable_id)
      @contacts = @client.contacts.order('principal DESC').page params[:page]
      @phone.destroy
      @phones = phone_list(@client, @contacts, @senders)
      @response  = t('controllers.phones.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def new_contact
      @contact = Contact.new
      @contact.addresses.build
      @contact.phones.build
      @contacts = @client.contacts.order('principal DESC').page params[:page]
      @button = t('views.shared.buttons.save')
    end

    def add_contact
      @create = false
      @contact = Contact.new(contact_params)
      @client = Client.find(@contact.contactable_id)
      if !params[:address][:id].blank?
        address = Address.find(params[:address][:id])
        @contact.addresses << clone_address(address)
      end
      respond_to do |format|
        if @contact.save
          @create = true
          rol = Role.find_by_name('Cliente')
          password = Devise.friendly_token.first(8)
          user = User.create(username: @contact.name.mentionable, password: password, email: @contact.email, role_id: rol.id)
          puts "\n\n\nPassword:"
          puts "Password: #{password}"
          RegistrationMailer.welcome_email(user, password).deliver
          @response = t('controllers.contacts.create')
          @contacts = @client.contacts.order('principal DESC').page params[:page]
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def show_contact; end

    def edit_contact
      @contact.phones.build if @contact.phones.blank?
      @contacts = @client.contacts.order('principal DESC').page params[:page]
      @button = t('views.shared.buttons.update')
    end

    def update_contact
      @update = false
      @client = Client.find(@contact.contactable_id)
      respond_to do |format|
        if @contact.update(contact_params)
          @contacts = @client.contacts.order('principal DESC').page params[:page]
          @update = true
          @response = t('controllers.contacts.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_contact
      @client = Client.find(@contact.contactable_id)
      @contact.destroy
      @contacts = @client.contacts.order('principal DESC').page params[:page]
      @response = t('controllers.contacts.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    # add senders methods(AnaCeci 11/24/20)

    def new_sender
      @sender = Sender.new
      @sender.phones.build
      @button = t('views.shared.buttons.save')
    end

    def add_sender
      @sender = Sender.new(sender_params)
      @client = Client.find(@sender.senderable_id)
      respond_to do |format|
        if @sender.save
          @senders = @client.senders
          @response = t('controllers.senders.create')
          @senders = @client.senders.order('name DESC').page params[:page]
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        else
          format.js { render :new, content_type: 'text/javascript' }
        end
      end
    end

    def show_sender; end

    def edit_sender
      @sender.phones.build if @sender.phones.blank?
      @button = t('views.shared.buttons.update')
      @senders = @client.senders.order('name DESC').page params[:page]
    end

    def update_sender
      @update = false
      @client = Client.find(@sender.senderable_id)
      respond_to do |format|
        if @sender.update(sender_params)
          @senders = @client.senders.order('name desc').page params[:page]
          @update = true
          @response = t('controllers.senders.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_sender
      @client = Client.find(@sender.senderable_id)
      @sender.destroy
      respond_to do |format|
        @senders = @client.senders.order('name DESC').page params[:page]
        @response = t('controllers.senders.destroy')
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def new_client_product
      @button = t('views.shared.buttons.save')
      @product = ClientProduct.new
    end

    def add_client_product
      @product = ClientProduct.new(client_product_params)
      respond_to do |format|
        if @product.save
          @products = @client.client_products.page params[:page]
          @response = t('controllers.client_products.create')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def edit_client_product; end

    def update_client_product
      @update = false
      respond_to do |format|
        if @product.update(client_product_params)
          @products = @client.client_products.page params[:page]
          @update = true
          @response = t('controllers.client_products.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_client_product
      @product.destroy
      client = Client.find(@product.client_id)
      @products = client.client_products.page params[:page]
      @response  = t('controllers.client_products.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    #--------- client document     #add methods(AnaCeci 12/10/20)

    def new_client_document
      @button = t('views.shared.buttons.save')
      @documents = @client.client_documents.page params[:page]
      @client_document = ClientDocument.new
    end

    def add_client_document
      @client_document = ClientDocument.create(client_document_params)
      respond_to do |format|
        if @client_document.save
          @create = true
          @documents = @client.client_documents.page params[:page]
          @response = t('controllers.client_documents.create')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_client_document
      @client_document.destroy
      @response  = t('controllers.client_documents.destroy')
      @documents = @client.client_documents.page params[:page]
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def download_client_document
      data = open(@client_document.file.service_url)
      send_data data.read, type: data.content_type, x_sendfile: true
    end

    def edit_client_document
      @documents = @client.client_documents.page params[:page]
      @button = t('views.shared.buttons.update')
    end

    def update_client_document
      @update = false
      respond_to do |format|
        if @client_document.update(client_document_params)
          @documents = @client.client_documents.page params[:page]
          @update = true
          @response = t('controllers.client_documents.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    #----------------------------------------
    def filter_states
      country_name = params[:country_id]
      country = Country.find_by_name(country_name)
      @states = State.where('country_id = ?', country.id)
      respond_to do |format|
        format.json { render json: @states }
      end
    end

    def update_readonly
      if params[:readonly] == 'true'
        @readonly = true
      else
        @readonly = false
      end
      respond_to do |format|
        format.js
      end
    end

    def set_column
      # este metodo es para seteaqr a las configuraciones del cliente las columnas
      @column_names = params[:column]
      # javascript que se utilizara en la vista
      #             var selected = [...select.selectedOptions]
      #                 .map(option => option.value
      #                 );
      #             console.log(selected);
    end

    def filter
      @clients = Client.filters(params)
      set_default_columns
      @column_names = get_columns
      @all_columns = @clients.get_all_columns_names
      respond_to do |format|
        format.js { render 'filter.js.erb' }
      end
    end

    private

    def set_readonly
      if params[:readonly] == 'true'
        @readonly = true
      else
        @readonly = false
      end
    end

    def set_client
      @client = Client.friendly.find(params[:id])
    end

    def set_client_for_contact
      @client = Client.find(params[:id])
    end

    def set_client_for_product
      @client = Client.find(params[:id])
    end

    def set_client_for_document
      @client = Client.find(params[:id])
    end

    def set_address
      @address = Address.find(params[:address_id])
    end

    def set_phone
      @phone = Phone.find(params[:phone_id])
    end

    def set_contact
      @contact = Contact.find(params[:contact_id])
    end

    def set_sender
      @sender = Sender.find(params[:sender_id])
    end

    def set_client_product
      @product = ClientProduct.find(params[:product_id])
    end

    def set_client_document
      @client_document = ClientDocument.find(params[:document_id])
    end

    def client_params
      params.require(:client).permit(:name, :rfc, :website, :parent_entity_id, :currency_id, :payment_term_id,
                                     :pay_like, :cfdi_id, :way_pay_id, :credit_limit, :foreign, :contract, :has_credit,
                                     :payment_method_id, :authorized_credit, :logo, :status, :middleman_id,
                                     :advanced_parameters,
                                     addresses_attributes: addresses_attributes,
                                     phones_attributes: phones_attributes,
                                     contacts_attributes: contacts_attributes)
    end

    def address_params
      params.require(:address).permit(:id, :addressable_type, :name, :addressable_id, :street, :interior_number,
                                      :outdoor_number, :city, :zip_code, :country, :state_id, :colony, :principal,
                                      address_type_ids: [])
    end

    def phone_params
      params.require(:phone).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code, :type_phone,
                                    :mobile_number)
    end

    def phones_params
      params.require(:phones).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code,
                                     :type_phone, :principal, :mobile_number)
    end

    def addresses_params
      params.require(:addresses).permit(:id, :addressable_type, :addressable_id, :street, :interior_number,
                                        :outdoor_number, :city, :zip_code, :country, address_type_ids: [])
    end

    def contact_params
      params.require(:contact).permit(:id, :contactable_id, :contactable_type, :name, :email, :area,
                                      :job, :birth_date, :principal, :photo,
                                      phones_attributes: phones_attributes)
    end

    def sender_params
      params.require(:sender).permit(:id, :senderable_type, :senderable_id, :contact, :name, :rfc, :email,
                                     :address, sender_type_ids: [], phones_attributes: phones_attributes)
    end

    def client_product_params
      params.require(:client_product).permit(:client_id, :product_id, :tariff_fraction, :image, :tag_list)
    end

    def addresses_attributes
      %i[id _destroy addressable_type addressable_id name street interior_number outdoor_number country city zip_code
         principal address_type_ids state_id colony]
    end

    def phones_attributes
      %i[id _destroy phoneable_type phoneable_id code number extension principal type_phone mobile_number]
    end

    def contacts_attributes
      [:id, :_destroy, :contactable_type, :contactable_id, :name, :email, :birth_date, :principal,
       :area, :job, { phones_attributes: phones_attributes, addresses_attributes: addresses_attributes }]
    end

    def senders_attributes
      [:id, :_destroy, :contactable_type, :contactable_id, :name, :email, :birth_date, :principal,
       :area, :job, { phones_attributes: phones_attributes }]
    end

    def client_document_params
      params.require(:client_document).permit(:client_id, :document_id, :deleted_at, :file)
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.client.other').html_safe, admin_clients_path
    end

    def set_default_columns
      set_columns(%w[name website rfc status])
    end

    def set_columns(columns)
      @column_names = @clients.get_columns_names(columns)
    end

    def get_columns
      @column_names
    end
  end
end
