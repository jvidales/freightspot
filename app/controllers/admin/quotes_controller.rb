module Admin
  # Quotes Controller
  class QuotesController < Admin::ApplicationController
    before_action :set_quote, only: %i[show destroy authorize]
    before_action :add_index_breadcrumb_metronic, only: %i[show]

    load_and_authorize_resource

    respond_to :html, :js, only: [:index]

    def index
      add_breadcrumbs_metronic I18n.translate('activerecord.models.quote.other')
      # @filter = params.slice(:service_mode) unless params[:service_mode].blank?
      # @filter = @filter[:service_mode] unless params[:service_mode].blank?
      @quotes = Quote.filter(params.slice(:service_mode, :status)).where('authorized = false').page(params[:page])
      respond_with(@quotes)
    end

    def show
      @expenses = 0
      @incomes = 0
      @profit = 0
      @charges = @quote.charges
      @charges.each do |charge|
        @expenses += charge.total.to_f if charge.type_charge == 'expense'
        @incomes += charge.total.to_f if charge.type_charge == 'income'
        @profit = @incomes.to_f - @expenses.to_f
      end
      add_breadcrumbs_metronic @quote.number.html_safe
    end

    def destroy
      @quote.destroy
      flash[:error] = 'Quote successfully deleted'
      redirect_to admin_quotes_path
    end

    def authorize
      shipment = initialize_shipment(@quote)
      shipment.quote_id = @quote.id
      shipment.shipment_units = initialize_shipment_units(@quote.quote_units)
      shipment.collection_route = initialize_collection_routes(@quote.origin_route)
      shipment.delivery_route = initialize_delivery_route(@quote.destiny_route)
      if shipment.save
        @quote.update(authorized: true)
        flash[:success] = t('controllers.quotes.authorize')
        redirect_to admin_quotes_path
      else
        flash[:error] = 'Review the data'
      end
    end

    private

    def set_quote
      @quote = Quote.friendly.find(params[:id])
    end

    def initialize_shipment(quote)
      Shipment.new(quote.attributes.except!('quoted_at', 'rate_id', 'expired_at', 'payment_term_id',
                                            'organization_id', 'service_type', 'total_weight_volume',
                                            'is_active_shipment', 'authorized', 'id', 'status'))
    end

    def initialize_shipment_units(quote_units)
      shipment_units = []
      quote_units.each do |quote_unit|
        shipment_unit = ShipmentUnit.new(quote_unit.attributes.except('quote_id', 'service_id'))
        shipment_units << shipment_unit
      end
      shipment_units
    end

    def initialize_collection_routes(quote_origin)
      CollectionRoute.new(quote_origin.attributes.except('quote_id'))
    end

    def initialize_delivery_route(quote_destiny)
      DeliveryRoute.new(quote_destiny.attributes.except('quote_id'))
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.quote.other').html_safe, admin_quotes_path
    end
  end
end
