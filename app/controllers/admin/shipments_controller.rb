module Admin
  # Class controller Shipments
  class ShipmentsController < Admin::ApplicationController
    before_action :set_shipment, only: %i[show destroy]
    before_action :add_index_breadcrumb_metronic, only: %i[show]

    load_and_authorize_resource

    respond_to :html, :js, only: [:index]

    def index
      @filter = params.slice(:service_mode) unless params[:service_mode].blank?
      @filter = @filter[:service_mode] unless params[:service_mode].blank?
      @shipments = Shipment.filter(params.slice(:service_mode)).page(params[:page])
      respond_with(@shipments)
      add_breadcrumbs_metronic I18n.translate('activerecord.models.shipment.other')
    end

    def show
      add_breadcrumbs_metronic @shipment.number.html_safe
    end

    private

    def set_shipment
      @shipment = Shipment.friendly.find(params[:id])
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.shipment.other').html_safe, admin_quotes_path
    end
  end
end
