module Admin
  # Creates the CRUD for the ports.
  class PortsController < Admin::ApplicationController
    before_action :set_port, only: %i[show edit update destroy]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    load_and_authorize_resource

    def index
      @ports = Port.all
      add_breadcrumbs_metronic I18n.translate('activerecord.models.port.other')
      @geojson = build_geojson
      puts "\n\nEntro"
      puts @geojson
    end

    def show
      add_breadcrumbs_metronic @port.port_identifier
    end

    def new
      @port = Port.new
      add_breadcrumbs_metronic t('views.admin.ports.new.subtitle').to_s.html_safe
    end

    def create
      @port = Port.new(port_params)
      if @port.save
        flash[:success] = I18n.translate('controllers.ports.create')
        redirect_to admin_ports_path
      else
        render :new
      end
    end

    def edit
      add_breadcrumbs_metronic "#{t('views.admin.ports.edit.subtitle')} #{@port.port_identifier}".html_safe
    end

    def update
      if @port.update(port_params)
        flash[:success] = I18n.translate('controllers.ports.update')
        redirect_to admin_ports_path
      else
        render :edit
      end
    end

    def destroy
      @port.destroy
      flash[:error] = I18n.translate('controllers.ports.destroy')
      redirect_to admin_ports_path
    end

    private

    def set_port
      @port = Port.friendly.find(params[:id])
    end

    def port_params
      params.require(:port).permit(:id, :port_identifier, :iata_code, :name, :transportation_method,
                                   :inactive, :country_id)
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.port.other').html_safe, admin_ports_path
    end

    def build_geojson
      {
        type: 'FeatureCollection',
        features: @ports.map(&:to_feature)
      }
    end
  end
end
