module Admin
  # Creates the CRUD for the taxes.
  class TaxesController < Admin::ApplicationController
    before_action :set_tax, only: %i[edit show update destroy]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    load_and_authorize_resource

    def index
      @taxes = Tax.all
      add_breadcrumbs_metronic I18n.translate('activerecord.models.tax.other')
    end

    def new
      @tax = Tax.new
      add_breadcrumbs_metronic t('views.admin.taxes.new.subtitle').to_s.html_safe
    end

    def create
      @tax = Tax.new(tax_params)
      if @tax.save
        flash[:success] = I18n.translate('controllers.taxes.create')
        redirect_to admin_taxes_path
      else
        render :new
      end
    end

    def update
      if @tax.update(tax_params)
        flash[:success] = I18n.translate('controllers.taxes.update')
        redirect_to admin_taxes_path
      else
        render :edit
      end
    end

    def edit
      add_breadcrumbs_metronic "#{t('views.admin.taxes.edit.subtitle')} #{@tax.code}".html_safe
    end

    def destroy
      @tax.destroy
      flash[:error] = I18n.translate('controllers.taxes.destroy')
      redirect_to admin_taxes_path
    end

    private

    def tax_params
      params.require(:tax).permit(:code, :name, :value)
    end

    def set_tax
      @tax = Tax.friendly.find(params[:id])
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.tax.other').html_safe, admin_taxes_path
    end
  end
end
