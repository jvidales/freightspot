module Admin
  # Creates the CRUD for the agents
  class AgentsController < Admin::ApplicationController
    include Utils::Search
    include PhonesMethods
    include SharedMethods
    include ContactsMethods
    include AddressesMethods
    include Reports::DashboardReport

    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]
    before_action :set_readonly, only: %i[show]
    before_action :set_agent, only: %i[show edit update destroy new_address edit_address set_addresses destroy_address
                                       set_phones]
    before_action :set_phones, only: %i[show]
    before_action :set_contacts, only: %i[show]
    before_action :set_addresses, only: %i[show]
    before_action :set_agent_for_phone, only: %i[add_phone update_phone]
    before_action :set_agent_for_contact, only: %i[add_contact update_contact]

    load_and_authorize_resource

    def index
      add_breadcrumbs_metronic I18n.translate('activerecord.models.agent.other').html_safe
      @agents = Agent.order(:name)
    end

    def show
      add_breadcrumbs_metronic @agent.name.to_s.html_safe
      dashboard
      respond_to do |format|
        format.js { render 'show.js.erb' }
        format.html
      end
    end

    def new
      @agent = Agent.new
      @agent.addresses.build
      @agent.phones.build
      @agent.contacts.build.phones.build
      add_breadcrumbs_metronic I18n.translate('views.admin.agents.new.subtitle').html_safe
    end

    def edit; end

    def create
      @agent = Agent.new(agent_params)
      if @agent.save
        flash[:success] = t('controllers.agents.create')
        redirect_to admin_agents_path
      else
        render :new
      end
    end

    def update
      respond_to do |format|
        if @agent.update(agent_params)
          @response = t('controllers.agents.update')
          format.js
        else
          render :show
        end
      end
    end

    def new_contact
      super
      @contacts = contacts(@agent)
    end

    def edit_contact
      super
      @contacts = contacts(@agent)
    end

    private

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.agent.other').html_safe, admin_agents_path
    end

    def set_agent
      @agent = Agent.friendly.find(params[:id])
    end

    def agent_params
      params.require(:agent).permit(:name, :website, :rfc, :payment_term_id, :currency_id, :cfdi_id, :way_pay_id,
                                    :payment_method_id, :logo,
                                    phones_attributes: phones_attributes,
                                    addresses_attributes: addresses_attributes,
                                    contacts_attributes: contacts_attributes)
    end

    def phones_attributes
      %i[id phoneable_type phoneable_id code number extension type_phone principal mobile_number]
    end

    def addresses_attributes
      %i[id addressable_type addressable_id name street interior_number outdoor_number colony city country state_id
         zip_code principal address_type_ids]
    end

    def contacts_attributes
      [:id, :contactable_type, :contactable_id, :name, :email, :area, :job, :birth_date, :principal,
       { phones_attributes: phones_attributes },
       { addresses_attributes: addresses_attributes }]
    end

    def set_contacts
      contacts(@agent)
    end

    def set_phones
      @contacts = @agent.contacts.order('principal DESC').page params[:page]
      @phones = phone_list(@agent, @contacts, [])
    end

    def set_agent_for_phone
      @agent = model_with_phoneable(Agent)
    end

    def set_agent_for_contact
      @agent = model_with_contactable(Agent)
    end
  end
end
