# All Administrate controllers inherit from this
# `Administrate::ApplicationController`, making it the ideal place to put
# authentication logic or other before_actions.
#
# If you want to add pagination or other controller-level concerns,
# you're free to overwrite the RESTful controller actions.
module Admin
  # Application Controller
  class ApplicationController < Administrate::ApplicationController
    protect_from_forgery with: :exception

    before_action :authenticate_admin!
    before_action :set_locale
    before_action :set_last_seen_at, if: :user_signed_in?
    before_action :set_breadcrumb_metronic

    protected

    def authenticate_admin!
      redirect_to '/', alert: 'Not unauthorized.' unless user_signed_in? && current_user.admin?
    end

    def set_locale
      locale = current_user.setting.language.code.to_s.strip.to_sym
      I18n.locale = if I18n.available_locales.include?(locale)
                      locale
                    else
                      I18n.default_locale
                    end
    end

    def default_url_options
      { locale: I18n.locale }
    end

    def set_last_seen_at
      current_user.touch(:last_seen_at)
    end

    def add_breadcrumbs_metronic(label, path = nil)
      @breadcrumbs << {
        label: label,
        path: path
      }
    end

    def set_breadcrumb_metronic
      @breadcrumbs = []
    end

    rescue_from CanCan::AccessDenied do |_exception|
      flash[:error] = t('views.flash.unauthorized')
      redirect_to admin_root_url
    end
  end
end
