module Admin
  # Packaging Type Controller
  class PackagingTypesController < Admin::ApplicationController
    before_action :set_packaging, only: %i[edit show update destroy]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    load_and_authorize_resource

    def index
      @packaging_types = PackagingType.all
      add_breadcrumbs_metronic I18n.translate('activerecord.models.packaging_type.other').html_safe
    end

    def new
      @packaging_type = PackagingType.new
      add_breadcrumbs_metronic I18n.translate('views.admin.packaging_types.new.subtitle').html_safe
    end

    def create
      @packaging_type = PackagingType.new(packaging_type_params)
      if @packaging_type.save
        flash[:success] = I18n.translate('controllers.packaging_types.create')
        redirect_to admin_packaging_types_path
      else
        render :new
      end
    end

    def edit
      flash_message = "#{I18n.translate('views.admin.packaging_types.edit.subtitle')} #{@packaging_type.code}".html_safe
      add_breadcrumbs_metronic flash_message
    end

    def update
      if @packaging_type.update(packaging_type_params)
        flash[:success] = I18n.translate('controllers.packaging_types.update')
        redirect_to admin_packaging_types_path
      else
        render :update
      end
    end

    def destroy
      @packaging_type.destroy
      flash[:error] = I18n.translate('controllers.packaging_types.destroy')
      redirect_to admin_packaging_types_path
    end

    private

    def packaging_type_params
      params.require(:packaging_type).permit(:code, :name, :definition)
    end

    def set_packaging
      @packaging_type = PackagingType.friendly.find(params[:id])
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.packaging_type.other').html_safe,
                               admin_packaging_types_path
    end
  end
end
