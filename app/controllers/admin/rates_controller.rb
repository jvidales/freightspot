module Admin
  # Rates controller
  class RatesController < Admin::ApplicationController
    include PortsMethods
    before_action :set_rate, only: %i[edit update destroy]

    def edit; end

    def update
      respond_to do |format|
        if @rate.update(rate_params)
          @response = t('controllers.rates.update')
          format.js { render layout: false, content_type: 'text/javascript' }
        else
          format.js { render :edit }
        end
      end
    end

    def destroy
      @tariff = Tariff.find(@rate.tariff_id)
      if @tariff.rates.size == 1
        @tariff.destroy
      else
        @rate.destroy
      end
      flash[:error] = I18n.translate('controllers.rates.destroy')
      redirect_to admin_tariffs_path
    end

    private

    def set_rate
      @rate = Rate.find(params[:id])
    end

    def rate_params
      params.require(:rate).permit(:id, :tariff_id, :origin_port_id, :discharge_port_id, :freight_rate, :minimal_cost,
                                   :container_id, :measurement_unit, :transite_time, :frequency, :via_code, :via,
                                   :free_days, :guarantee_letter)
    end
  end
end
