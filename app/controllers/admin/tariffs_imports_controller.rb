module Admin
  # TariffsImports Controller
  class TariffsImportsController < ApplicationController
    def create
      @tariffs_import = TariffsImport.new(params[:tariffs_import])
      if @tariffs_import.save
        flash[:success] = 'Import completed successfully'
        redirect_to admin_tariffs_path
      end
    end
  end
end
