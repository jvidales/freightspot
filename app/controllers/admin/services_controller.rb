module Admin
  # Class controller Services
  class ServicesController < Admin::ApplicationController
    before_action :set_service, only: %i[edit show update destroy]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    load_and_authorize_resource

    def index
      @services = Service.all
      add_breadcrumbs_metronic I18n.translate('activerecord.models.service.other').html_safe
    end

    def show
      add_breadcrumbs_metronic @service.code.to_s.html_safe
    end

    def new
      @service = Service.new
      add_breadcrumbs_metronic I18n.translate('views.admin.services.new.subtitle').html_safe
    end

    def create
      @service = Service.new(service_params)
      if @service.save
        flash[:success] = I18n.translate('controllers.services.create')
        redirect_to admin_services_path
      else
        render :new
      end
    end

    def edit
      add_breadcrumbs_metronic "#{I18n.translate('views.admin.services.edit.subtitle')} #{@service.code}".html_safe
    end

    def update
      if @service.update(service_params)
        flash[:success] = I18n.translate('controllers.services.update')
        redirect_to admin_services_path
      else
        render :update
      end
    end

    def destroy
      @service.destroy
      flash[:error] = I18n.translate('controllers.services.destroy')
      redirect_to admin_services_path
    end

    private

    def service_params
      params.require(:service).permit(:code, :description, :price, :has_cost, :service_type, :currency_id,
                                      :tax_id, :service_mode_id)
    end

    def set_service
      @service = Service.friendly.find(params[:id])
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.service.other').html_safe, admin_services_path
    end
  end
end
