module Admin
  # Creates the CRUD for the containers
  class ContainersController < Admin::ApplicationController
    before_action :set_container, only: %i[edit show update destroy]
    before_action :set_specifications, only: %i[new edit]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    load_and_authorize_resource

    def index
      @containers = Container.all.order(:name)
      add_breadcrumbs_metronic I18n.translate('activerecord.models.container.other')
    end

    def show
      add_breadcrumbs_metronic @container.code.to_s.html_safe
    end

    def new
      add_breadcrumbs_metronic I18n.translate('views.admin.containers.new.subtitle').html_safe
      @container = Container.new
      @specifications.each do |s|
        @container.container_dimensions.build(name: s)
      end
    end

    def create
      @container = Container.new(container_params)
      if @container.save
        redirect_to admin_containers_path
        flash[:success] = t('controllers.containers.create')
      else
        render :new
      end
    end

    def edit
      add_breadcrumbs_metronic "#{t('views.admin.containers.edit.subtitle')} #{@container.code}".html_safe
    end

    def update
      if @container.update(container_params)
        redirect_to admin_containers_path
        flash[:success] = t('controllers.containers.update')
      else
        render :update
      end
    end

    def destroy
      @container.destroy
      flash[:error] = t('controllers.containers.destroy')
      redirect_to admin_containers_path
    end

    private

    def container_params
      params.require(:container).permit(:id, :code, :name, :description,
                                        container_dimensions_attributes: container_dimensions_attributes)
    end

    def container_dimensions_attributes
      %i[id name metric_value metric_measurement_id imperial_value imperial_measurement_id
         container_id]
    end

    def set_container
      @container = Container.friendly.find(params[:id])
    end

    def set_specifications
      @specifications = ['Tare Weight', 'Payload Capacity', 'Maximum Gross Weight', 'Cubic Capacity',
                         'Internal Length', 'Internal Width', 'Internal Height', 'External Length',
                         'External Width', 'External Height', 'Door Opening Width', 'Door Opening Height']
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.container.other').html_safe, admin_containers_path
    end
  end
end
