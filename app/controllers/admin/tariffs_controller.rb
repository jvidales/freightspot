module Admin
  # Creates the CRUD for the tariffs
  class TariffsController < Admin::ApplicationController
    include RatesMethods

    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]
    before_action :set_tariff, only: %i[show edit update destroy]

    load_and_authorize_resource

    respond_to :html, :js, only: [:index]

    def index
      add_breadcrumbs_metronic I18n.translate('activerecord.models.rate.other')
      @tariffs_import = TariffsImport.new
      @filter = params.slice(:service_mode) unless params[:service_mode].blank?
      @filter = @filter[:service_mode] unless params[:service_mode].blank?
      @rates = Rate.filter(params.slice(:service_mode)).order(:minimal_cost).page(params[:page])
      respond_with(@rates)
    end

    def show
      @rate = Rate.find(params[:rate_id])
      @surcharges = @rate.rate_surcharges
      add_breadcrumbs_metronic @tariff.id
    end

    def new
      @tariff = Tariff.new
      @tariff.rates.build
      add_breadcrumbs_metronic t('views.admin.tariffs.new.title').to_s.html_safe
    end

    def create
      @tariff = Tariff.new(tariff_params)
      if @tariff.save
        flash[:success] = t('controllers.tariffs.create')
        redirect_to admin_tariffs_path
      else
        render :new
      end
    end

    def filter_freight_types
      service_mode_variable = params[:service_mode]
      service_mode = ServiceMode.find_by_variable(service_mode_variable)
      @freight_types = ModeTransportation.where('service_mode_id = ?', service_mode.id)
      respond_to do |format|
        format.json { render json: @freight_types }
      end
    end

    def filter_carriers
      service_mode = ServiceMode.find_by_variable(params[:service_mode])
      @carriers = Supplier.filter_by_service_mode(service_mode.id)
      respond_to do |format|
        format.json { render json: @carriers }
      end
    end

    def port_code
      port = Port.find(params[:port_id])
      @port_code = port.port_identifier
      respond_to do |format|
        format.json { render json: @port_code }
      end
    end

    private

    def tariff_params
      params.require(:tariff).permit(:id, :carrier_id, :service_mode, :freight_type_id, :started_at, :ended_at,
                                     rates_attributes: rates_attributes)
    end

    def set_tariff
      @tariff = Tariff.find(params[:id])
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.rate.other').html_safe, admin_tariffs_path
    end
  end
end
