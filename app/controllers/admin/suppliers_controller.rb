module Admin
  # Creates the CRUD for the suppliers.
  class SuppliersController < Admin::ApplicationController
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]
    before_action :set_supplier, only: %i[edit show destroy update new_address show_address
                                          edit_address update_address destroy_address
                                          new_phone show_phone destroy_phone edit_phone
                                          update_phone]
    before_action :set_readonly, only: %i[show]
    before_action :set_supplier_for_contact, only: %i[new_contact show_contact edit_contact destroy_contact]
    before_action :set_address, only: %i[show_address edit_address update_address destroy_address]
    before_action :set_phone, only: %i[show_phone edit_phone update_phone destroy_phone]
    before_action :set_contact, only: %i[show_contact edit_contact update_contact destroy_contact]

    load_and_authorize_resource

    def index
      @suppliers = Supplier.all
      add_breadcrumbs_metronic I18n.translate('activerecord.models.supplier.other').html_safe
      respond_to do |format|
        format.html
        format.pdf {
          render pdf: 'Client No.',
                 disposition: 'inline',
                 page_size: 'A4',
                 template: 'admin/suppliers/_collection.html.slim',
                 layout: 'template',
                 orientation: 'Portrait',
                 lowquality: true,
                 zoom: 1,
                 dpi: 75
        }
      end
    end

    def show
      @addresses = @supplier.addresses.page params[:page]
      @contacts = @supplier.contacts.order('principal DESC').page params[:page]
      @phones = @supplier.phones
      dashboard
      # @documents = @client.client_documents.page params[:page]
      add_breadcrumbs_metronic @supplier.name.to_s.html_safe
    end

    def dashboard
      @serie = [
        { name: 'Embarque FS-001', data: 20 },
        { name: 'Embarque FS-002', data: 15 },
        { name: 'Embarque FS-003', data: 35 },
        { name: 'Embarque FS-004', data: 10 },
        { name: 'Embarque FS-005', data: 20 },
      ]
      User.default_timezone = :utc
      @user_date = User.group_by_day(:created_at).count
      @options = {
        title: 'Properties Growth',
        subtitle: 'Grouped Per Day',
        xtitle: 'Day',
        ytitle: 'Properties',
        stacked: true,
        height: 200
      }
    end

    def new
      @supplier = Supplier.new
      @supplier.addresses.build
      @supplier.phones.build
      @supplier.contacts.build.phones.build
      add_breadcrumbs_metronic I18n.translate('views.admin.suppliers.shared.new.subtitle').html_safe
    end

    def edit; end

    def create
      @supplier = Supplier.new(supplier_params)
      @supplier.contacts.first.addresses << clone_address(@supplier.addresses.first)
      if @supplier.save
        flash[:success] = I18n.translate('controllers.supplier.create')
        redirect_to admin_suppliers_path
      else
        render :new
      end
    end

    def update
      if @supplier.update(supplier_params)
        @response = I18n.translate('controllers.supplier.update')
        respond_to do |format|
          format.js
        end
      else
        render :show
      end
    end

    def destroy
      @supplier.destroy
      flash[:error] = I18n.translate('controllers.supplier.destroy')
      redirect_to admin_suppliers_path
    end

    def new_address
      @address = Address.new
    end

    def show_address; end

    def add_address
      @create = false
      @address = Address.new(address_params)
      @supplier = Supplier.find(@address.addressable_id)
      respond_to do |format|
        if @address.save
          @create = true
          @addresses = @supplier.addresses.page params[:page]
          @response = I18n.translate('controllers.addresses.create')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def edit_address; end

    def update_address
      @update = false
      respond_to do |format|
        if @address.update(address_params)
          @addresses = @supplier.addresses.page params[:page]
          @update = true
          @response = I18n.translate('controllers.addresses.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_address
      @address.destroy
      @addresses = @supplier.addresses.page params[:page]
      @response  = t('controllers.addresses.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def clone_address(address)
      cloneAddress = Address.new
      cloneAddress.name = address.name
      cloneAddress.street = address.street
      cloneAddress.interior_number = address.interior_number
      cloneAddress.outdoor_number = address.outdoor_number
      cloneAddress.colony = address.colony
      cloneAddress.city = address.city
      cloneAddress.country = address.country
      cloneAddress.state_id = address.state_id
      cloneAddress.zip_code = address.zip_code
      cloneAddress.address_type_ids = address.address_type_ids
      cloneAddress.principal = address.principal
      cloneAddress
    end

    def new_phone
      @phone = Phone.new
    end

    def add_phone
      @create = false
      @phone = Phone.new(phone_params)
      respond_to do |format|
        if @phone.save
          @phones = @supplier.phones
          @create = true
          @response = t('controllers.phones.create')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def show_phone; end

    def edit_phone; end

    def update_phone
      @update = false
      respond_to do |format|
        if @phone.update(phone_params)
          @phones = @supplier.phones
          @update = true
          @response = t('controllers.phones.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_phone
      @phone.destroy
      @phones = @supplier.phones
      @response  = t('controllers.phones.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def new_contact
      @contact = Contact.new
      @contact.addresses.build
      @contact.phones.build
    end

    def add_contact
      @create = false
      @contact = Contact.new(contact_params)
      @client = Client.find(@contact.contactable_id)
      address = Address.find(params[:address][:id])
      @contact.addresses << clone_address(address)
      respond_to do |format|
        if @contact.save
          @create = true
          @response = t('controllers.contacts.create')
          @contacts = @supplier.contacts.order('principal DESC').page params[:page]
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def show_contact; end

    def edit_contact
      @contact.phones.build if @contact.phones.blank?
    end

    def update_contact
      @update = false
      @supplier = Supplier.find(@contact.contactable_id)
      respond_to do |format|
        if @contact.update(contact_params)
          @contacts = @supplier.contacts.order('principal DESC').page params[:page]
          @update = true
          @response = t('controllers.contacts.update')
          format.js { render layout: false, content_type: 'text/javascript' }
          format.html {}
        end
      end
    end

    def destroy_contact
      @supplier = Supplier.find(@contact.contactable_id)
      @contact.destroy
      @contacts = @supplier.contacts.order('principal DESC').page params[:page]
      @response = t('controllers.contacts.destroy')
      respond_to do |format|
        format.html {}
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    def filter_states
      country_name = params[:country_id]
      country = Country.find_by_name(country_name)
      @states = State.where('country_id = ?', country.id)
      respond_to do |format|
        format.json { render json: @states }
      end
    end

    def update_readonly
      if params[:readonly] == 'true'
        @readonly = true
      else
        @readonly = false
      end
      respond_to do |format|
        format.js
      end
    end

    private

    def set_readonly
      if params[:readonly] == 'true'
        @readonly = true
      else
        @readonly = false
      end
    end

    def set_supplier
      @supplier = Supplier.friendly.find(params[:id])
    end

    def set_supplier_for_contact
      @supplier = Supplier.find(params[:id])
    end

    def set_address
      @address = Address.find(params[:address_id])
    end

    def set_phone
      @phone = Phone.find(params[:phone_id])
    end

    def set_contact
      @contact = Contact.find(params[:contact_id])
    end

    def supplier_params
      params.require(:supplier).permit(:name, :rfc, :website, :parent_entity, :currency_id, :payment_term_id,
                                       :pay_like, :cfdi_id, :way_pay_id, :credit_limit, :foreign, :contract,
                                       :has_credit, :payment_method_id, :authorized_credit, :logo,
                                       :transportation_method, :iata_code, :caat_code, :scac_number,
                                       :parent_entity_id, service_mode_ids: [],
                                                          addresses_attributes: address_attributes,
                                                          phones_attributes: phones_attributes,
                                                          contacts_attributes: contacts_attributes)
    end

    def address_params
      params.require(:address).permit(:id, :addressable_type, :name, :addressable_id, :street, :interior_number,
                                      :outdoor_number, :city, :zip_code, :country, :state_id, :colony, :principal,
                                      address_type_ids: [])
    end

    def phone_params
      params.require(:phone).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code, :type_phone,
                                    :mobile_number)
    end

    def contact_params
      params.require(:contact).permit(:id, :contactable_id, :contactable_type, :name, :email, :area,
                                      :job, :birth_date, :principal, :photo,
                                      phones_attributes: phones_attributes)
    end

    def addresses_params
      params.require(:addresses).permit(:id, :addressable_type, :addressable_id, :street, :interior_number,
                                        :outdoor_number, :city, :zip_code, :country, address_type_ids: [])
    end

    def phones_params
      params.require(:phones).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code,
                                     :type_phone, :principal)
    end

    def address_attributes
      %i[id _destroy addressable_type addressable_id name street interior_number outdoor_number country city zip_code
         principal address_type_ids state_id colony]
    end

    def phones_attributes
      %i[id _destroy phoneable_type phoneable_id code number extension principal type_phone mobile_number]
    end

    def contacts_attributes
      [:id, :_destroy, :contactable_type, :contactable_id, :name, :email, :birth_date, :principal,
       :area, :job, phones_attributes: phones_attributes]
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.supplier.other').html_safe, admin_suppliers_path
    end
  end
end
