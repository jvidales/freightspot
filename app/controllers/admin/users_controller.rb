module Admin
  # Creates the CRUD for the users.
  class UsersController < Admin::ApplicationController
    before_action :set_user, only: %i[edit show update destroy]
    before_action :allow_without_password, only: [:update]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    load_and_authorize_resource

    def index
      @users = User.all
      add_breadcrumbs_metronic I18n.translate('activerecord.models.user.other')
    end

    def show
      add_breadcrumbs_metronic @user.username.to_s.html_safe
    end

    def new
      @user = User.new
      @user.build_employee
      add_breadcrumbs_metronic I18n.translate('views.admin.users.new.subtitle').html_safe
    end

    def edit
      add_breadcrumbs_metronic "#{t('views.admin.users.edit.subtitle')} #{@user.username}".html_safe
    end

    def create
      @user = User.new(user_params)
      if @user.save
        flash[:success] = I18n.translate('controllers.user.create')
        redirect_to admin_users_path
      else
        render :new
      end
    end

    def update
      form = params[:form]
      if @user.update(user_params)
        if form == 'Employee'
          flash[:success] = I18n.translate('controllers.user.update')
          redirect_to admin_users_path
        else
          flash[:success] = 'User client successfully updated'
          redirect_to admin_clients_path
        end
      else
        render :edit
      end
    end

    def destroy
      current_user
      @user.destroy
      flash[:error] = I18n.translate('controllers.user.destroy')
      redirect_to admin_users_path
    end

    private

    def user_params
      params.require(:user).permit(:avatar, :username, :name, :time_zone, :email, :password, :password_confirmation,
                                   :admin, :role_id, employee_attributes: employee_attributes)
    end

    def employee_attributes
      %i[name last_name birth_date phone phone_extension origin city office_branch
         squad job_title join_date language mobile id user_id]
    end

    def set_user
      @user = User.friendly.find(params[:id])
    end

    def allow_without_password
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
      end
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.user.other').html_safe, admin_users_path
    end
  end
end
