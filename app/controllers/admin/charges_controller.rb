module Admin
  # Creates the CRUD for the charges
  class ChargesController < Admin::ApplicationController
    before_action :set_charge, only: %i[edit show destroy_charge update]
    def show; end
    
    def new
      puts "PARAMS"
      puts "#{params}"

      if params[:chargeable_type] == "Quote"
        @charge = Charge.new
      else  
        @shipment = Shipment.find(params[:shipment_id])
        @type_charge = params[:type_charge]
        @charge = Charge.new
        @charge.type_charge = get_type_charge(@type_charge)
        @services = Service.where("service_mode_id = ? AND service_type = ?", get_type_service(@shipment.transportation_method), get_type_charge(@type_charge)).order(:description)

      end
    end

    def edit
      @shipment = Shipment.find(@charge.chargeable_id)
      @type_charge = @charge.type_charge
      @charge.type_charge = get_type_charge(@type_charge)
      @services = Service.where("service_mode_id = ? AND service_type = ?", get_type_service(@shipment.transportation_method), get_type_charge(@type_charge)).order(:description)
    end

    def create
      @create = false
      @charge = Charge.new(charge_params)
      if @charge.type_charge == 'income'
        @charge.apply_type = 'Client'
      else
        @charge.apply_type = 'Supplier'
      end
      if @charge.save
        @create = true
        flash[:success] = 'Charge successfully created'
      else
        @type_charge = @charge.type_charge
        @shipment = Shipment.find(@charge.chargeable_id)
        @services = Service.where("service_mode_id = ? AND service_type = ?", get_type_service(@shipment.transportation_method), get_type_charge(@type_charge)).order(:description)
      end
    end

    def update
      @update = false
      if @charge.update(charge_params)
        @update = true
        flash[:success] = 'Charge successfully updated'
      else
        @type_charge = @charge.type_charge
        @shipment = Shipment.find(@charge.chargeable_id)
        @services = Service.where("service_mode_id = ? AND service_type = ?", get_type_service(@shipment.transportation_method), get_type_charge(@type_charge)).order(:description)
      end
    end

    def destroy_charge
      @charge.destroy
      flash[:error] = "Charge succesfully removed"
      redirect_to admin_shipments_path
    end

    def get_type_charge(type)
      value = nil
      case (type)
      when 'income'
        value = 0
      when 'expense'
        value = 1
      end
      value
    end

    def get_type_service(type)
      value = nil
      case (type)
      when 'aerial'
        value = 2
      when 'land'
        value = 4
      when 'maritime'
        value = 1
      end
    end

    private
    def charge_params
      params.require(:charge).permit(:id, :chargeable_type, :chargeable_id, :service_id, :currency_id, :tax_id,
                                     :description, :apply_to_able_type, :apply_to_able_id, :type_charge, :number_pieces,
                                     :total_weight, :total_volume, :weight_to_charge, :quantity, :unit, :price, :total)
    end

    def set_charge
      @charge = Charge.find(params[:id])
    end
  end
end