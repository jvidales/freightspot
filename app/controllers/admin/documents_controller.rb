module Admin
  # Creates the CRUD for documents
  class DocumentsController < Admin::ApplicationController
    before_action :set_document, only: %i[show edit update destroy]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit]

    load_and_authorize_resource

    def index
      @documents = Document.order('name ASC')
      add_breadcrumbs_metronic I18n.translate('activerecord.models.document.other').html_safe
    end

    def show
      add_breadcrumbs_metronic @document.name.html_safe
    end

    def new
      @document = Document.new
      add_breadcrumbs_metronic t('views.admin.documents.new.subtitle').to_s.html_safe
    end

    def create
      if Document.new(document_params).save
        flash[:success] = I18n.translate('controllers.documents.create')
        redirect_to admin_documents_path
      else
        render :new
      end
    end

    def edit
      add_breadcrumbs_metronic "#{t('views.admin.documents.edit.subtitle')} #{@document.name}".html_safe
    end

    def update
      if @document.update(document_params)
        flash[:success] = I18n.translate('controllers.documents.update')
        redirect_to admin_document_path(@document.id)
      else
        render :edit
      end
    end

    def destroy
      @document.destroy
      flash[:error] = I18n.translate('controllers.documents.destroy')
      redirect_to admin_documents_path
    end

    private

    def set_document
      @document = Document.find(params[:id])
    end

    def document_params
      params.require(:document).permit(:id, :name, :obligatory, :document_type, :phase, :active, :supplier,
                                       :client)
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.document.other').html_safe, admin_documents_path
    end
  end
end
