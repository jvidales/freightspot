module Admin
  # Creates the CRUD for the roles
  class RolesController < Admin::ApplicationController
    before_action :set_role, only: %i[edit show update destroy assign]
    before_action :add_index_breadcrumb_metronic, only: %i[show new edit assign]

    load_and_authorize_resource

    def index
      @roles = Role.all.order(:name)
      add_breadcrumbs_metronic I18n.translate('activerecord.models.role.other')
    end

    def show
      add_breadcrumbs_metronic @role.name
    end

    def new
      add_breadcrumbs_metronic I18n.translate('views.admin.roles.new.subtitle').html_safe
      @role = Role.new
    end

    def create
      @role = Role.new(role_params)
      if @role.save
        flash[:success] = 'Role successfully created'
        redirect_to admin_roles_path
      else
        render :new
      end
    end

    def edit
      add_breadcrumbs_metronic "#{t('views.admin.roles.edit.subtitle')} #{@role.name}".html_safe
    end

    def update
      if @role.update(role_params)
        flash[:success] = 'Role successfully updated'
        redirect_to admin_roles_path
      else
        render :edit
      end
    end

    def destroy
      @role.destroy
      flash[:error] = 'Role successfully deleted'
      redirect_to admin_roles_path
    end

    def assign
      add_breadcrumbs_metronic "#{@role.name} - #{t('views.admin.roles.assign.subtitle')}".html_safe
      @access = AccessRole.where(role_id: @role.id)
    end

    private

    def role_params
      params.require(:role).permit(:name)
    end

    def set_role
      @role = Role.friendly.find(params[:id])
    end

    def add_index_breadcrumb_metronic
      add_breadcrumbs_metronic I18n.translate('activerecord.models.role.other').html_safe, admin_roles_path
    end
  end
end
