module Admin
  # Creates the rate_surcharge controller
  class RateSurchargesController < Admin::ApplicationController
    before_action :set_rate, only: %i[new edit]
    before_action :set_rate_surcharge, only: %i[edit update destroy]

    def new
      @rate_surcharge = RateSurcharge.new
    end

    def edit; end

    def create
      @rate_surcharge = RateSurcharge.new(rate_surcharge_params)
      @rate = Rate.find(@rate_surcharge.rate_id)
      respond_to do |format|
        if @rate_surcharge.save
          @response = t('controllers.rate_surcharge.create')
          @surcharges = @rate.rate_surcharges
          format.js { render layout: false, content_type: 'text/javascript' }
        else
          format.js { render :new }
        end
      end
    end

    def update
      @rate = Rate.find(@rate_surcharge.rate_id)
      respond_to do |format|
        if @rate_surcharge.update(rate_surcharge_params)
          @response = t('controllers.rate_surcharge.update')
          @surcharges = @rate.rate_surcharges
          format.js { render layout: false, content_type: 'text/javascript' }
        else
          format.js { render :edit }
        end
      end
    end

    def destroy
      @rate = Rate.find(@rate_surcharge.rate_id)
      @rate_surcharge.destroy
      respond_to do |format|
        @response = t('controllers.rate_surcharge.destroy')
        @surcharges = @rate.rate_surcharges
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end

    private

    def set_rate_surcharge
      @rate_surcharge = RateSurcharge.find(params[:id])
    end

    def set_rate
      @rate = Rate.find(params[:rate_id])
    end

    def rate_surcharge_params
      params.require(:rate_surcharge).permit(:id, :rate_id, :surcharge_id, :currency_id, :value, :type_surcharge)
    end
  end
end
