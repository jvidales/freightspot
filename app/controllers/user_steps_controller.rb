# UserSteps Controller
class UserStepsController < ApplicationController
  include Wicked::Wizard

  steps :client
  before_action :set_user, only: %i[show update]

  def show
    @user = User.find_by_username(params[:resource])
    @password = params[:password]
    @client = Client.new
    render_wizard
  end

  def update
    @password = params[:password]
    @client = Client.new(client_params)
    @client.contacts << @user.contact
    if @client.save
      RegistrationMailer.confirmation_email(@user, @password, @client).deliver
      sign_in(@user)
      redirect_to customer_root_path
    else
      render_wizard
    end
  end

  def finish_wizard_path
    client_path(current_user)
  end

  private

  def set_user
    @user = User.find_by_username(params[:resource])
  end

  def client_params
    params.require(:client)
          .permit(:name, :annual_international_freight_shipments, contact_attributes: contact_attributes) # ...
  end

  def contact_attributes
    %i[id contactable_type name email user_id]
  end
end
