# frozen_string_literal: true

module Users
  # Sessions Controller
  class SessionsController < Devise::SessionsController
    def after_sign_in_path_for(resource)
      case resource.role.slug
      when 'administrador'
        admin_root_path
      else
        customer_root_path
      end
    end
  end
end
