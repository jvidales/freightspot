# Application Controller
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  around_action :set_time_zone, if: :current_user
  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    admin_root_path
  end

  protected

  def authenticate_admin
    redirect_to root_path(locale: :es), alert: 'Not unauthorized.' unless user_signed_in? && current_user.admin?
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :accept_terms_of_service_and_privacy_policies,
                                                       { contact_attributes: contact_attributes }])
    devise_parameter_sanitizer.permit(:sign_in, keys: %i[email password])
  end

  def require_current_account_admin
    redirect_to root_path, alert: 'You must be an admin to do that.' unless current_account_admin?
  end

  def set_time_zone(&block)
    time_zone = current_user.try(:time_zone) || 'UTC'
    Time.use_zone(time_zone, &block)
  end

  def contact_attributes
    %i[id contactable_type name email user_id]
  end
end
