# == Schema Information
#
# Table name: service_modes
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  variable   :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_service_modes_on_deleted_at  (deleted_at)
#
class ServiceMode < ApplicationRecord
  audited

  has_many :services, inverse_of: :service_mode
  has_many :mode_transportations, inverse_of: :service_mode, dependent: :destroy
  has_many :quotes, inverse_of: :service_mode
  has_many :multi_services, inverse_of: :service_mode
  has_many :suppliers, through: :multi_services, inverse_of: :service_modes

  enum variable: %i[aerial land maritime customs]
end
