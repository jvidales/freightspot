# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    access user
    @power.each do |k, v|
      can v, k
    end
  end

  def access(user)
    @power = {}
    user.role.access_roles.each do |ac|
      @power[Object.const_get(system_module_name(ac.system_module_id))] = Access.find(AccessRole.where(role_id: user.role.id, system_module_id: ac.system_module_id).pluck(:access_id)).pluck(:name).flatten.collect { |r| r.to_sym }
    end
  end

  def system_module_name(id)
    SystemModule.find(id).name
  end
end
