# == Schema Information
#
# Table name: addresses
#
#  id               :bigint           not null, primary key
#  addressable_type :string
#  city             :string
#  colony           :string
#  country          :string
#  deleted_at       :datetime
#  interior_number  :string
#  name             :string
#  outdoor_number   :string
#  principal        :boolean          default(FALSE)
#  slug             :string
#  street           :string
#  zip_code         :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  addressable_id   :bigint
#  state_id         :bigint
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#  index_addresses_on_deleted_at                           (deleted_at)
#  index_addresses_on_slug                                 (slug)
#  index_addresses_on_state_id                             (state_id)
#
# Foreign Keys
#
#  fk_rails_...  (state_id => states.id)
#
class Address < ApplicationRecord
  acts_as_paranoid
  audited
  paginates_per 6

  belongs_to :addresable, polymorphic: true, optional: true
  belongs_to :state, inverse_of: :addresses, optional: true


  has_many :address_address_types, inverse_of: :address, dependent: :destroy
  has_many :address_types, through: :address_address_types, inverse_of: :addresses

  validates :name, presence: true
  validates :street, presence: true
  validates :interior_number, presence: true
  validates :country, presence: true

  def street_with_number
    "#{street} ##{interior_number}"
  end
end
