# == Schema Information
#
# Table name: senders
#
#  id              :bigint           not null, primary key
#  address         :string
#  contact         :string
#  deleted_at      :datetime
#  email           :string
#  name            :string
#  phone           :string
#  rfc             :string
#  senderable_type :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  senderable_id   :bigint
#
# Indexes
#
#  index_senders_on_deleted_at                         (deleted_at)
#  index_senders_on_senderable_type_and_senderable_id  (senderable_type,senderable_id)
#
class Sender < ApplicationRecord
  include Phoneable
  audited

  has_one_attached :logo

  belongs_to :senderable, polymorphic: true

  has_many :sender_sender_types, inverse_of: :sender, dependent: :destroy
  has_many :sender_types, through: :sender_sender_types, inverse_of: :senders

  validates :name, presence: true

  def principal_phone?
    principal_phone = false
    phones.each do |phone|
      principal_phone = true if phone.principal == true
    end
    principal_phone
  end

  # function that obtains the data from the contact's main telephone number
  def principal_phone
    principal_phone = Phone.new
    phones.each do |phone|
      principal_phone = phone if phone.principal == true
    end
    principal_phone
  end

  # function that gets the main phone number of the principal contact.
  def phone
    principal_phone.number.blank? ? '' : principal_phone.number.to_s.strip
  end

  # function that obtains the number with extension of the main telephone of the principal contact.
  def phone_with_extension
    extension + phone
  end

  def extension
    principal_phone.extension.blank? ? '' : principal_phone.extension
  end

  # function that obtains the country code number of the main telephone of the principal contact.
  def phone_with_country_code
    country_code + phone
  end

  def country_code
    principal_phone.code.blank? ? '' : "#{principal_phone.code} "
  end
end
