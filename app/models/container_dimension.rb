# == Schema Information
#
# Table name: container_dimensions
#
#  id                      :bigint           not null, primary key
#  deleted_at              :datetime
#  imperial_value          :decimal(, )
#  metric_value            :decimal(, )
#  name                    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  container_id            :bigint
#  imperial_measurement_id :bigint
#  metric_measurement_id   :bigint
#
# Indexes
#
#  index_container_dimensions_on_container_id             (container_id)
#  index_container_dimensions_on_deleted_at               (deleted_at)
#  index_container_dimensions_on_imperial_measurement_id  (imperial_measurement_id)
#  index_container_dimensions_on_metric_measurement_id    (metric_measurement_id)
#
# Foreign Keys
#
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (imperial_measurement_id => units.id)
#  fk_rails_...  (metric_measurement_id => units.id)
#
class ContainerDimension < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :container, inverse_of: :container_dimensions
  belongs_to :imperial_measurement, class_name: 'Unit', inverse_of: :container_dimensions, optional: true
  belongs_to :metric_measurement, class_name: 'Unit', inverse_of: :container_dimensions, optional: true
end
