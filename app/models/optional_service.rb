# == Schema Information
#
# Table name: optional_services
#
#  id         :bigint           not null, primary key
#  active     :boolean          default(TRUE)
#  category   :string
#  cost       :decimal(, )
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_optional_services_on_deleted_at  (deleted_at)
#
class OptionalService < ApplicationRecord
  has_many :quote_optional_services, inverse_of: :optional_service, :dependent => :destroy
  has_many :quotes, :through => :quote_optional_services

  def name_label
    I18n.translate "views.customer.quotes.optional_services.#{self.name}"
  end
end
