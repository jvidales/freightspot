# == Schema Information
#
# Table name: tariffs
#
#  id              :bigint           not null, primary key
#  cargo_type      :bigint
#  deleted_at      :datetime
#  ended_at        :datetime
#  service_mode    :bigint
#  started_at      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  carrier_id      :bigint
#  freight_type_id :bigint
#
# Indexes
#
#  index_tariffs_on_carrier_id       (carrier_id)
#  index_tariffs_on_deleted_at       (deleted_at)
#  index_tariffs_on_freight_type_id  (freight_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (carrier_id => suppliers.id)
#  fk_rails_...  (freight_type_id => mode_transportations.id)
#

class Tariff < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :carrier, class_name: 'Supplier', inverse_of: :tariffs
  belongs_to :freight_type, class_name: 'ModeTransportation', inverse_of: :tariffs, optional: true

  has_many :rates, inverse_of: :tariff, dependent: :destroy
  has_many :quotes, inverse_of: :tariff

  accepts_nested_attributes_for :rates, reject_if: :all_blank, allow_destroy: true

  enum service_mode: %i[aerial land maritime]

  validates :service_mode, presence: true
end
