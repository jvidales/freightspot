# == Schema Information
#
# Table name: charges
#
#  id                 :bigint           not null, primary key
#  apply_by           :integer
#  apply_to_able_type :string
#  apply_type         :string
#  chargeable_type    :string
#  deleted_at         :datetime
#  description        :text
#  number_pieces      :integer          default(1)
#  price              :decimal(20, 4)   default(0.0)
#  quantity           :decimal(20, 4)   default(0.0)
#  total              :decimal(20, 4)   default(0.0)
#  total_volume       :decimal(20, 4)   default(0.0)
#  total_weight       :decimal(20, 4)   default(0.0)
#  type_charge        :bigint
#  unit               :string
#  weight_to_charge   :decimal(20, 4)   default(0.0)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  apply_to_able_id   :bigint
#  chargeable_id      :bigint
#  currency_id        :bigint
#  service_id         :bigint
#  tax_id             :bigint
#
# Indexes
#
#  index_charges_on_chargeable_type_and_chargeable_id  (chargeable_type,chargeable_id)
#  index_charges_on_currency_id                        (currency_id)
#  index_charges_on_deleted_at                         (deleted_at)
#  index_charges_on_service_id                         (service_id)
#  index_charges_on_tax_id                             (tax_id)
#
# Foreign Keys
#
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (service_id => services.id)
#  fk_rails_...  (tax_id => taxes.id)
#
class Charge < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :chargeable, polymorphic: true, optional: true
  belongs_to :currency, inverse_of: :charges
  belongs_to :service, inverse_of: :charges
  belongs_to :tax, inverse_of: :charges, optional: true

  enum apply_to_able_type: %i[shipment quote]
  enum type_charge: %i[income expense]
  enum apply_by: %i[container piece weight volume]

  def apply_to
    apply_to = ''
    if type_charge == 'income'
      client = Client.find(apply_to_able_id)
      apply_to = client.name
    else
      supplier = Supplier.find(apply_to_able_id)
      apply_to = supplier.name
    end
    apply_to
  end
end
