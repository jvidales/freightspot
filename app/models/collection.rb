class Collection < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :collectionable, polymorphic: true
  belongs_to :country, inverse_of: :collections

  has_many :shipment_routes, inverse_of: :collection
end
