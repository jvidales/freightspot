# == Schema Information
#
# Table name: units
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  name        :string
#  unit_system :bigint
#  unit_type   :bigint
#  value       :integer          default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_units_on_deleted_at  (deleted_at)
#
class Unit < ApplicationRecord
  audited

  has_many :container_dimensions, inverse_of: :imperial_measurement, foreign_key: :imperial_measurement_id
  has_many :container_dimensions, inverse_of: :metric_measurement, foreign_key: :metric_measurement_id

  enum unit_type: %i[longitude weight volume]
  enum unit_system: %i[imperial metric]
end
