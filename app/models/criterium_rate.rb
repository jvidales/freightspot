# == Schema Information
#
# Table name: criterium_rates
#
#  id           :bigint           not null, primary key
#  deleted_at   :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  criterium_id :bigint
#  rate_id      :bigint
#
# Indexes
#
#  index_criterium_rates_on_criterium_id  (criterium_id)
#  index_criterium_rates_on_deleted_at    (deleted_at)
#  index_criterium_rates_on_rate_id       (rate_id)
#
# Foreign Keys
#
#  fk_rails_...  (criterium_id => criteria.id)
#  fk_rails_...  (rate_id => rates.id)
#
class CriteriumRate < ApplicationRecord
  belongs_to :criterium
  belongs_to :rate
end
