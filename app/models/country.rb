# == Schema Information
#
# Table name: countries
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  phonecode  :string
#  prohibited :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_countries_on_deleted_at  (deleted_at)
#
class Country < ApplicationRecord
  audited

  has_many :ports, inverse_of: :country
  has_many :states, inverse_of: :country

  def self.phonecodes
    Country.where('phonecode != ?', '')
  end
end
