# == Schema Information
#
# Table name: currencies
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_currencies_on_deleted_at  (deleted_at)
#
class Currency < ApplicationRecord
  audited

  has_many :agents, inverse_of: :currency
  has_many :charges, inverse_of: :currency
  has_many :clients, inverse_of: :currency
  has_many :criteria, inverse_of: :currency
  has_many :middlemen, inverse_of: :currency
  has_many :rate_surcharges, inverse_of: :currency
  has_many :services, inverse_of: :currency
  has_many :suppliers, inverse_of: :currency
  has_many :surcharges, through: :rate_surcharges
end
