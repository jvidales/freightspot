# == Schema Information
#
# Table name: taxes
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  value      :decimal(10, 4)   default(0.0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_taxes_on_code        (code)
#  index_taxes_on_deleted_at  (deleted_at)
#  index_taxes_on_slug        (slug) UNIQUE
#
class Tax < ApplicationRecord
  extend FriendlyId
  acts_as_paranoid
  audited
  friendly_id :code, use: :slugged

  has_many :services, inverse_of: :tax
  has_many :charges, inverse_of: :tax

  validates :code, presence: true, uniqueness: { case_sensitive: false }
  validates :name, presence: true, uniqueness: { case_sensitive: false }
end
