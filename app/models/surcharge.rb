# == Schema Information
#
# Table name: surcharges
#
#  id          :bigint           not null, primary key
#  active      :boolean          default(TRUE)
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_surcharges_on_deleted_at  (deleted_at)
#  index_surcharges_on_slug        (slug)
#
class Surcharge < ApplicationRecord
  extend FriendlyId
  acts_as_paranoid
  audited

  friendly_id :code, use: :slugged

  has_many :rate_surcharges, inverse_of: :surcharge
  has_many :rates, through: :rate_surcharges
end
