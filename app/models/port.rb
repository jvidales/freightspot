# == Schema Information
#
# Table name: ports
#
#  id                    :bigint           not null, primary key
#  deleted_at            :datetime
#  iata_code             :string
#  inactive              :boolean          default(FALSE)
#  latitude              :decimal(, )
#  longitude             :decimal(, )
#  name                  :string
#  port_identifier       :string
#  slug                  :string
#  transportation_method :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  country_id            :bigint
#
# Indexes
#
#  index_ports_on_country_id  (country_id)
#  index_ports_on_deleted_at  (deleted_at)
#  index_ports_on_slug        (slug)
#
# Foreign Keys
#
#  fk_rails_...  (country_id => countries.id)
#
class Port < ApplicationRecord
  acts_as_paranoid
  audited
  extend FriendlyId

  belongs_to :country, inverse_of: :ports

  has_many :rates, inverse_of: :origin_port, foreign_key: 'origin_port_id'
  has_many :rates, inverse_of: :discharge_port, foreign_key: 'discharge_port_id'
  has_many :collection_routes, inverse_of: :origin_port, foreign_key: 'origin_port_id'
  has_many :delivery_routes, inverse_of: :destiny_port, foreign_key: 'destiny_port_id'
  has_many :origin_routes, foreign_key: :origin_port_id, inverse_of: :origin_port
  has_many :destiny_routes, foreign_key: :destiny_port_id, inverse_of: :destiny_port
  has_many :criteria, foreign_key: :origin_id, inverse_of: :origin
  has_many :criteria, foreign_key: :destiny_id, inverse_of: :destiny

  friendly_id :port_identifier, use: :slugged

  enum transportation_method: %i[aerial land maritime]

  def coordinates
    [longitude, latitude]
  end

  def to_feature
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: coordinates
      },
      properties: {
        id: id,
        port_identifier: port_identifier,
        name: name
      }
    }
  end
end
