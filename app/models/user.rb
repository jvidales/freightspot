# == Schema Information
#
# Table name: users
#
#  id                                           :bigint           not null, primary key
#  accept_terms_of_service_and_privacy_policies :boolean          default(FALSE)
#  accepted_privacy_at                          :datetime
#  accepted_terms_at                            :datetime
#  admin                                        :boolean          default(FALSE)
#  announcements_read_at                        :datetime
#  approved                                     :boolean          default(FALSE)
#  current_sign_in_at                           :datetime
#  current_sign_in_ip                           :inet
#  deleted_at                                   :datetime
#  email                                        :string           default(""), not null
#  encrypted_password                           :string           default(""), not null
#  invitation_accepted_at                       :datetime
#  invitation_created_at                        :datetime
#  invitation_limit                             :integer
#  invitation_sent_at                           :datetime
#  invitation_token                             :string
#  invitations_count                            :integer          default(0)
#  invited_by_type                              :string
#  last_seen_at                                 :datetime
#  last_sign_in_at                              :datetime
#  last_sign_in_ip                              :inet
#  remember_created_at                          :datetime
#  reset_password_sent_at                       :datetime
#  reset_password_token                         :string
#  sign_in_count                                :integer          default(0), not null
#  slug                                         :string
#  time_zone                                    :string           default("Mexico City")
#  username                                     :string
#  created_at                                   :datetime         not null
#  updated_at                                   :datetime         not null
#  invited_by_id                                :bigint
#  role_id                                      :bigint
#
# Indexes
#
#  index_users_on_deleted_at                         (deleted_at)
#  index_users_on_email                              (email)
#  index_users_on_invitation_token                   (invitation_token)
#  index_users_on_invitations_count                  (invitations_count)
#  index_users_on_invited_by_id                      (invited_by_id)
#  index_users_on_invited_by_type_and_invited_by_id  (invited_by_type,invited_by_id)
#  index_users_on_reset_password_token               (reset_password_token)
#  index_users_on_role_id                            (role_id)
#  index_users_on_slug                               (slug)
#  index_users_on_username                           (username)
#
# Foreign Keys
#
#  fk_rails_...  (role_id => roles.id)
#
class User < ApplicationRecord
  extend FriendlyId

  acts_as_paranoid
  audited
  friendly_id :username, use: :slugged
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  belongs_to :role, inverse_of: :users

  has_one :employee, inverse_of: :user, dependent: :destroy
  has_one :contact, inverse_of: :user, dependent: :destroy
  has_one_attached :avatar

  has_one :setting, inverse_of: :user

  accepts_nested_attributes_for :employee
  accepts_nested_attributes_for :contact

  validates :username, uniqueness: true
  validates :accept_terms_of_service_and_privacy_policies, presence: true

  scope :online, -> { where('last_seen_at > ?', 15.minutes.ago) }

  def email_required?
    true
  end

  def email_changed?
    false
  end

  def superadmin?
    rol
  end
end
