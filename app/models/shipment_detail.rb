# == Schema Info
#
# Table name: shipment_details
#
# id                    :bigint(8)        not null, primary key
# description           :string
# pieces                :integer
# quantity_container    :integer
# packaging_type_id     :bigint           FOREIGN KEY
# container_id          :bigint           FOREIGN KEY
# shipment_id           :bigint           FOREIGN KEY
# length                :numeric(10,4)    DEFAULT 0,
# height                :numeric(10,4)    DEFAULT 0,
# width                 :numeric(10,4)    DEFAULT 0,
# weight                :numeric(10,4)    DEFAULT 0,
# volume                :numeric(20,4)    DEFAULT 0,
# slug                  :string
# created_at            :timestamp
# updated_at            :timestamp
# deleted_at            :timestamp
#
class ShipmentDetail < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :shipment, required: false
  belongs_to :packaging_type, inverse_of: :shipment_details
  belongs_to :container, inverse_of: :shipment_details, optional: true

  validates :description, presence: true
  validates :length, presence: true
  validates :width, presence: true
  validates :height, presence: true

  def calculate_volume
    (self.volume = self.length * self.width * self .height) * self.pieces
  end

  def lengthMToCm
    self.length * 100
  end

  def widthMToCm
    self.width * 100
  end

  def heightMToCm
    self.height * 100
  end
end
