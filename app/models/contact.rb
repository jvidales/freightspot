# == Schema Information
#
# Table name: contacts
#
#  id               :bigint           not null, primary key
#  area             :string
#  birth_date       :datetime
#  contactable_type :string
#  deleted_at       :datetime
#  email            :string
#  first_name       :string
#  job              :string
#  last_name        :string
#  principal        :boolean          default(FALSE)
#  slug             :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  contactable_id   :bigint
#  user_id          :bigint
#
# Indexes
#
#  index_contacts_on_contactable_type_and_contactable_id  (contactable_type,contactable_id)
#  index_contacts_on_deleted_at                           (deleted_at)
#  index_contacts_on_slug                                 (slug)
#  index_contacts_on_user_id                              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Contact < ApplicationRecord
  include Addressable
  include Phoneable
  extend FriendlyId

  acts_as_paranoid
  audited
  friendly_id :name, use: :slugged

  paginates_per 6

  has_one_attached :photo

  belongs_to :user, inverse_of: :contact, required: false
  belongs_to :contactable, polymorphic: true, optional: true

  has_person_name

  validates :name, presence: true
  validates :email, presence: true

  def principal_address?
    principal_address = false
    addresses.each do |address|
      principal_address = true if address.principal == true
    end
    principal_address
  end

  def principal_phone?
    principal_phone = false
    phones.each do |phone|
      principal_phone = true if phone.principal == true
    end
    principal_phone
  end

  # function that obtains the data from the main address of the contact
  def principal_address
    principal_address = Address.new
    addresses.each do |address|
      principal_address = address if address.principal == true
    end
    principal_address
  end

  # function that obtains the data from the contact's main telephone number
  def principal_phone
    principal_phone = Phone.new
    phones.each do |phone|
      principal_phone = phone if phone.principal == true
    end
    principal_phone
  end

  # function that gets the complete information of the main address of the principal contact.
  def full_address
    street + number_street + colony + zip_code_with_cp + state + country
  end

  def state
    principal_address.state.blank? ? '' : "#{principal_address.state.name}, "
  end

  def country
    principal_address.country.blank? ? '' : "#{principal_address.country}"
  end

  # function that obtains the postal code of the main full address of the principal contact.
  def zip_code_with_cp
    principal_address.zip_code.blank? ? '' : "CP #{principal_address.zip_code}, "
  end

  # function that gets the street from the main address of the principal contact.
  def street
    principal_address.street.blank? ? '' : "#{principal_address.street} "
  end

  # function that gets the street number from the main address of the principal contact.
  def number_street
    principal_address.interior_number.blank? ? '' : "N° #{principal_address.interior_number}, "
  end

  # function that gets the colony from the main address of the principal contact.
  def colony
    principal_address.colony.blank? ? '' : "Col. #{principal_address.colony}, "
  end

  # function that gets the main phone number of the principal contact.
  def phone
    principal_phone.number.blank? ? principal_phone.mobile_number.to_s.strip : principal_phone.number.to_s.strip
  end

  # function that obtains the number with extension of the main telephone of the principal contact.
  def phone_with_extension
    extension + phone
  end

  def extension
    principal_phone.extension.blank? ? '' : principal_phone.extension
  end

  # function that obtains the country code number of the main telephone of the principal contact.
  def phone_with_country_code
    phone
  end

  def country_code
    principal_phone.code.blank? ? '' : "#{principal_phone.code} "
  end
end
