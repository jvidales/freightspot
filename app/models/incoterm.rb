# == Schema Information
#
# Table name: incoterms
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :string
#  inactive    :boolean          default(FALSE)
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_incoterms_on_deleted_at  (deleted_at)
#  index_incoterms_on_slug        (slug)
#
class Incoterm < ApplicationRecord
  extend FriendlyId

  acts_as_paranoid
  audited

  friendly_id :code, use: :slugged

  has_many :shipments, inverse_of: :incoterm
  has_many :quotes, inverse_of: :incoterm
end
