# == Schema Information
#
# Table name: collection_routes
#
#  id             :bigint           not null, primary key
#  deleted_at     :datetime
#  latitude       :decimal(, )
#  longitude      :decimal(, )
#  origin         :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  origin_port_id :bigint
#  shipment_id    :bigint
#
# Indexes
#
#  index_collection_routes_on_deleted_at      (deleted_at)
#  index_collection_routes_on_origin_port_id  (origin_port_id)
#  index_collection_routes_on_shipment_id     (shipment_id)
#
# Foreign Keys
#
#  fk_rails_...  (origin_port_id => ports.id)
#  fk_rails_...  (shipment_id => shipments.id)
#
class CollectionRoute < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :shipment, inverse_of: :collection_route
  belongs_to :origin_port, class_name: 'Port', inverse_of: :collection_routes
end
