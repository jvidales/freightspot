# == Schema Information
#
# Table name: clients
#
#  id                                     :bigint           not null, primary key
#  advanced_parameters                    :boolean
#  annual_international_freight_shipments :bigint
#  authorized_credit                      :boolean          default(FALSE)
#  credit_limit                           :decimal(10, 4)   default(0.0)
#  deleted_at                             :datetime
#  has_credit                             :boolean          default(FALSE)
#  inactive                               :boolean          default(FALSE)
#  name                                   :string
#  rfc                                    :string
#  slug                                   :string
#  status                                 :bigint           default("pending")
#  website                                :string
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  cfdi_id                                :bigint
#  currency_id                            :bigint
#  middleman_id                           :bigint
#  parent_entity_id                       :bigint
#  payment_method_id                      :bigint
#  payment_term_id                        :bigint
#  way_pay_id                             :bigint
#
# Indexes
#
#  index_clients_on_cfdi_id            (cfdi_id)
#  index_clients_on_currency_id        (currency_id)
#  index_clients_on_deleted_at         (deleted_at)
#  index_clients_on_middleman_id       (middleman_id)
#  index_clients_on_parent_entity_id   (parent_entity_id)
#  index_clients_on_payment_method_id  (payment_method_id)
#  index_clients_on_payment_term_id    (payment_term_id)
#  index_clients_on_slug               (slug)
#  index_clients_on_way_pay_id         (way_pay_id)
#
# Foreign Keys
#
#  fk_rails_...  (cfdi_id => cfdis.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (middleman_id => middlemen.id)
#  fk_rails_...  (parent_entity_id => clients.id)
#  fk_rails_...  (payment_method_id => payment_methods.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (way_pay_id => way_pays.id)
#
class Client < ApplicationRecord
  include Addressable
  include Contactable
  include Noteable
  include Phoneable
  include Senderable
  include InformationDetail
  include Filterable
  extend FriendlyId

  acts_as_paranoid
  audited

  friendly_id :name, use: :slugged

  has_one :bank_account, inverse_of: :client
  has_one_attached :contract
  has_one_attached :logo

  has_many :client_products, inverse_of: :client
  has_many :client_documents, inverse_of: :client
  has_many :products, through: :client_products, inverse_of: :clients
  has_many :quotes, inverse_of: :client
  has_many :shipments, inverse_of: :client
  has_many :shipment_entities, inverse_of: :client

  belongs_to :cfdi, inverse_of: :clients, optional: true
  belongs_to :currency, inverse_of: :clients
  belongs_to :middleman, inverse_of: :clients, optional: true
  belongs_to :payment_method, inverse_of: :clients, optional: true
  belongs_to :payment_term, inverse_of: :clients, optional: true
  belongs_to :parent_entity, class_name: 'Client', inverse_of: :parent_entity, optional: true
  belongs_to :way_pay, inverse_of: :clients, optional: true

  validates :name, presence: true
  # validates :middleman, presence: true, if: :advanced_parameters?
  # validates :parent_entity, presence: true, if: :advanced_parameters?

  enum status: %i[pending authorized denied]
  enum annual_international_freight_shipments: %i[500 100 25 none_first none_ship]


  def advanced_parameters?
    advanced_parameters == true
  end

  def self.get_columns_names(incluir_columnas)
    columnas = Client.column_names
    excluir_columnas = columnas - incluir_columnas
    columnas - excluir_columnas
  end

  def self.get_all_columns_names
    excluir_columnas = %W[id parent_entity_id advanced_parameters slug created_at updated_at deleted_at]
    columnas = Client.column_names
    columnas - excluir_columnas
  end
end
