class Delivery < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :deliverable, polymorphic: true
  belongs_to :country

  has_many :shipment_routes, inverse_of: :delivery
end
