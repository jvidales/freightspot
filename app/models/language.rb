# == Schema Information
#
# Table name: languages
#
#  id         :bigint           not null, primary key
#  code       :string
#  default    :boolean          default(FALSE)
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_languages_on_deleted_at  (deleted_at)
#
class Language < ApplicationRecord
  audited

  has_many :settings, inverse_of: :language
end
