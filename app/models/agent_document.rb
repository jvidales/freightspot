# == Schema Information
#
# Table name: agent_documents
#
#  id          :bigint           not null, primary key
#  deleted_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  agent_id    :bigint
#  document_id :bigint
#
# Indexes
#
#  index_agent_documents_on_agent_id     (agent_id)
#  index_agent_documents_on_deleted_at   (deleted_at)
#  index_agent_documents_on_document_id  (document_id)
#
# Foreign Keys
#
#  fk_rails_...  (agent_id => agents.id)
#  fk_rails_...  (document_id => documents.id)
#
class AgentDocument < ApplicationRecord
  belongs_to :agent
  belongs_to :document
end
