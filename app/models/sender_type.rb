# == Schema Information
#
# Table name: sender_types
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  inactive   :boolean          default(FALSE)
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_sender_types_on_deleted_at  (deleted_at)
#
class SenderType < ApplicationRecord
  audited

  has_many :sender_sender_types, inverse_of: :sender_type
  has_many :senders, through: :sender_sender_types, inverse_of: :sender_types
end
