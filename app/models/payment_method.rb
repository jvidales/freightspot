# == Schema Information
#
# Table name: payment_methods
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_payment_methods_on_deleted_at  (deleted_at)
#
class PaymentMethod < ApplicationRecord
  audited

  has_many :agents, inverse_of: :payment_method
  has_many :clients, inverse_of: :payment_method
  has_many :middlemen, inverse_of: :payment_method
  has_many :suppliers, inverse_of: :payment_method
end
