# == Schema Information
#
# Table name: payment_terms
#
#  id              :bigint           not null, primary key
#  amount_days_pay :integer
#  deleted_at      :datetime
#  description     :string
#  inactive        :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_payment_terms_on_deleted_at  (deleted_at)
#
class PaymentTerm < ApplicationRecord
  audited

  has_many :agents, inverse_of: :payment_term
  has_many :clients, inverse_of: :payment_term
  has_many :suppliers, inverse_of: :payment_term
  has_many :quotes, inverse_of: :payment_term
  has_many :middlemen, inverse_of: :payment_term  
end
