# == Schema Information
#
# Table name: cfdis
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  moral       :boolean          default(FALSE)
#  name        :string
#  physical    :boolean          default(FALSE)
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_cfdis_on_deleted_at  (deleted_at)
#
class Cfdi < ApplicationRecord
  audited

  has_many :agents, inverse_of: :cfdi
  has_many :clients, inverse_of: :cfdi
  has_many :middlemen, inverse_of: :cfdi
  has_many :suppliers, inverse_of: :cfdi
end
