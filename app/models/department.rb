# == Schema Information
#
# Table name: departments
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  branch_id  :bigint
#
# Indexes
#
#  index_departments_on_branch_id   (branch_id)
#  index_departments_on_deleted_at  (deleted_at)
#
# Foreign Keys
#
#  fk_rails_...  (branch_id => branches.id)
#
class Department < ApplicationRecord
  audited

  belongs_to :branch, inverse_of: :departments
  has_many :teams, inverse_of: :department
end
