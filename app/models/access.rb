# == Schema Information
#
# Table name: accesses
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_accesses_on_deleted_at  (deleted_at)
#
class Access < ApplicationRecord
  extend FriendlyId

  acts_as_paranoid
  audited

  friendly_id :name, use: :slugged

  has_many :access_roles, inverse_of: :access
  has_many :roles, through: :access_roles, inverse_of: :accesses
end
