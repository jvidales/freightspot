# == Schema Information
#
# Table name: organizations
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_organizations_on_deleted_at  (deleted_at)
#  index_organizations_on_slug        (slug) UNIQUE
#
class Organization < ApplicationRecord
  audited

  has_many :branches, inverse_of: :organization
  has_many :quotes, inverse_of: :organization
  has_many :shipments, inverse_of: :organization
end
