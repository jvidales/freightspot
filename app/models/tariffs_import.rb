# Import Class
class TariffsImport
  include ActiveModel::Model
  require 'roo'

  attr_accessor :file

  def initialize(attributes={})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when '.csv' then Csv.new(file.path, nil, :ignore)
    when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
    when '.xlsx' then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def load_imported_items
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).map do |i|
      row = [header, spreadsheet.row(i)].transpose.to_h
      carrier = carrier_data(row['carrier'])
      started_at = row['started_at']
      ended_at = row['ended_at']
      service_mode = row['service_mode']
      tariff = Tariff.find_or_create_by(carrier_id: carrier.id, started_at: started_at, ended_at: ended_at,
                                        service_mode: service_mode)
      tariff.rates << rate_create(row)
      tariff.save
      tariff
    end
  end

  def imported_items
    @imported_items ||= load_imported_items
  end

  def save
    if imported_items.map(&:valid?).all?
      true
    else
      imported_items.each_with_index do |item, index|
        item.errors.full_messages.each do |msg|
          errors.add :base, "Row #{index + 6}: #{msg}"
        end
      end
      false
    end
  end

  def rate_create(row)
    rate = Rate.new
    rate.origin_port = port_data(row['pol_code'])
    rate.discharge_port = port_data(row['pod_code'])
    rate.freight_rate = row['freight_rate']
    rate.minimal_cost = row['minimal_cost']
    rate.transite_time = row['transite_time']
    rate.frequency = row['frequency']
    rate.via_code = row['via_code']
    rate.via = row['via']
    rate.free_days = row['free_days']
    rate.guarantee_letter = row['guarantee_letter']
    rate
  end

  def carrier_data(name)
    Supplier.find_by_name(name)
  end

  def port_data(code)
    Port.find_by_port_identifier(code)
  end
end
