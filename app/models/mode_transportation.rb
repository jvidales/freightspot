# == Schema Information
#
# Table name: mode_transportations
#
#  id              :bigint           not null, primary key
#  code            :string
#  deleted_at      :datetime
#  name            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint
#
# Indexes
#
#  index_mode_transportations_on_deleted_at       (deleted_at)
#  index_mode_transportations_on_service_mode_id  (service_mode_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#
class ModeTransportation < ApplicationRecord
  audited

  belongs_to :service_mode, required: false

  has_many :quotes, foreign_key: 'freight_type_id', inverse_of: :freight_type
  has_many :shipments, inverse_of: :mode_transportation
  has_many :tariffs, foreign_key: 'freight_type_id', inverse_of: :freight_type
end
