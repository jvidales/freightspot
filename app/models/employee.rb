# == Schema Information
#
# Table name: employees
#
#  id              :bigint           not null, primary key
#  birth_date      :datetime
#  city            :string
#  deleted_at      :datetime
#  first_name      :string
#  job_title       :string
#  join_date       :datetime
#  language        :string
#  last_name       :string
#  mobile          :string
#  office_branch   :string
#  origin          :string
#  phone           :string
#  phone_extension :string
#  squad           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :bigint
#
# Indexes
#
#  index_employees_on_deleted_at  (deleted_at)
#  index_employees_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Employee < ApplicationRecord
  acts_as_paranoid
  audited
  has_person_name

  belongs_to :user, -> { with_deleted }, inverse_of: :employee

  has_many :quotes, inverse_of: :salesman, foreign_key: 'salesman_id'
  has_many :quotes, inverse_of: :employee, foreign_key: 'employee_id'
  has_many :shipments, inverse_of: :salesman, foreign_key: 'employee_id'
  has_many :shipments, inverse_of: :employee
end
