# == Schema Information
#
# Table name: way_pays
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_way_pays_on_deleted_at  (deleted_at)
#
class WayPay < ApplicationRecord
  audited

  has_many :agents, inverse_of: :way_pay
  has_many :clients, inverse_of: :way_pay
  has_many :middlemen, inverse_of: :way_pay
  has_many :suppliers, inverse_of: :way_pay
end
