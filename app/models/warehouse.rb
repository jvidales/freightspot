# == Schema Information
#
# Table name: warehouses
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_warehouses_on_deleted_at  (deleted_at)
#
class Warehouse < ApplicationRecord
  acts_as_paranoid
  audited
end
