# == Schema Information
#
# Table name: shipments
#
#  id                      :bigint           not null, primary key
#  arrivaled_at            :datetime
#  bar_code                :string
#  chargeable_weight       :decimal(20, 4)   default(0.0)
#  danger_item             :boolean          default(FALSE)
#  declared_value          :decimal(10, 4)   default(0.0)
#  deleted_at              :datetime
#  departured_at           :datetime
#  description             :text
#  estimated_arrivaled_at  :datetime
#  estimated_departured_at :datetime
#  free_day_of_delay       :integer
#  frequency               :bigint
#  guarantee_letter        :boolean          default(FALSE)
#  guide_number            :string
#  number                  :string
#  operation_type          :integer
#  qr_code                 :string
#  realized_at             :datetime
#  reservation_number      :string
#  service_mode            :bigint
#  slug                    :string
#  status                  :bigint           default("waiting_collection")
#  total_pieces            :decimal(10, 4)   default(0.0)
#  total_volume            :decimal(20, 4)   default(0.0)
#  total_weight            :decimal(10, 4)   default(0.0)
#  type_service            :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  client_id               :bigint
#  employee_id             :bigint
#  freight_type_id         :bigint
#  incoterm_id             :bigint
#  quote_id                :bigint
#  salesman_id             :bigint
#
# Indexes
#
#  index_shipments_on_client_id        (client_id)
#  index_shipments_on_deleted_at       (deleted_at)
#  index_shipments_on_employee_id      (employee_id)
#  index_shipments_on_freight_type_id  (freight_type_id)
#  index_shipments_on_incoterm_id      (incoterm_id)
#  index_shipments_on_quote_id         (quote_id)
#  index_shipments_on_salesman_id      (salesman_id)
#  index_shipments_on_slug             (slug)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (freight_type_id => mode_transportations.id)
#  fk_rails_...  (incoterm_id => incoterms.id)
#  fk_rails_...  (quote_id => quotes.id)
#  fk_rails_...  (salesman_id => employees.id)
#
class Shipment < ApplicationRecord
  include Filterable
  extend FriendlyId

  acts_as_paranoid
  audited
  friendly_id :number, use: :slugged

  paginates_per 4

  belongs_to :client, inverse_of: :shipments, optional: true
  belongs_to :employee, inverse_of: :shipments, optional: true
  belongs_to :freight_type, class_name: 'ModeTransportation', inverse_of: :shipments, optional: true
  belongs_to :incoterm, inverse_of: :shipments, optional: true
  belongs_to :quote, inverse_of: :shipment
  belongs_to :salesman, class_name: 'Employee', inverse_of: :shipments, optional: true

  has_one :delivery_route, inverse_of: :shipment
  has_one :collection_route, inverse_of: :shipment

  has_many :shipment_units, inverse_of: :shipment

  enum frequency: %i[diary weekly biweekly monthly yearly]
  enum operation_type: %i[national export import]
  enum service_mode: %i[aerial land maritime]
  enum status: %i[waiting_collection in_transit delivery_route delivered]
  enum type_service: %i[door_door door_port port_port port_door]

  accepts_nested_attributes_for :shipment_units
  accepts_nested_attributes_for :collection_route
  accepts_nested_attributes_for :delivery_route

  scope :filter_by_service_mode, lambda { |servide_mode|
    where('shipments.service_mode = ?', servide_mode)
  }

  def full_status1?
    completed = false
    completed = true if read_attribute_before_type_cast(:status).positive?
    completed
  end

  def full_status2?
    completed = false
    completed = true if read_attribute_before_type_cast(:status) > 1
    completed
  end

  def full_status3?
    completed = false
    completed = true if read_attribute_before_type_cast(:status) > 2
    completed
  end

  def full_status4?
    completed = false
    completed = true if read_attribute_before_type_cast(:status) > 3
    completed
  end
end
