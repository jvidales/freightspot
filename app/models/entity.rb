# == Schema Information
#
# Table name: entities
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  locale     :string
#  name       :string
#  sender     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_entities_on_deleted_at  (deleted_at)
#
class Entity < ApplicationRecord
  audited

  has_many :shipment_entities
  has_one :shipment
end
