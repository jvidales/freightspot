# == Schema Information
#
# Table name: client_products
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  tariff_fraction :decimal(, )
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  client_id       :bigint
#  product_id      :bigint
#
# Indexes
#
#  index_client_products_on_client_id   (client_id)
#  index_client_products_on_deleted_at  (deleted_at)
#  index_client_products_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (product_id => products.id)
#
class ClientProduct < ApplicationRecord
  acts_as_paranoid
  acts_as_taggable_on :tags
  audited
  paginates_per 6

  has_one_attached :image

  belongs_to :client, inverse_of: :client_products
  belongs_to :product, inverse_of: :client_products
end
