# customer detail
module InformationDetail
  # function that obtains the data from the main address of the contact
  def main_address
    principal_address = Address.new
    addresses.each do |address|
      principal_address = address if address.principal == true
    end
    principal_address
  end

  # function that obtains the data from the contact's main telephone number
  def main_phone
    principal_phone = Phone.new
    phones.each do |phone|
      principal_phone = phone if phone.principal == true
    end
    principal_phone
  end

  # Function that obtains the data from the main contact
  def main_contact
    principal_contact = Contact.new
    contacts.each do |contact|
      principal_contact = contact if contact.principal == true
    end
    principal_contact
  end

  # function that gets the email from the main contact
  def contact_email
    main_contact.email
  end

  # function that gets the full name of the primary contact
  def contact_full_name
    main_contact.name
  end

  # function that gets the type of phone
  def type_phone
    main_phone.type_phone
  end

  # function that obtains the number with extension of the main telephone of the principal contact.
  def phone_with_extension
    "#{phone} Ext. #{extension}"
  end

  def extension
    main_phone.extension.blank? ? '' : main_phone.extension
  end

  # function that obtains the country code number of the main telephone of the principal contact.
  def phone_with_country_code
    phone
  end

  def country_code
    main_phone.code.blank? ? '' : "#{main_phone.code} "
  end

  # function that gets the main phone number of the principal contact.
  def phone
    main_phone.number
  end

  # function that gets the complete information of the main address of the principal contact.
  def full_address
    street + number_street + colony + zip_code_with_cp
  end

  # function that obtains the postal code of the main full address of the principal contact.
  def zip_code_with_cp
    main_address.zip_code.blank? ? '' : "CP #{main_address.zip_code}"
  end

  # function that obtains the postal code of the main address of the principal contact.
  def zip_code
    main_address.zip_code.blank? ? '' : main_address.zip_code
  end

  # function that gets the street from the main address of the principal contact.
  def street
    main_address.street.blank? ? '' : "C. #{main_address.street} "
  end

  # function that gets the street number from the main address of the principal contact.
  def number_street
    main_address.interior_number.blank? ? '' : "N° #{main_address.interior_number}, "
  end

  # function that gets the colony from the main address of the principal contact.
  def colony
    main_address.colony.blank? ? '' : "Col. #{main_address.colony} "
  end

  # function that obtains the city of the main address of the principal contact.
  def city
    main_address.city
  end
end
