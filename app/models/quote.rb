# == Schema Information
#
# Table name: quotes
#
#  id                  :bigint           not null, primary key
#  authorized          :boolean          default(FALSE)
#  danger_item         :boolean          default(FALSE)
#  declared_value      :decimal(10, 4)   default(0.0)
#  deleted_at          :datetime
#  description         :text
#  expired_at          :datetime
#  free_day_of_delay   :integer
#  frequency           :bigint
#  guarantee_letter    :boolean          default(FALSE)
#  is_active_shipment  :boolean          default(FALSE)
#  number              :string
#  operation_type      :integer
#  quoted_at           :datetime
#  service_mode        :bigint
#  service_type        :string
#  slug                :string
#  status              :bigint           default("open")
#  total_pieces        :decimal(10, 4)   default(0.0)
#  total_volume        :decimal(20, 4)   default(0.0)
#  total_weight        :decimal(10, 4)   default(0.0)
#  total_weight_volume :decimal(20, 4)   default(0.0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  client_id           :bigint
#  employee_id         :bigint
#  freight_type_id     :bigint
#  incoterm_id         :bigint
#  organization_id     :bigint
#  payment_term_id     :bigint
#  rate_id             :bigint
#  salesman_id         :bigint
#
# Indexes
#
#  index_quotes_on_client_id        (client_id)
#  index_quotes_on_deleted_at       (deleted_at)
#  index_quotes_on_employee_id      (employee_id)
#  index_quotes_on_freight_type_id  (freight_type_id)
#  index_quotes_on_incoterm_id      (incoterm_id)
#  index_quotes_on_organization_id  (organization_id)
#  index_quotes_on_payment_term_id  (payment_term_id)
#  index_quotes_on_rate_id          (rate_id)
#  index_quotes_on_salesman_id      (salesman_id)
#  index_quotes_on_slug             (slug)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (freight_type_id => mode_transportations.id)
#  fk_rails_...  (incoterm_id => incoterms.id)
#  fk_rails_...  (organization_id => organizations.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (rate_id => rates.id)
#  fk_rails_...  (salesman_id => employees.id)
#
class Quote < ApplicationRecord
  include Chargeable
  include Filterable

  extend FriendlyId
  acts_as_paranoid
  audited
  friendly_id :number, use: :slugged

  paginates_per 4

  belongs_to :client, inverse_of: :quotes, optional: true
  belongs_to :employee, inverse_of: :quotes, optional: true
  belongs_to :freight_type, class_name: 'ModeTransportation', inverse_of: :quotes, optional: true
  belongs_to :incoterm, inverse_of: :quotes, optional: true
  belongs_to :organization, inverse_of: :quotes
  belongs_to :payment_term, inverse_of: :quotes, optional: true
  belongs_to :rate, inverse_of: :quotes, optional: true
  belongs_to :salesman, class_name: 'Employee', inverse_of: :quotes, optional: true

  has_many :quote_units, inverse_of: :quote, dependent: :destroy

  has_many :quote_optional_services, inverse_of: :quote, dependent: :destroy
  has_many :optional_services, through: :quote_optional_services

  has_one :shipment, inverse_of: :quote
  has_one :origin_route, inverse_of: :quote
  has_one :destiny_route, inverse_of: :quote

  enum frequency: %i[diary weekly biweekly monthly yearly]
  enum operation_type: %i[national export import]
  enum service_mode: %i[aerial land maritime]
  enum type_service: %i[door_door door_port port_port port_door]
  enum status: %i[open closed confirmed declined]

  accepts_nested_attributes_for :destiny_route
  accepts_nested_attributes_for :origin_route
  accepts_nested_attributes_for :quote_units
  accepts_nested_attributes_for :quote_optional_services

  after_save :generate_number

  scope :filter_by_service_mode, lambda { |servide_mode|
    where('quotes.service_mode = ?', servide_mode)
  }

  def generate_number
    update_column(:number, "FS-#{1000 + id}")
  end

  def total_costs_charge
    charges_cost = Charge.where('type_charge = ? AND chargeable_id = ?', 1, id)
    total_charges_cost = 0
    charges_cost.each do |cc|
      total_charges_cost += cc.total
    end
    total_charges_cost
  end

  def total_incomes_charge
    charges_cost = Charge.where('type_charge = ? AND chargeable_id = ?', 0, id)
    total_charges_cost = 0
    charges_cost.each do |cc|
      total_charges_cost += cc.total
    end
    total_charges_cost
  end

  def editable?
    is_active_shipment
  end
end
