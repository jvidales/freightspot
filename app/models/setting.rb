# == Schema Information
#
# Table name: settings
#
#  id          :bigint           not null, primary key
#  deleted_at  :datetime
#  time_zone   :string           default("Mexico City")
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  language_id :bigint
#  user_id     :bigint
#
# Indexes
#
#  index_settings_on_deleted_at   (deleted_at)
#  index_settings_on_language_id  (language_id)
#  index_settings_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (language_id => languages.id)
#  fk_rails_...  (user_id => users.id)
#
class Setting < ApplicationRecord
  belongs_to :user, inverse_of: :setting
  belongs_to :language, inverse_of: :settings
end
