# == Schema Information
#
# Table name: destiny_routes
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  destiny         :string
#  latitude        :decimal(, )
#  longitude       :decimal(, )
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  destiny_port_id :bigint
#  quote_id        :bigint
#
# Indexes
#
#  index_destiny_routes_on_deleted_at       (deleted_at)
#  index_destiny_routes_on_destiny_port_id  (destiny_port_id)
#  index_destiny_routes_on_quote_id         (quote_id)
#
# Foreign Keys
#
#  fk_rails_...  (destiny_port_id => ports.id)
#  fk_rails_...  (quote_id => quotes.id)
#
class DestinyRoute < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :quote, inverse_of: :destiny_route
  belongs_to :destiny_port, class_name: 'Port', inverse_of: :destiny_routes, optional: :true
end
