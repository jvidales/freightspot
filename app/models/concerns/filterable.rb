# Filtertable methods
module Filterable
  extend ActiveSupport::Concern

  # Class methods
  module ClassMethods
    def filter(filtering_params)
      results = where(nil)
      filtering_params.each do |key, value|
        results = results.public_send("filter_by_#{key}", value) if value.present?
      end
      results
    end

    def filters(params)
      conditions = {}
      conditions['shipments.incoterm_id'] = params['incoterm'] if params['incoterm'].present?
      conditions['shipments.service_mode'] = params['service_mode'] if params['service_mode'].present?
      conditions['shipments.freight_type_id'] = params['freight_type'] if params['freight_type'].present?
      conditions[:payment_term_id] = params['payment_term'] if params['payment_term'].present?
      conditions[:status] = params['status'] if params['status'].present?
      if params['annual_international_freight_shipments'].present?
        conditions[:annual_international_freight_shipments] = params['annual_international_freight_shipments']
      end

      order(:name)
        .includes(:payment_term, :shipments).references(:payment_term, :shipments)
        .where(conditions)
    end
  end
end
