module Deliverable
  extend ActiveSupport::Concern

  included do
    has_many :deliveries, as: :deliverable, dependent: :destroy
    accepts_nested_attributes_for :deliveries, allow_destroy: true
  end
end
