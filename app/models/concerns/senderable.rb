module Senderable
  extend ActiveSupport::Concern

  included do
    has_many :senders, as: :senderable, dependent: :destroy
    accepts_nested_attributes_for :senders, allow_destroy: true
  end
end
