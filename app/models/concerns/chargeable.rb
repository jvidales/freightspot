# Module Chargeable
module Chargeable
  extend ActiveSupport::Concern

  included do
    has_many :charges, as: :chargeable, dependent: :destroy
    accepts_nested_attributes_for :charges, allow_destroy: true
  end
end
