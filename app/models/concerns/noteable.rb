module Noteable
  extend ActiveSupport::Concern

  included do
    has_one :note
    accepts_nested_attributes_for :note, allow_destroy: true
  end
end