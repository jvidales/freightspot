module Collectionable
  extend ActiveSupport::Concern

  included do
    has_many :collections, as: :collectionable, dependent: :destroy
    accepts_nested_attributes_for :collections, allow_destroy: true
  end
end
