module Phoneable
  extend ActiveSupport::Concern

  included do
    has_many :phones, as: :phoneable, dependent: :destroy
    accepts_nested_attributes_for :phones, allow_destroy: true
  end
end
