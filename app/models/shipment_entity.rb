# == Schema Info
#
# Table name: shipment_entities
#
# id                    :bigint(8)        not null, primary key
# client_id             :bigint           FOREIGN KEY
# address               :string           DEFAULT ''
# shipment_id           :bigint           FOREIGN KEY
# entity_id             :bigint           FOREIGN KEY
# customer_to_collect   :boolean          DEFAULT false
# created_at            :timestamp
# updated_at            :timestamp
# deleted_at            :timestamp
#
class ShipmentEntity < ApplicationRecord
  acts_as_paranoid
  audited

  belongs_to :shipment
  belongs_to :client, inverse_of: :shipment_entities, optional: true
  belongs_to :entity
end
