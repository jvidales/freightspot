# FormConstrol
class CalendarPickerInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    wrapper_css_classes = %w[input-group date datetimepicker-group]
    wrapper_css_classes << wrapper_options[:error_class] if has_errors?

    template.content_tag(:div, class: wrapper_css_classes.join(' '), data: { target_input: 'nearest' }, id: "#{object_name}_#{attribute_name}_datetimepicker") do
      template.concat @builder.text_field(attribute_name, input_html_options)
      template.concat div_button
    end
  end

  def input_html_options
    super.tap do |options|
      custom_css_classes = %w[form-control datetimepicker-input]
      custom_css_classes << 'is-invalid' if has_errors?
      options[:class] = (options[:class] + custom_css_classes).uniq

      options[:data] ||= {}
      options[:data][:target] = "##{object_name}_#{attribute_name}_datetimepicker"
    end
  end

  private

  def div_button
    template.content_tag(:div, class: 'input-group-append', data: { target: "##{object_name}_#{attribute_name}_datetimepicker", toggle: 'datetimepicker' } ) do
      template.concat span_table
    end
  end

  def span_table
    template.content_tag(:div, class: 'input-group-text') do
      template.concat icon_table
    end
  end

  def icon_remove
    '<i class="fas fa-eraser"></i>'.html_safe
  end

  def icon_table
    '<i class="fas fa-calendar"></i>'.html_safe
  end
end
