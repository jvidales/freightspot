class ClientsDatatable

  delegate :params, :name,:website, :phone, :status, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Client.count,
      iTotalDisplayRecords: clients.length,
      aaData: data
    }
  end

  private

  def data

    clients.map do |client|
      [
        client.name,
        client.website,
        (client.type_phone == 'mobile' ? client.phone_with_country_code : client.phone_with_extension),
        client.status
      ]
    end

  end

  def clients
    @clients ||= fetch_clients
  end

  def fetch_clients
    clients = Client.all
    clients
  end

end