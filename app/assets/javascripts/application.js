// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require rails-ujs
//= require jquery-ui/widgets/autocomplete
//= require autocomplete-rails
//= require cocoon
//= require toastr
//= require login-3
//= require activestorage
//= require local-time
//= require plugins
//= require prismjs
//= require scripts
//= require toastr
//= require widgets
//= require wizard1
//= require wizard2
//= require wizard3
//= require bootstrap-select
//= require font-awesome-6.0
//= require datatables
//= require datatables.responsive
//= require moment
// If you require timezone data (see moment-timezone-rails for additional file options)
//= require moment-timezone-with-data
//= require tempusdominus-bootstrap-4
//= require selectize
//= require apexcharts
//= require intlTelInput
//= require libphonenumber/utils
//= require jQuery.tagify.min.js
//= require tagify.min.js
// require mapbox.js
// require mapbox-gl
// require mapbox-gl-geocoder
//= require turf.min.js

function show_content_modal(title, title_button, action) {
    if (action == 'create') {
        // $("#modal-dg").addClass("modal-lg" );
        $("#modal_title" ).html(title);
        $("#btn_submit_modal").show();
        $("#btn_close_modal").show();
        $("#btn_yes_modal").hide();
        $("#btn_no_modal").hide();
        $("#btn_submit_modal" ).html(title_button);
    } else if (action == 'update') {
        $("#modal_title" ).html(title);
        $("#btn_submit_modal").show()
        $("#btn_close_modal").show();
        $("#btn_yes_modal").hide();
        $("#btn_no_modal").hide();
        $("#btn_submit_modal" ).html(title_button);
    } else if (action == 'destroy') {
        $("#modal_title" ).html(title);
        $("#btn_submit_modal").hide();
        $("#btn_close_modal").hide();
        $("#btn_yes_modal").show();
        $("#btn_no_modal").show();
    } else {
        // $("#modal-dg").addClass("modal-lg" );
        $("#modal_title" ).html(title);
        $("#btn_close_modal").show();
        $("#btn_yes_modal").hide();
        $("#btn_no_modal").hide();
        $("#btn_submit_modal").hide();
        // $("#modal-dg").removeClass( "modal-lg" );
    }
}

$(document).ready(function(){
    $("#btn_submit_modal").click(function () {
        $('#btn-submit-form').trigger( "click" );
    });

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
});
