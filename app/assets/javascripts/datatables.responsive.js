"use strict";
/********** Datatable List Agents **********/
var KTDatatablesAgents = function() {
    const initTableAgents = function () {
        const table = $('#kt_datatable_agents');

        // begin first table
        table.DataTable({
            responsive: true,

            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableAgents();
        }
    };
}();

var KTDatatablesExtensionsResponsive = function() {
	var initTable1 = function() {
		var table = $('#kt_datatable');

		// begin first table
		table.DataTable({
			responsive: true,
			columnDefs: [
				{
					width: '150px',
					targets: 0
				},
				{
					width: '75px',
					targets: 14,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Online', 'state': 'danger'},
							2: {'title': 'Retail', 'state': 'primary'},
							3: {'title': 'Direct', 'state': 'success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
							'<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
					}
				}
			]
		});
	};

	return {
		//main function to initiate the module
		init: function() {
			initTable1();
		}
	};
}();

/********** Datatable List Addresses *********/
var KTDatatablesAddresses = function() {
    const initTableAddresses = function () {
        const table = $('#kt_datatable_addresses');

        // begin first table
        table.DataTable({
            responsive: true,

            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableAddresses();
        }
    };
}();

/********** Datatable List Addresses *********/
var KTDatatablesGeneral = function() {
    var initTableGeneral = function() {
        var tableGeneral = $('#kt_datatable_general');

        // begin first table
        tableGeneral.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableGeneral();
        }
    };
}();

/********** Datatable List Clients *********/
var KTDatatablesClients = function() {
    $.fn.dataTable.Api.register('column().title()', function() {
        return $(this.header()).text().trim();
    });

    var initTableClient = function() {
        var table = $('#kt_datatable_client').DataTable({
            responsive: true
        });
        refreshTable();
    };

    function refreshTable(){
        var table = $('#kt_datatable_client').DataTable();
        var checkboxlist =  document.getElementById('columns_list');
        var checkOptions =  checkboxlist.getElementsByTagName('input');
        for ( let j = 0; j < checkOptions.length; j++) {
            var column = table.column(j);
            if (checkOptions[j].checked) {
                column.visible(true);
            }else{
                column.visible(false);
            }
        }
    }

    return {
        //main function to initiate the module
        init: function() {
            initTableClient();
        },
        refresh: function (){
            refreshTable();
        }
    };
}();

/********** Datatable List Middlemen *********/
var KTDatatablesMiddlemen = function() {
    var initTableMiddleman = function() {
        var table = $('#kt_datatable_middleman');

        // begin first table
        table.DataTable({
            responsive: true,
            columnDefs: [
				{
					width: '150px',
					targets: 0
				},
				{
					width: '75px'
				}
			]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableMiddleman();
        }
    };
}();

/********** Datatable List Addresses *********/
var KTDatatablesAddresses = function() {
    const initTableAddresses = function () {
        const table = $('#kt_datatable_addresses');

        // begin first table
        table.DataTable({
            responsive: true,

            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableAddresses();
        }
    };
}();

/********** Datatable List Charges **********/
const KTDatatablesCharges = function () {
    const initTableCharges = function () {
        const table = $('#kt_datatable_charges');

        // begin first table
        table.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            initTableCharges();
        }
    };
}();

/********** Datatable List Phones **********/
const KTDatatablesPhones = function () {
    const initTablePhones = function () {
        const table = $('#kt_datatable_phones');

        // begin first table
        table.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            initTablePhones();
        }
    };
}();

/********** Datatable List Services **********/
var KTDatatablesServices = function() {
    var initTableServices = function() {
        var tableServices = $('#kt_datatable_services');

        // begin first table
        tableServices.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableServices();
        }
    };
}();

/********** Datatable List Rates **********/
var KTDatatablesRates = function() {
    var initTableRates = function() {
        var tableRates = $('#kt_datatable_rates');

        // begin first table
        tableRates.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableRates();
        }
    };
}();

/********** Datatable List Concepts **********/
var KTDatatablesConcepts = function() {
    var initTableConcepts = function() {
        var tableConcepts = $('#kt_datatable_concepts');

        // begin first table
        tableConcepts.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableConcepts();
        }
    };
}();

/********** Datatable List Ports **********/
var KTDatatablesPorts = function() {
    var initTablePorts = function() {
        var tablePorts = $('#kt_datatable_ports');

        // begin first table
        tablePorts.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTablePorts();
        }
    };
}();

/********** Datatable List Packaging Types **********/
var KTDatatablesPackagingTypes = function() {
    var initTablePackagingTypes = function() {
        var tablePackagingTypes = $('#kt_datatable_packaging_types');

        // begin first table
        tablePackagingTypes.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTablePackagingTypes();
        }
    };
}();

/********** Datatable List Users **********/
var KTDatatablesUsers = function() {
    var initTableUsers = function() {
        var tableUsers = $('#kt_datatable_users');

        // begin first table
        tableUsers.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableUsers();
        }
    };
}();

/********** Datatable List Suppliers **********/
var KTDatatablesSuppliers = function() {
    var initTableSuppliers = function() {
        var tableSuppliers = $('#kt_datatable_suppliers');

        // begin first table
        tableSuppliers.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableSuppliers();
        }
    };
}();

/********** Datatable List Shipments **********/
var KTDatatablesShipments = function() {
    var initTableShipments = function() {
        var tableShipments = $('#kt_datatable_shipments');

        // begin first table
        tableShipments.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableShipments();
        }
    };
}();
/********** Datatable List Quotes **********/
var KTDatatablesQuotes = function() {
    var initTableQuotes = function() {
        var tableQuotes = $('#kt_datatable_quotes');

        // begin first table
        tableQuotes.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableQuotes();
        }
    };
}();
/********** Datatable List Taxes **********/
var KTDatatablesTaxes = function() {
    var initTableTaxes = function() {
        var tableTaxes = $('#kt_datatable_taxes');

        // begin first table
        tableTaxes.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableTaxes();
        }
    };
}();

/********** Datatable List Documents **********/
var KTDatatablesDocuments = function() {
    var initTableDocuments = function() {
        var tableDocuments = $('#kt_datatable_documents');

        // begin first table
        tableDocuments.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableDocuments();
        }
    };
}();

/********** Datatable List Senders **********/
var KTDatatablesSenders = function() {
    var initTableSenders = function() {
        var tableSenders = $('#kt_datatable_senders');

        // begin first table
        tableSenders.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableSenders();
        }
    };
}();


/********** Datatable List Products **********/
var KTDatatablesProducts = function() {
    var initTableProducts = function() {
        var tableProducts = $('#kt_datatable_products');

        // begin first table
        tableProducts.DataTable({
            responsive: true,
            columnDefs: [
                {
                    width: '150px',
                    targets: 0
                }
            ]
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            initTableProducts();
        }
    };
}();