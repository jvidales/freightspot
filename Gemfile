source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.9'

gem 'activerecord-session_store'
gem 'acts-as-taggable-on'
gem 'administrate'
gem 'apexcharts'
gem 'audited'
gem 'aws-sdk-s3'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'bootstrap4-datetime-picker-rails'
gem 'cancancan'
gem 'cocoon'
gem 'coffee-rails', '~> 4.2'
gem 'devise'
gem 'devise_invitable'
gem 'enum_help'
gem 'flag-icons-rails'
gem 'friendly_id'
gem 'groupdate'
gem 'i18n-timezones'
gem 'intl-tel-input-rails'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'kaminari'
gem 'local_time'
gem 'mapbox-gl-rails'
gem 'mapbox-rails'
gem 'mapbox-sdk'
gem 'mini_magick'
gem 'name_of_person'
gem 'paranoia'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.4', '>= 5.2.4.3'
gem 'rails-i18n'
gem 'rails-jquery-autocomplete'
gem 'roo'
gem 'roo-xls'
gem 'sass-rails', '~> 5.0'
gem 'selectize-rails'
gem 'simple_form'
gem 'slim-rails'
gem 'strong_migrations'
gem 'toastr-rails'
gem 'uglifier', '>= 1.3.0'
gem 'wicked'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'wkhtmltopdf-binary-edge'

group :development, :test do
  gem 'annotate'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'ffaker'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'listen'
  gem 'railroady'
  gem 'rubocop'
  gem 'spring'
  gem 'spring-watcher-listen'
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
