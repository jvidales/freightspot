Rails.application.routes.draw do
  get 'user_steps/client'
  scope "/:locale", locale: /#{I18n.available_locales.join("|")}/ do
    namespace :customer do
      root 'dashboard#show'
      resources :shipments
      resources :clients
      resources :reports
      resources :products
      resources :billings
      resources :quotes do
        collection do
          get 'search_freight_types/:service_mode', action: :search_freight_types, as: 'search_freight_types'
          get 'set_freight_type/:freight_type', action: :set_freight_type, as: 'set_freight_type'
          get 'search_service_type_by_incoterm/:incoterm_id/:operation_type', action: :search_service_type_by_incoterm, as: 'search_service_type_by_incoterm'
          get 'search_service_type_by_operation_type/:operation_type/:incoterm_id', action: :search_service_type_by_operation_type, as: 'search_service_type_by_operation_type'
          get 'get_port_coordinates/:port_id', action: :get_port_coordinates, as: 'get_port_coordinates'
          get 'get_optional_service/:optional_service_id', action: :get_optional_service, as: 'get_optional_service'

          get :tariffs
        end
      end
    end

    namespace :admin do
      resources :settings,  only: [:set_language] do
        collection do
          get 'set_language/:id/:language', action: :set_language, as: 'set_language'
        end
      end
      resources :criteria
      resources :tariffs_imports, only: [:create]
      resources :rate_surcharges
      resources :rates, only: %i[edit update destroy]
      resources :tariffs do
        collection do
          post 'import'
          get 'filter_freight_types/:service_mode', action: :filter_freight_types, as: 'filter_freight_types'
          get 'filter_carriers/:service_mode', action: :filter_carriers, as: 'filter_carriers'
          get 'port_code/:port_id', action: :port_code, as: 'port_code'
        end
      end
      resources :agents do
        collection do
          get 'filter_states/:country_id', action: :filter_states, as: 'filter_states'
          get 'filter_address/:address_id', action: :filter_address, as: 'filter_address'
        end
        member do
          get :update_readonly

          get :new_address
          get :edit_address
          get :show_address
          delete :destroy_address
          post :add_address
          patch :update_address

          get :new_phone
          get :edit_phone
          get :show_phone
          delete :destroy_phone
          post :add_phone
          patch :update_phone

          get :new_contact
          get :edit_contact
          get :show_contact
          delete :destroy_contact
          post :add_contact
          patch :update_contact
          put :update_contact
          post :update_contact

          get :new_document
          get :edit_document
          get :show_document
          delete :destroy_document
          post :add_document
          patch :update_document
          put :update_document

        end
      end
      resources :client_documents
      resources :products
      resources :access_roles do
        collection do
          get 'add_access/:role_id/:access_id/:system_module_id', action: :add_access, as: 'add_permission'
          get 'remove_access/:role_id/:access_id/:system_module_id', action: :remove_access, as: 'remove_permission'
          #get 'add_permission/:id/:access_id/:module_id', action: :add_permission, as: 'add_permission'
        end
      end
      resources :roles do
        member do
          get :assign
        end
      end
      resources :containers
      resources :charges do
        member do
          delete :destroy_charge
        end
      end
      resources :shipments
      resources :suppliers do
        collection do
          get 'filter_states/:country_id', action: :filter_states, as: 'filter_states'
        end
        member do
          get :update_readonly

          get :new_address
          get :edit_address
          get :show_address
          delete :destroy_address
          post :add_address
          patch :update_address

          get :new_phone
          get :edit_phone
          get :show_phone
          delete :destroy_phone
          post :add_phone
          patch :update_phone

          get :new_contact
          get :edit_contact
          get :show_contact
          delete :destroy_contact
          post :add_contact
          patch :update_contact
          put :update_contact
          post :update_contact
        end
      end
      resources :clients do
        collection do
          post 'filter', action: :filter, as: 'filter'

          get 'filter_states/:country_id', action: :filter_states, as: 'filter_states'
          get 'set_column/:column', action: :set_column, as: 'set_column'
        end
        member do
          get :update_readonly

          get :new_address
          get :edit_address
          get :show_address
          delete :destroy_address
          post :add_address
          patch :update_address

          get :new_phone
          get :edit_phone
          get :show_phone
          delete :destroy_phone
          post :add_phone
          patch :update_phone

          get :new_contact
          get :edit_contact
          get :show_contact
          delete :destroy_contact
          post :add_contact
          patch :update_contact
          put :update_contact
          post :update_contact

          get :new_sender
          get :edit_sender
          get :show_sender
          delete :destroy_sender
          post :add_sender
          patch :update_sender

          get :new_client_product
          get :edit_client_product
          get :show_client_product
          delete :destroy_client_product
          post :add_client_product
          patch :update_client_product
          put :update_client_product

          get :new_client_document
          get :edit_client_document
          get :show_client_document
          get :download_client_document
          delete :destroy_client_document
          post :add_client_document
          patch :update_client_document
          put :update_client_document

          get :import_csv_file

        end
        resources :addresses
        resources :phones
        resources :contacts
        resources :senders
      end
      resources :quotes, except: %i[new create edit update] do
        member do
          get :authorize
        end
      end
      resources :middlemen do
        collection do
          get 'filter_states/:country_id', action: :filter_states, as: 'filter_states'
        end
        member do
          get :update_readonly

          get :new_address
          get :edit_address
          get :show_address
          delete :destroy_address
          post :add_address
          patch :update_address

          get :new_phone
          get :edit_phone
          get :show_phone
          delete :destroy_phone
          post :add_phone
          patch :update_phone

          get :new_contact
          get :edit_contact
          get :show_contact
          delete :destroy_contact
          post :add_contact
          patch :update_contact
        end
      end
      resources :users
      resources :services
      resources :packaging_types
      resources :taxes, except: :show
      resources :ports
      resources :documents
      root 'dashboard#show'
    end

    root 'home#index'
    devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
    resources :user_steps
  end
  root to: redirect("/#{I18n.default_locale}", status: 302), as: :redirected_root
  get "/*path", to: redirect("/#{I18n.default_locale}/%{path}", status: 302),
      constraints: { path: /(?!(#{I18n.available_locales.join("|")})\/).*/ },
      format: false
end
